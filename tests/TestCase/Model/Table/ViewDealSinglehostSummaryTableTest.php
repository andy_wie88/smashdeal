<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewDealSinglehostSummaryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewDealSinglehostSummaryTable Test Case
 */
class ViewDealSinglehostSummaryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewDealSinglehostSummaryTable
     */
    public $ViewDealSinglehostSummary;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.view_deal_singlehost_summary'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealSinglehostSummary') ? [] : ['className' => 'App\Model\Table\ViewDealSinglehostSummaryTable'];
        $this->ViewDealSinglehostSummary = TableRegistry::get('ViewDealSinglehostSummary', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealSinglehostSummary);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
