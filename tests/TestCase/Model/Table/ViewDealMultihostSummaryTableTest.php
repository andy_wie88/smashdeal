<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewDealMultihostSummaryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewDealMultihostSummaryTable Test Case
 */
class ViewDealMultihostSummaryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewDealMultihostSummaryTable
     */
    public $ViewDealMultihostSummary;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.view_deal_multihost_summary'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealMultihostSummary') ? [] : ['className' => 'App\Model\Table\ViewDealMultihostSummaryTable'];
        $this->ViewDealMultihostSummary = TableRegistry::get('ViewDealMultihostSummary', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealMultihostSummary);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
