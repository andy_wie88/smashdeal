<div class="alert alert-danger alert-dismissable alert-icms">
    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
    <?= h($message) ?>
    <script>
        $(".alert").delay(5000).fadeOut();
        $(".close").click(function()
    	{
    		$(this).parent().fadeOut();
    	});
    </script>
</div>