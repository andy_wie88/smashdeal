<?php
namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Network\Session;
use Cake\ORM\Behavior;

/**
 * Creator behavior
 */
class CreatorBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function beforeSave(Event $event,EntityInterface $entity)
    {
        $auth = new Session();
        if($auth->read('Auth')!=null)
            $id = $auth->read(["Auth"])["User"]["username"];
        else
            $id = $auth->read(["DealAuth"])["User"]["username"];
        if($entity->isNew() == true)
            $entity->createdby = $id;
        else
            $entity->modifiedby = $id;
    }

}
