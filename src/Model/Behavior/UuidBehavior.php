<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Text;

class UuidBehavior extends Behavior
{

	public function beforeSave(Event $event, EntityInterface $entity)
	{
		if(!isset($entity->id))
		{
			$entity->id = Text::uuid();
		}
	}

}