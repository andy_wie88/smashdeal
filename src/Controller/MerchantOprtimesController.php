<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MerchantOprtimes Controller
 *
 * @property \App\Model\Table\MerchantOprtimesTable $MerchantOprtimes
 */
class MerchantOprtimesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $merchantOprtimes = $this->paginate($this->MerchantOprtimes);

        $this->set(compact('merchantOprtimes'));
        $this->set('_serialize', ['merchantOprtimes']);
    }

    /**
     * View method
     *
     * @param string|null $id Merchant Oprtime id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $merchantOprtime = $this->MerchantOprtimes->get($id, [
            'contain' => []
        ]);

        $this->set('merchantOprtime', $merchantOprtime);
        $this->set('_serialize', ['merchantOprtime']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $merchantOprtime = $this->MerchantOprtimes->newEntity();
        if ($this->request->is('post')) {
            $merchantOprtime = $this->MerchantOprtimes->patchEntity($merchantOprtime, $this->request->data);
            if ($this->MerchantOprtimes->save($merchantOprtime)) {
                $this->Flash->success(__('The merchant oprtime has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The merchant oprtime could not be saved. Please, try again.'));
        }
        $this->set(compact('merchantOprtime'));
        $this->set('_serialize', ['merchantOprtime']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Merchant Oprtime id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $merchantOprtime = $this->MerchantOprtimes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $merchantOprtime = $this->MerchantOprtimes->patchEntity($merchantOprtime, $this->request->data);
            if ($this->MerchantOprtimes->save($merchantOprtime)) {
                $this->Flash->success(__('The merchant oprtime has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The merchant oprtime could not be saved. Please, try again.'));
        }
        $this->set(compact('merchantOprtime'));
        $this->set('_serialize', ['merchantOprtime']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Merchant Oprtime id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $merchantOprtime = $this->MerchantOprtimes->get($id);
        if ($this->MerchantOprtimes->delete($merchantOprtime)) {
            $this->Flash->success(__('The merchant oprtime has been deleted.'));
        } else {
            $this->Flash->error(__('The merchant oprtime could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
