<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MerchantOutlets Controller
 *
 * @property \App\Model\Table\MerchantOutletsTable $MerchantOutlets
 */
class MerchantOutletsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $merchantOutlets = $this->paginate($this->MerchantOutlets);

        $this->set(compact('merchantOutlets'));
        $this->set('_serialize', ['merchantOutlets']);
    }

    /**
     * View method
     *
     * @param string|null $id Merchant Outlet id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $merchantOutlet = $this->MerchantOutlets->get($id, [
            'contain' => []
        ]);

        $this->set('merchantOutlet', $merchantOutlet);
        $this->set('_serialize', ['merchantOutlet']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $merchantOutlet = $this->MerchantOutlets->newEntity();
        if ($this->request->is('post')) {
            $merchantOutlet = $this->MerchantOutlets->patchEntity($merchantOutlet, $this->request->data);
            if ($this->MerchantOutlets->save($merchantOutlet)) {
                $this->Flash->success(__('The merchant outlet has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The merchant outlet could not be saved. Please, try again.'));
        }
        $this->set(compact('merchantOutlet'));
        $this->set('_serialize', ['merchantOutlet']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Merchant Outlet id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $merchantOutlet = $this->MerchantOutlets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $merchantOutlet = $this->MerchantOutlets->patchEntity($merchantOutlet, $this->request->data);
            if ($this->MerchantOutlets->save($merchantOutlet)) {
                $this->Flash->success(__('The merchant outlet has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The merchant outlet could not be saved. Please, try again.'));
        }
        $this->set(compact('merchantOutlet'));
        $this->set('_serialize', ['merchantOutlet']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Merchant Outlet id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $merchantOutlet = $this->MerchantOutlets->get($id);
        if ($this->MerchantOutlets->delete($merchantOutlet)) {
            $this->Flash->success(__('The merchant outlet has been deleted.'));
        } else {
            $this->Flash->error(__('The merchant outlet could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
