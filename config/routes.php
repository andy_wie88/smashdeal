<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;
use Cake\Cache\Cache;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');
//simpan ke session kalo sudah fix ya..
//setidaknya dicache ke local file jadi ngak perlu diquery setiap kali..
try{
    /**
     * Get Connection to check to database
     * Get All Menu then generate routes.
     */
    $con = ConnectionManager::get('default');
    if(($results = Cache::read('defaultroute')) === false){
        $results = $con->newQuery()
            ->select("*")
            ->from("tb_sys_menus")
            ->where(["isadmin"=>"False","isactive"=>"TRUE","isajax"=>"FALSE"])
            ->execute()
            ->fetchAll('assoc');
        Cache::write('defaultroute',$results,'long');
    }

    Router::scope("/", function (RouteBuilder $routes) use($results){

        foreach($results as $link)
        {
            $config = ["controller"=>$link["controller"],"action"=>$link["action"]];
            if($link["plugin"]!=null || $link["plugin"]!="")
            {
                $config["plugin"]=$link["plugin"];
            }
            if($link["prefix"]!=null || $link["prefix"]!="")
            {
                $config["prefix"]=$link["prefix"];
            }
            $routes->connect($link["url"],$config);
        }

        if(Configure::read('debug')==true)
            $routes->fallbacks('DashedRoute');

    });

    /**
     * Get Admin Page Prefix
     * Then Generate route by using Admin Page Prefix as Scope
     */
    if($adminprefix = Cache::read('adminroute') === false){
        $adminprefix = $con->newQuery()
            ->select("*")
            ->from("tb_sys_configs")
            ->where(["code"=>"ADMPRX"])
            ->execute()
            ->fetchAll("assoc");
        Cache::write('adminroute',$adminprefix,'long');
    }

    if(count($adminprefix)>0)
    {
        $prefix = $adminprefix[0]["value"];
        $results = $con->newQuery()
                ->select("*")
                ->from("tb_sys_menus")
                ->where(["isadmin"=>"TRUE","isactive"=>"TRUE","isajax"=>"FALSE"])
                ->execute()
                ->fetchAll('assoc');

        Router::scope($prefix,function(RouteBuilder $routes) use ($results){

            foreach($results as $link)
            {
                $config = ["controller"=>$link["controller"],"action"=>$link["action"]];
                if($link["plugin"]!=null || $link["plugin"]!="")
                {
                    $config["plugin"]=$link["plugin"];
                }
                if($link["prefix"]!=null || $link["prefix"]!="")
                {
                    $config["prefix"]=$link["prefix"];
                }
                $routes->connect($link["url"],$config);
            }

            $routes->connect("/helloworld",["controller"=>"Users","action"=>'index',"plugin"=>"System","prefix"=>'Backend']);
        });
    }
    else
    {
        throw new Exception("Misconfiguration");
    }

    if(($results = Cache::read('xhrroute')) === false){
        $results = $con->newQuery()
            ->select("*")
            ->from("tb_sys_menus")
            ->where(["isactive"=>"TRUE","isajax"=>TRUE])
            ->execute()
            ->fetchAll('assoc');
        Cache::write('xhrroute',$results,'long');
    }

    Router::scope("/xhr",function(RouteBuilder $routes) use ($results){

        foreach($results as $link)
        {
            $config = ["controller"=>$link["controller"],"action"=>$link["action"]];
            if($link["plugin"]!=null || $link["plugin"]!="")
            {
                $config["plugin"]=$link["plugin"];
            }
            if($link["prefix"]!=null || $link["prefix"]!="")
            {
                $config["prefix"]=$link["prefix"];
            }
            $routes->connect($link["url"],$config);
        }

    });
}
catch(Exception $ex)
{
    /**
     * If Something goes wrong eq: Connection unintialize or something  else.
     * redirect to installation to generate database, dll.
     */
    Router::scope("/", function (RouteBuilder $routes){

        $routes->connect("/*",['controller'=>'Install','action'=>'toInstall','plugin'=>'Install']);
        $routes->connect('/install/*',['controller'=>'Install','action'=>'install','plugin'=>'Install']);
    });
}

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();