(function($){

	$.fn.popdisplay	= function( options )
	{
		var settings = {'header':'Pop Display','width':'400px','height':'500px','fullscreen':false};
		var components = [];

		settings = $.extend(settings,options);

		if(settings.fullscreen)
		{
			settings.width = $(window).width()-20;
			settings.height = $(window).height()-20;
		}

		$(this).each(function()
		{
			var id = randomString(5,"aA#");
			if($(this).attr("id")==undefined)
				$(this).attr("id",id);
			else
				id = $(this).attr("id");

			var isOnDom = $.contains(document,"#"+id);

			components.push({
				'component': $(this),
				'url' : $(this).attr("data-href"),
				'loaded' : false,
				'windowid' : randomString(5,'Aa#'),
				'id' :id,
				'onPage' : isOnDom
			});
		});

		$(components).each(function(index)
		{
			if(components[index].onPage){
				$("#"+this.id).on("click",function(e)
				{
					e.preventDefault();
					popAction(index);
				});
			}
			else
			{
				$(document).on("click","#"+this.id,function(e)
				{
					e.preventDefault();
					popAction(index);
				});
			}
		});

		var popAction = function(index)
		{
			if(components[index].loaded==false)
			{
				$.get(components[index].url,function(data)
				{
					components[index].content = data;
					renderWindow(index);
					components[index].loaded = true;
				});
			}
			else
			{
				renderWindow(index)
			}
		}

		var renderWindow = function(index)
		{
			var content = 
						"<div id='"+components[index].windowid+"'>"+
							"<div>"+settings.header+"</div>"+
							"<div class='content'><div class='col-md-12'>"+components[index].content+"</div></div>"+
						"</div>";
			$("#"+components[index].id).append(
				content
			);
			$("#"+components[index].windowid).jqxWindow({
				width:settings.width,
				height:settings.height,
				maxWidth:$(window).width(),
				maxHeight:$(window).height()
			});
		}

		function randomString(length, chars) {
		    var mask = '';
		    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
		    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    if (chars.indexOf('#') > -1) mask += '0123456789';
		    if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
		    var result = '';
		    for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
		    return result;
		}
		
	}

})(jQuery);