var dpconverter = function(format)
{
	switch(format)
	{
		case "dd+MMMM+Y" : 
			return "dd MM yyyy";
		case "Y-MM-dd" :
			return "dd-mm-Y";
		case "MM/dd/Y" :
			return "mm/dd/yyyy";
		case "dd/MM/Y" : 
			return "dd/mm/yyyy";
	}
}