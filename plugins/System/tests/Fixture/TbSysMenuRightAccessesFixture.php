<?php
namespace System\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TbSysMenuRightAccessesFixture
 *
 */
class TbSysMenuRightAccessesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'menurightid' => ['type' => 'string', 'length' => 100, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'userroleid' => ['type' => 'string', 'length' => 100, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['menurightid', 'userroleid'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'menurightid' => 'bb7789f3-bf0c-4133-bbba-5f792dcb9115',
            'userroleid' => '0ab60e29-986f-48d6-b5b4-d7ad8bbcc7fd'
        ],
    ];
}
