<?php
namespace System\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use System\Controller\Component\PagesComponent;

/**
 * System\Controller\Component\PagesComponent Test Case
 */
class PagesComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Controller\Component\PagesComponent
     */
    public $Pages;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Pages = new PagesComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pages);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
