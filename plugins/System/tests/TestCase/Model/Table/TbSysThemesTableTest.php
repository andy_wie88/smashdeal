<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysThemesTable;

/**
 * System\Model\Table\TbSysThemesTable Test Case
 */
class TbSysThemesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysThemesTable
     */
    public $TbSysThemes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_themes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysThemes') ? [] : ['className' => 'System\Model\Table\TbSysThemesTable'];
        $this->TbSysThemes = TableRegistry::get('TbSysThemes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysThemes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
