<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysUserPreferencesTable;

/**
 * System\Model\Table\TbSysUserPreferencesTable Test Case
 */
class TbSysUserPreferencesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysUserPreferencesTable
     */
    public $TbSysUserPreferences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_user_preferences'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysUserPreferences') ? [] : ['className' => 'System\Model\Table\TbSysUserPreferencesTable'];
        $this->TbSysUserPreferences = TableRegistry::get('TbSysUserPreferences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysUserPreferences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
