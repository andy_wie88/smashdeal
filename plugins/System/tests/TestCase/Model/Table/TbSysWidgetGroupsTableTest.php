<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysWidgetGroupsTable;

/**
 * System\Model\Table\TbSysWidgetGroupsTable Test Case
 */
class TbSysWidgetGroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysWidgetGroupsTable
     */
    public $TbSysWidgetGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_widget_groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysWidgetGroups') ? [] : ['className' => 'System\Model\Table\TbSysWidgetGroupsTable'];
        $this->TbSysWidgetGroups = TableRegistry::get('TbSysWidgetGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysWidgetGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
