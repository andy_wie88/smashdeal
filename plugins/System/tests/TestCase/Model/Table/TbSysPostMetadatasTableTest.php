<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysPostMetadatasTable;

/**
 * System\Model\Table\TbSysPostMetadatasTable Test Case
 */
class TbSysPostMetadatasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysPostMetadatasTable
     */
    public $TbSysPostMetadatas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_post_metadatas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysPostMetadatas') ? [] : ['className' => 'System\Model\Table\TbSysPostMetadatasTable'];
        $this->TbSysPostMetadatas = TableRegistry::get('TbSysPostMetadatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysPostMetadatas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
