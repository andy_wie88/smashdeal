<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysSubMenusTable;

/**
 * System\Model\Table\TbSysSubMenusTable Test Case
 */
class TbSysSubMenusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysSubMenusTable
     */
    public $TbSysSubMenus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_sub_menus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysSubMenus') ? [] : ['className' => 'System\Model\Table\TbSysSubMenusTable'];
        $this->TbSysSubMenus = TableRegistry::get('TbSysSubMenus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysSubMenus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
