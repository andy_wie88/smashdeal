<div>
    <h3><?= __d("system", "Attachement Details") ?></h3>
    <div class="col-md-12 no-padding">
        <?= $this->element('System.mediarenderer',['bean'=>$media,'isajax'=>true]) ?>
    </div>
    <h4><?= $media["title"]; ?></h4>
    <p class="info"><?= $media->date->i18nFormat($config["SITEDTFORM"]); ?></p>
    <hr/>
    <form class="form-horizontal">
        <div class="form-group">
            <label class="col-md-4 control-label">
                <?= __d("system","URL") ?>
            </label>
            <div class="col-md-8">
                <input type="text" class="form-control" value="<?= $this->Url->build(['controller'=>'Medias','action'=>'view','prefix'=>'Frontend','plugin'=>'System',$media->slug],['fullBase' => true]) ?>" readonly />
            </div>
        </div>
        <?php
            $lang = [];
            $defaultlang = $config["SITEDFLTLG_VALUE"];
            foreach($media->langs as $lng)
            {
                if($lng["languageid"]==$defaultlang)
                {
                    $lang = $lng;
                }
            }
        ?>
        <div class="form-group">
            <label class="col-md-4 control-label">
                <?= __d("system","Title") ?>
            </label>
            <div class="col-md-8">
                <input type="text" class="form-control" value="<?= $lang->title ?>" readonly />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">
                <?= __d("system","Description") ?>
            </label>
            <div class="col-md-8">
                <textarea class="form-control" readonly><?= $lang->body ?></textarea>
            </div>
        </div>
    </form>
</div>