<?php foreach($medias as $media): ?>
    <?php $file_path = "";
        foreach($media["metas"] as $meta){
            if($meta["name"]=="file_path")
            {
                $file_path = $meta["value"];
                break;
            }
        }
    ?>
    <li>
        <div class="col-md-2">
            <span class="floating-box">
            </span>
            <?= $this->element('mediathumbnail',['bean'=>$media]) ?>
        </div>
    </li>
<?php endforeach; ?>