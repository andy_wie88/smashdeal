<?php if(isset($medias)): ?>
    <?php foreach($medias as $media): ?>
        <div class="image-wrapper" id="<?= $media["id"] ?>">
            <?= $this->element('System.mediarenderer',['bean'=>$media,'isajax'=>true]) ?>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <?= $this->element('System.mediarenderer',['bean'=>$media,'isajax'=>true]) ?>
<?php endif; ?>
