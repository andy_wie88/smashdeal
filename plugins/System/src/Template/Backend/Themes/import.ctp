<?= $this->Form->create($theme,['class'=>'form-horizontal dropzone','id'=>'uploadplugin','type'=>'file']); ?>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-upload"></i>
        </button>
    </div>
    <div class="box box-default">
        <div class="box-body">
            <p>
                <?= __d('system',"Please choose your theme file.") ?>
            </p>
            <input name="file" type="file" id="uploader"/>
        </div>
    </div>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-upload"></i>
        </button>
    </div>
<?= $this->Form->end(); ?>