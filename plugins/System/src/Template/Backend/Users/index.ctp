<?= $this->AssetCompress->css('System.Use54dds',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Use54dds',['block'=>'footer-script']) ?>
<?php
	$allAmount = count($allusers->toArray());
	$activeAmount = 0;
	$inactiveAmount = 0;
	foreach($allusers as $mdl){
		if($mdl["isactive"]==true)
			$activeAmount++;
		else
			$inactiveAmount++;
	} 
?>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<small>
				<strong>
					<?php if($status==null): ?>
						<?= "All (".$allAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'Users','action'=>'search','plugin'=>'System','prefix'=>'Backend',"s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'Users','action'=>'index','plugin'=>'System','prefix'=>'Backend']); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="active"): ?>
						<?= "Active (".$activeAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Users','action'=>'search','plugin'=>'System','prefix'=>'Backend',"active","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Users','action'=>'index','plugin'=>'System','prefix'=>'Backend',"active"]); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="inactive"): ?>
						<?= "Inactive (".$inactiveAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Users','action'=>'search','plugin'=>'System','prefix'=>'Backend',"inactive","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Users','action'=>'index','plugin'=>'System','prefix'=>'Backend',"inactive"]); ?>
						<?php endif; ?>
					<?php endif; ?>
				</strong>
			</small>
		</h3>
		<div class="box-tools col-md-4 no-padding">
            <?= $this->Form->create($user,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
            <div class="input-group input-group-sm">
                <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
            <?= $this->Form->end(); ?>
        </div>
	</div>
	<div class="box-body no-padding">
		<?= $this->Form->create($user,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
			<div class="list-action clearfix">
					<div class="col-md-2 no-padding">
						<select id="bulk-action" name="action" class="form-control i-combo">
							<option value=""><?= __d('system',"Bulk Action") ?></option>
							<option value="1"><?= __d('system',"Activate") ?></option>
							<option value="2"><?= __d('system',"Non-Activate") ?></option>
						</select>
					</div>
					<div class="col-md-2 no-padding">
						<button class="btn btn-default" type="submit" id="btn-bulk-action">
							<?= __d('system',"Apply") ?>
						</button>
					</div>
				<div class="pull-right item-amount">
					<?php
						$label = ($allAmount>=2)?" items":" item";
						echo $allAmount . $label;
					?>
				</div>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th class="cb-wrap" width="3%">
							<input type="checkbox" class="checkbox-custom" id="checkall" />
						</th>
						<th width="5%"></th>
						<th width="15%"><?= __d('system',"Username") ?></th>
						<th width="15%"><?= __d('system',"First Name") ?></th>
						<th width="15%"><?= __d('system',"Last Name") ?></th>
						<th width="15%"><?= __d('system',"Birthday") ?></th>
						<th width="5%"><?= __d('system',"Status") ?></th>
						<th width="5%"><?= __d('system',"Admin") ?></th>
						<th width="10%" class="text-center"><?= __d('system',"Action") ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($users)>0): ?>
						<?php foreach($users as $u): ?>
							<tr>
								<td class="cb-wrap">
									<input type="checkbox" name="value[]" value="<?= $u["id"] ?>" class="cb-item checkbox-custom checkbox-child" data-value="<?= $u["id"] ?>"/>
								</td>
								<td>
									<?php
										if($u["profile"]["profilepic"]!=null){
											echo $this->Html->image(DS."files".DS.$u["profile"]["profilepic"],["class"=>"img-circle img-thumbnail","style"=>"height:47px;width:50px"]);
										}
										else
										{
											echo $this->Html->image("System.user.png",["class"=>'img-circle img-thumbnail',"style"=>"height:47px;width:50px"]);
										}
									?>
								</td>
								<td><?= $u['username'] ?></td>
								<td><?= $u['profile']['firstname'] ?></td>
								<td><?= $u['profile']['lastname'] ?></td>
								<td><?= ($u['profile']['birthday']!=null)?$u['profile']['birthday']->i18nFormat($config["SITEDTFORM"]):'-'; ?></td>
								<td style="text-align:center">
									<input type="checkbox" class="cbreadonly" <?= ($u["isactive"])?"checked":"" ?> readonly/>
								</td>
								<td style="text-align:center">
									<input type="checkbox" class="cbreadonly" <?= ($u["issuperadmin"])?"checked":"" ?> readonly/>
								</td>
								<td style="text-align:center">
									<div class="btn-group btn-group-sm">
										<?= $this->Html->link("<i class='fa fa-edit'></i>",['controller'=>'Users','action'=>'modify','plugin'=>'System','prefix'=>'Backend',$u["id"]],['class'=>'btn btn-default btn-sm','escape'=>false]) ?>
										<?= $this->Html->link("<i class='fa fa-eye'></i>",['controller'=>'Users','action'=>'view','plugin'=>'System','prefix'=>'Backend',$u["id"]],['class'=>'btn btn-default btn-sm','escape'=>false]); ?>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    <span class="caret"></span>
										    <span class="sr-only">Toggle Dropdown</span>
									  	</button>
									  	<ul class="dropdown-menu dropdown-menu-right">
									  		<li>
									  		 	<a href="#" class="quick-prev" data-href="<?= $this->url->build(['controller'=>'Users','action'=>'view','plugin'=>'System','prefix'=>'Backend',$u['id']]); ?>">
									  		 		<?= __d('system',"Quick Preview") ?>
									  		 	</a>
									  		</li>
										    <li role="separator" class="divider"></li>
									  		<li>
									  			<?= $this->Html->link(__d("system","Dashboard Auth"),['controller'=>'Users','action'=>'dashboardAuth','plugin'=>'System','prefix'=>'Backend',$u["id"]]); ?>
									  		</li>
										    <li>
										    	<?php if(!$u["isactive"]): ?>
													<?= $this->Html->link(__d('system',"Activate"),['controller'=>'Users','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend','active',$u["id"]]); ?>
												<?php else: ?>
													<?= $this->Html->link(__d('system',"Inactivate"),['controller'=>'Users','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend','inactive',$u["id"]]); ?>
												<?php endif; ?>
										    </li>
										    <li>
												<?= $this->Html->link(__d('system',"Reset Password"),['controller'=>'Users','action'=>'resetPassword','plugin'=>'System','prefix'=>'Backend',$u["id"]]); ?>
										    </li>
									  	</ul>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="9">
								<?php 
									if(isset($s))
										echo __d('system',"No user found for ''{0}'' keyword.",[$s]);
									else
										echo __d('system',"No user avaiable."); 
								?>
							</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		<?= $this->Form->end(); ?>
	</div>
	<div class="box-footer">
		<?= $this->element('System.paginator'); ?>
	</div>
</div>