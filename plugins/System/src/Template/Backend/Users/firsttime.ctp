<?= $this->AssetCompress->css('System.Use57dds',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Use57dds',['block'=>'footer-script']) ?>
<?= $this->Form->create($user,['class'=>'form-horizontal','type'=>'file']) ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title"><?= __d('system',"Change Password") ?></h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			<label class="col-md-2 control-label"><?= __d('system',"Password") ?></label>
			<div class="col-md-4">
				<?= $this->Form->input('password',['class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Password','value'=>'']) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label"><?= __d('system',"Confirm Password") ?></label>
			<div class="col-md-4">
				<?= $this->Form->input('confirm_password',['class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Confirm Password','value'=>'','type'=>'password']) ?>
			</div>
		</div>
	</div>
</div>
<div class="box box-default">
	<div class="box-header">
		<h3 class="box-title">
			<?= __d('system',"Profile Data") ?>
		</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"First Name") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('profile.firstname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"first name")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"Last Name") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('profile.lastname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"last name")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"Full Name") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('profile.fullname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"full name")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"Display Name") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('profile.displayname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"display name")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"Birthday") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('profile.birthday',['class'=>'form-control datepick','label'=>false,'div'=>false,'type'=>'text','placeholder'=>__d('system',"20-07-2015"),'value'=>(!empty($user->profile["birthday"]))?$user->profile["birthday"]->i18nFormat($config['SITEDTFORM']):""]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"Address") ?>
			</label>
			<div class="col-md-10">
				<?= $this->Form->input('profile.address',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"address")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"Website") ?>
			</label>
			<div class="col-md-10">
				<?= $this->Form->input('profile.website',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"website")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"About me") ?>
			</label>
			<div class="col-md-10">
				<?= $this->Form->input('profile.aboutme',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"about me")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">
				<?= __d('system',"Profile Picture") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('profile.profilepic',['class'=>'form-control','label'=>false,'div'=>false,'type'=>'file']) ?>
			</div>
		</div>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<?= $this->Form->end(); ?>