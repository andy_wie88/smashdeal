<?= $this->AssetCompress->script('System.Menu2341',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.Menu2341',['block'=>'css']) ?>
<div class="icms-tab">
    <ul class="nav nav-tabs icms-nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#edit-menu" aria-controls="edit-menu" role="tab" data-toggle="tab"><?= __d("system", "Edit Menus") ?></a></li>
        <li role="presentation"><a href="#manage-locator" aria-controls="manage-locator" role="tab" data-toggle="tab"><?= __d("system","Manage Locator") ?></a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane clearfix active" id="edit-menu">
            <div class="box box-icms-float">
                <div class="box-body">
                    <?= $this->Form->create($menugroup,['type'=>'get','class'=>'form-inline']); ?>
                    <div class="form-group">
                        <label for="#menugroup"><?= __d("system","Select menu to edit : ") ?></label>
                        <select name="s" id="menugroup" class="form-control input-sm" style="min-width: 200px;">
                            <?php foreach($menugroups as $key=>$menu): ?>
                                <option value="<?= $key ?>" <?= ($key==$menugroup->id)?"selected":""; ?> ><?= $menu ?></option>
                            <?php endforeach; ?>
                        </select>
                        <button type="submit" class="btn btn-sm btn-primary"><?= __d("system","Select"); ?></button>
                        <span>
                            <?= __d("system","or"); ?>
                            <?= $this->Html->link(__d("system","create a new menu"),['controller'=>'Menus','action'=>'edit','plugin'=>'System','prefix'=>'Backend','new']) ?>
                        </span>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?php if($mode=="edit"): ?>
                        <?= $this->element('System.menuselector'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-8">
                    <div class="box icms-box clearfix">
                        <?= $this->Form->create($menugroup,['class'=>'form-horizontal']); ?>
                            <div class="icms-box-head">
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label">
                                        <?= __d("system","Menu Label") ?>
                                    </label>
                                    <div class="col-md-4">
                                        <?= $this->Form->input('name',['label'=>false,'class'=>'form-control','type'=>'post']) ?>
                                    </div>
                                    <div class="col-md-3 pull-right text-right">
                                        <div class="min-padding-right">
                                            <?php if($menu='edit'): ?>
                                                <button class="type btn btn-primary"><?= __d("system", "Save Menu") ?></button>
                                            <?php else: ?>
                                                <button class="type btn btn-primary"><?= __d("system", "Create Menu") ?></button>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="icms-box-body">
                                <?php if($mode=="edit"): ?>
                                    <h2><?= __d("system", "Menu Structure") ?></h2>
                                    <p><?= __d("system","Add menu item from the column on the left.") ?></p>
                                    <?php
                                        function genEditedList($view,$menus,$parentid = null)
                                        {
                                            $result = "";
                                            foreach($menus as $key=>$menu)
                                            {
                                                if($parentid==null){
                                                    if($menu["parentid"]==null)
                                                    {
                                                        $result .= "<li id='".$menu["id"]."' data-id='".$menu["id"]."'>";
                                                        $result .= $view->element('System.single_menu',['menu'=>$menu,'index'=>$key]);
                                                        $result .= "<ol class='sortable'>";
                                                        $result .= genEditedList($view, $menus,$menu["id"]);
                                                        $result .= "</ol>";
                                                        $result .= "</li>";
                                                    }
                                                }
                                                else{
                                                    if($menu["parentid"]==$parentid) {
                                                        $result .= "<li id='".$menu["id"]."' data-id='".$menu["id"]."'>";
                                                        $result .= $view->element('System.single_menu', ['menu' => $menu,'index'=>$key]);
                                                        $result .= "<ol class='sortable'>";
                                                        $result .= genEditedList($view, $menus, $menu["id"]);
                                                        $result .= "</ol>";
                                                        $result .= "</li>";
                                                    }
                                                }
                                            }
                                            return $result;
                                        }
                                    ?>
                                    <ol id="menugroupitem" class="sortable">
                                        <?= genEditedList($this,$menugroup["menu_details"]) ?>
                                    </ol>
                                <?php else: ?>
                                    <p><?= __d("system","Give your menu a name, then click Create Menu") ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="icms-box-foot clearfix">
                                <?php if($mode=="edit"): ?>
                                    <div class="col-md-3">
                                        <?= $this->Html->link( __d("system","delete menu"),['controller'=>'Menus','action'=>'delete','plugin'=>'System','prefix'=>'Backend',$menugroup["id"]]) ?>
                                    </div>
                                <?php endif; ?>
                                <div class="col-md-3 min-padding-right pull-right text-right">
                                    <?php if($mode='edit'): ?>
                                        <button class="type btn btn-primary"><?= __d("system", "Save Menu") ?></button>
                                    <?php else: ?>
                                        <button class="type btn btn-primary"><?= __d("system", "Create Menu") ?></button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane clearfix" id="manage-locator">
            <p><?= __d("system","Your theme support {0} menus. Select which menu appears in each location",count($locators)) ?></p>
            <?= $this->Form->create($menugroup,['url'=>['action'=>'menuLocator']]) ?>
            <div class="col-md-6 no-padding">
                <div class="box box-icms-float">
                    <div class="box-body no-padding clearfix">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="40%"><?= __d("system","Location") ?></th>
                                    <th width="60%"><?= __d("system","Assigned Menu") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($locators as $key=>$locator): ?>
                                    <tr>
                                        <td><label for="" class="control-label"><?= $locator["label"] ?></label></td>
                                        <td>
                                            <div class="col-md-7">
                                                <input name="locator[<?= $key ?>][id]" type="hidden" value="<?= $locator["id"] ?>"/>
                                                <?= $this->Form->input('locator'.$key.'menugroupid',['name'=>'locator['.$key.'][menugroupid]','label'=>false,'div'=>false,'class'=>'form-control input-sm','options'=>$menugroups,'empty'=>__d("system",'--choose a menu--'),'value'=>$locator["menugroupid"]]) ?>
                                            </div>
                                            <div class="col-md-5">
                                                <?php if(isset($locator->menugroupid)): ?>
                                                    <?= $this->Html->link(__d("system","edit"),['controller'=>'Menus','action'=>'edit','prefix'=>'Backend','plugin'=>'System','s'=>$locator->menugroupid]); ?>
                                                <?php endif; ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <button type="submit" class="btn btn-primary btn-sm"><?= __d("system","Save Changes") ?></button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

