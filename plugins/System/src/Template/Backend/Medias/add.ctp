<?= $this->Html->css('plugins/fileupload/jquery.fileupload',['block'=>'css']) ?>
<?= $this->Html->script('plugins/fileupload/vendor/jquery.ui.widget',['block'=>'footer-script']) ?>
<?= $this->Html->script('plugins/fileupload/jquery.iframe-transport.js',['block'=>'footer-script']) ?>
<?= $this->Html->script('plugins/fileupload/jquery.fileupload.js',['block'=>'footer-script']) ?>
<?= $this->Html->script('System.media/upload',['block'=>'footer-script']) ?>
<div id="dropzone">
    <div class="dropzone-inner">
        <p class="dropzone-inner-info">
            <?= __d("system","Drop files here") ?>
        </p>
        <p>
            <?= __d("system","or"); ?>
        </p>
        <p class="dropzone-inner-buttons">
            <span class="btn btn-default btn-sm fileinput-button">
                <span><?= __d("system","select files") ?></span>
                <input id="fileupload" type="file" name="files[]" data-url="<?= $this->Url->build(['controller'=>'Medias','action'=>'upload','plugin'=>'System','prefix'=>'Ajax']) ?>" multiple/>
            </span>
        </p>
    </div>
</div>
<p style="padding-top: 10px;">
    <?= __d("system","you are using the multi-file uploader. Problems? Try the <a href='#' id='browser-uploader'>browser uploader</a> instead"); ?>
</p>
<p>
    <?= __d("system","Maximum upload file size: 200 MB.") ?>
</p>
<div id="progress" class="progress hidden">
    <div class="progress-bar progress-bar-success"></div>
</div>
<ul id="listfile" class="list-file">

</ul>