<?= $this->AssetCompress->css('System.Conf455',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Conf455',['block'=>'footer-script']) ?>
<?= $this->Form->create($writingConfig,['class'=>'form-horizontal']); ?>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h2 class="box-title"><?= __d("system","Writing Config") ?></h2>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="sitetitle" class="col-md-4 control-label">
                            <?= __d("system","Default Category") ?>
                        </label>
                        <div class="col-md-5">
                            <?= $this->Form->input('DFTCATE.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('DFTCATE.value',['class'=>'form-control','id'=>'defaultcategory','options'=>$listcategories,'label'=>false,'div'=>false,'escape'=>false]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
<?= $this->Form->end(); ?>