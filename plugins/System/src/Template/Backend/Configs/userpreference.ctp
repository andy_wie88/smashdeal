<?= $this->AssetCompress->script('System.userprefDD',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.userprefDD',['block'=>'css']) ?>
<?= $this->Form->create($userpref,['class'=>'form-horizontal','type'=>'file']); ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="row">
	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header">
				<h2 class="box-title"><?= __d("system","User Preferences") ?></h2>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label for="colorschema" class="control-label col-md-3">
						<?= __d("system","Color Schema"); ?>
					</label>
					<div class="col-md-4">
						<div class="drop-label">
							<?= $this->Form->input('ADMTHEME.id',["type"=>"hidden"]) ?>
							<?= $this->Form->radio('ADMTHEME.value',$colorschemas); ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="language" class="control-label col-md-3">
						<?= __d("system","Interface Language") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('ADMDFLTLG.id',['type'=>'hidden']) ?>
						<?= $this->Form->input('ADMDFLTLG.value',['class'=>'form-control','id'=>'language','label'=>false,'div'=>false,'options'=>$languages,'empty'=>__d("system","--Choose language--")]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<?= $this->Form->end(); ?>