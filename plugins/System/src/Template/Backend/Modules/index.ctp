<?= $this->AssetCompress->css('System.Plug434',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Plug434',['block'=>'footer-script']) ?>
<?php
	$allAmount = count($allmodules->toArray());
	$activeAmount = 0;
	$inactiveAmount = 0;
	foreach($allmodules as $mdl){
		if($mdl["isactive"]==true)
			$activeAmount++;
		else
			$inactiveAmount++;
	} 
?>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<small>
				<strong>
					<?php if($status==null): ?>
						<?= "All (".count($allmodules).")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'Modules','action'=>'search','plugin'=>'System','prefix'=>'Backend',"s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'Modules','action'=>'index','plugin'=>'System','prefix'=>'Backend']); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="active"): ?>
						<?= "Active (".$activeAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Modules','action'=>'search','plugin'=>'System','prefix'=>'Backend',"active","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Modules','action'=>'index','plugin'=>'System','prefix'=>'Backend',"active"]); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="inactive"): ?>
						<?= "Inactive (".$inactiveAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Modules','action'=>'search','plugin'=>'System','prefix'=>'Backend',"inactive","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Modules','action'=>'index','plugin'=>'System','prefix'=>'Backend',"inactive"]); ?>
						<?php endif; ?>
					<?php endif; ?>
				</strong>
			</small>
		</h3>
		<div class="box-tools col-md-4 no-padding">
            <?= $this->Form->create($module,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
            <div class="input-group input-group-sm">
                <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
            <?= $this->Form->end(); ?>
        </div>
	</div>
	<div class="box-body no-padding">
		<div class="list-action clearfix">
			<div class="pull-right item-amount">
				<?php
					$label = ($allAmount>=2)?__d("system"," items"):__d("system"," item");
					echo $allAmount . $label;
				?>
			</div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th width="40%"><?= __d('system',"Module") ?></th>
					<th width="55%"><?= __d('system',"Description") ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($modules)>0): ?>
					<?php foreach($modules as $mdl): ?>
						<tr>
							<td>
								<div style="padding-bottom:4px;"><?= $mdl["displayname"] ?></div>
								<div>
									<?php if(!$mdl["issystem"]): ?>
										<?php if($mdl["isactive"]): ?>
											<?= $this->Html->link('Inactivate',['controller	'=>'Modules','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend',$mdl->id,'inactive']) ?>
										<?php else: ?>
											<?= $this->Html->link('Activate',['controller'=>'Modules','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend',$mdl->id,'active']) ?>
										<?php endif; ?>
										|
										<?= $this->Html->link('Remove',['controller'=>'Modules','action'=>'remove','plugin'=>'System','prefix'=>'Backend',$mdl->id],['class'=>'text-danger']) ?>
									<?php endif; ?>
								</div>
							</td>
							<td>
								<p><?= $mdl["description"] ?></p>
								<div>
									<strong><?= __d('system',"Version ") ?><?= $mdl["version"] ?></strong>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="3">
							<?php
								if(isset($s))
									echo __d('system',"No module found for ''{0}'' keyword.",[$s]);
								else
									echo __d('system',"No module avaiable.")
							?>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
	<div class="box-footer">
		<?= $this->element('System.paginator'); ?>
	</div>
</div>