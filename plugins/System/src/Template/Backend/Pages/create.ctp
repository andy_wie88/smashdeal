<?= $this->Form->create($page,['class'=>'form-horizontal']); ?>
<div class="row">
    <div class="col-md-9">
        <?php $index = 0; ?>
        <?php if(!empty($top_widgets)): ?>
            <?php foreach($top_widgets as $key=>$widget): ?>
                <?php $elementname =  (!empty($widget["pluginname"]))?$widget["pluginname"].".".$widget["component"]:$widget["component"]; ?>
                <?= $this->element($elementname,['index'=>$index,'bean'=>$page]); ?>
                <?php $index++; ?>
            <?php endforeach ;?>
        <?php endif; ?>
        <?php $index = 0; ?>
        <?php if(!empty($bottom_widgets)): ?>
            <?php foreach($bottom_widgets as $key=>$widget): ?>
                <?php $elementname =  (!empty($widget["pluginname"]))?$widget["pluginname"].".".$widget["component"]:$widget["component"]; ?>
                <?= $this->element($elementname,['index'=>$index,'bean'=>$page]); ?>
                <?php $index++; ?>
            <?php endforeach ;?>
        <?php endif; ?>
    </div>
    <div class="col-md-3 no-padding-left">
        <div id="publish-status" class="box box-primary">
            <div class="box-header">
                <h2 class="box-title"><?= __d("system", "Publish") ?></h2>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php if($this->request->action!="modify"): ?>
                    <button type="submit" name="savetype" value="draft" class="btn btn-sm btn-default">
                        <?= __d("system","Save Draft"); ?>
                    </button>
                <?php else: ?>
                    <div class="clearfix">
                        <?= $this->Html->link(__d("system","Preview"),['controller'=>'Pages','action'=>'preview','System'=>'Backend','prefix'=>'Backend',$page->id],['class'=>'btn btn-sm btn-default pull-right','target'=>"_blank"]) ?>
                    </div>
                <?php endif; ?>
                <hr class="separator"/>
                <?php if(!empty($publish_widgets)): ?>
                    <?php foreach($publish_widgets as $key=>$widget): ?>
                        <?php $elementname =  (!empty($widget["pluginname"]))?$widget["pluginname"].".".$widget["component"]:$widget["component"]; ?>
                        <?= $this->element($elementname,['index'=>$index,'bean'=>$page]); ?>
                        <?php $index++; ?>
                    <?php endforeach ;?>
                <?php endif; ?>
            </div>
            <div class="box-footer">
                <?php if($this->request->action=="modify"): ?>
                    <button type="submit" name="savetype" value="update" class="btn btn-sm btn-primary pull-right">
                        <?= __d("system","Update") ?>
                    </button>
                <?php else: ?>
                    <button type="submit" name="savetype" value="publish" class="btn btn-sm btn-primary pull-right">
                        <?= __d("system","Publish"); ?>
                    </button>
                <?php endif; ?>
            </div>
        </div>
        <div id="page-attribute" class="box box-primary">
            <div class="box-header">
                <h2 class="box-title"><?= __d("system", "Page Attribute") ?></h2>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php if(!empty($attribute_widgets)): ?>
                    <?php foreach($attribute_widgets as $key=>$widget): ?>
                        <?php $elementname =  (!empty($widget["pluginname"]))?$widget["pluginname"].".".$widget["component"]:$widget["component"]; ?>
                        <?= $this->element($elementname,['index'=>$index,'bean'=>$page]); ?>
                        <?php $index++; ?>
                    <?php endforeach ;?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>