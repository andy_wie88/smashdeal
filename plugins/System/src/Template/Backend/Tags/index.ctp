<?= $this->AssetCompress->css('System.Tag345',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Tag345',['block'=>'footer-script']) ?>
<?php
$allAmount = count($alltags->toArray());
$activeAmount = 0;
$inactiveAmount = 0;
foreach($alltags as $mdl){
    if($mdl["isactive"]==true)
        $activeAmount++;
    else
        $inactiveAmount++;
}
?>
<div class="row">
    <div class="col-md-4">
        <?= $this->Form->create($tag,['url'=>['action'=>'add'],'class'=>'form-horizontal']); ?>
        <div class="box box-default">
            <div class="box-header">
                <h2 class="box-title"><?= __d("system", "Add new tag") ?></h2>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-md-12">
                        <?= $this->Form->input('name',['class'=>'form-control','label'=>__d("system","Name")]) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $this->Form->input('description',['class'=>'form-control','label'=>__d("system","Description")]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $index = 0; ?>
        <?php if(!empty($add_bottom_widgets)): ?>
            <?php foreach($add_bottom_widgets as $key=>$widget): ?>
                <?php $elementname =  (!empty($widget["pluginname"]))?$widget["pluginname"].".".$widget["component"]:$widget["component"]; ?>
                <?= $this->element($elementname,['index'=>$index,'bean'=>$tag]); ?>
                <?php $index++; ?>
            <?php endforeach ;?>
        <?php endif; ?>
        <button class="btn btn-primary" type="submit"><?= __d("system","save"); ?></button>
        <?= $this->Form->end(); ?>
    </div>
    <div class="col-md-8">
        <div class="box box-default">
            <div class="box-header">
                <h3 class="box-title">
                    <small>
                        <strong>
                            <?php if($status==null): ?>
                                <?= "All (".$allAmount.")" ?>
                            <?php else: ?>
                                <?php if(isset($s)): ?>
                                    <?= $this->Html->link("All (".$allAmount.")",['controller'=>'Tags','action'=>'search','plugin'=>'System','prefix'=>'Backend',"s"=>$s]); ?>
                                <?php else: ?>
                                    <?= $this->Html->link("All (".$allAmount.")",['controller'=>'Tags','action'=>'index','plugin'=>'System','prefix'=>'Backend']); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                            |
                            <?php if($status=="active"): ?>
                                <?= "Active (".$activeAmount.")" ?>
                            <?php else: ?>
                                <?php if(isset($s)): ?>
                                    <?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Tags','action'=>'search','plugin'=>'System','prefix'=>'Backend',"active","s"=>$s]); ?>
                                <?php else: ?>
                                    <?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Tags','action'=>'index','plugin'=>'System','prefix'=>'Backend',"active"]); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                            |
                            <?php if($status=="inactive"): ?>
                                <?= "Inactive (".$inactiveAmount.")" ?>
                            <?php else: ?>
                                <?php if(isset($s)): ?>
                                    <?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Tags','action'=>'search','plugin'=>'System','prefix'=>'Backend',"inactive","s"=>$s]); ?>
                                <?php else: ?>
                                    <?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Tags','action'=>'index','plugin'=>'System','prefix'=>'Backend',"inactive"]); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </strong>
                    </small>
                </h3>
                <div class="box-tools col-md-4 no-padding">
                    <?= $this->Form->create($tag,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
                    <div class="input-group input-group-sm">
                        <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                        <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
            <div class="box-body no-padding">
                <?= $this->Form->create($tag,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
                <div class="list-action clearfix">
                    <div class="col-md-3 no-padding">
                        <select id="bulk-action" name="action" class="form-control i-combo">
                            <option value=""><?= __d('system',"Bulk Action") ?></option>
                            <option value="1"><?= __d('system',"Remove") ?></option>
                        </select>
                    </div>
                    <div class="col-md-2 no-padding">
                        <button class="btn btn-default" type="submit" id="btn-bulk-action">
                            <?= __d('system',"Apply") ?>
                        </button>
                    </div>
                    <div class="pull-right item-amount">
                        <?php
                        $label = (count($tags)>=2)?__d("system"," items"):__d("system"," item");
                        echo count($tags) . $label;
                        ?>
                    </div>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th class="cb-wrap" width="5%"><input type="checkbox" class="checkbox-custom" id="checkall" /></th>
                        <th><?= __d("system","Name") ?></th>
                        <th><?= __d("system","Description") ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(count($tags)>0): ?>
                        <?php foreach($tags as $t): ?>
                            <tr>
                                <td class="cb-wrap">
                                    <input type="checkbox" class="checkbox-custom checkbox-child" name="value[]" value="<?= $t["id"] ?>"/>
                                </td>
                                <td>
                                    <?= $t["name"] ?>
                                    <div>
                                        <?= $this->Html->link(__d("system","edit"),['controller'=>'Tags','action'=>'modify','plugin'=>'System','prefix'=>'Backend',$t["id"]]) ?>|
                                        <?= $this->Html->link(__d("system","delete"),['controller'=>'Tags','action'=>'remove','plugin'=>'System','prefix'=>'Backend',$t["id"]],['class'=>'text-danger']) ?>
                                    </div>
                                </td>
                                <td>
                                    <?= $t["description"]; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <?= $this->element('System.paginator'); ?>
            </div>
        </div>
    </div>
</div>