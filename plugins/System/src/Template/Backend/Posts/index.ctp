<?= $this->AssetCompress->css('System.Page4345',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Page4345',['block'=>'footer-script']) ?>
<?php
$allAmount = count($allposts->toArray());
$draftAmount = 0;
$reviewAmount = 0;
$publishedAmount = 0;
foreach($allposts as $mdl){
    if($mdl["publishstatus"]==1)
        $draftAmount++;
    elseif($mdl["publishstatus"]==2)
        $reviewAmount++;
    elseif($mdl["publishstatus"]==3)
        $publishedAmount++;
}
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
            <small>
                <strong>
                    <?php if($status==null): ?>
                        <?= __d("system","All ({0})",$allAmount); ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link(__d("system","All ({0})",$allAmount),['controller'=>'Posts','action'=>'search','plugin'=>'System','prefix'=>'Backend',"s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link(__d("system","All ({0})",$allAmount),['controller'=>'Posts','action'=>'index','plugin'=>'System','prefix'=>'Backend']); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    |
                    <?php if($status=="draft"): ?>
                        <?= __d("system","Drafted ({0})",$draftAmount); ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link(__d("system","Drafted ({0})",$draftAmount),['controller'=>'Posts','action'=>'search','plugin'=>'System','prefix'=>'Backend',"draft","s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link(__d("system","Drafted ({0})",$draftAmount),['controller'=>'Posts','action'=>'index','plugin'=>'System','prefix'=>'Backend',"draft"]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    |
                    <?php if($status=="review"): ?>
                        <?= __d("system","Reviewing ({0})",$reviewAmount); ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link(__d("system","Reviewing ({0})",$reviewAmount),['controller'=>'Posts','action'=>'search','plugin'=>'System','prefix'=>'Backend',"review","s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link(__d("system","Reviewing ({0})",$reviewAmount),['controller'=>'Posts','action'=>'index','plugin'=>'System','prefix'=>'Backend',"review"]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    |
                    <?php if($status=="publish"): ?>
                        <?= __d("system","Published ({0})",$publishedAmount); ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link(__d("system","Published ({0})",$publishedAmount),['controller'=>'Posts','action'=>'search','plugin'=>'System','prefix'=>'Backend',"published","s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link(__d("system","Published ({0})",$publishedAmount),['controller'=>'Posts','action'=>'index','plugin'=>'System','prefix'=>'Backend',"published"]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </strong>
            </small>
        </h3>
        <div class="box-tools col-md-4 no-padding">
            <?= $this->Form->create($post,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
            <div class="input-group input-group-sm">
                <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
    <div class="box-body">
        <?= $this->Form->create($post,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
        <div class="list-action clearfix">
            <div class="col-md-2 no-padding">
                <select id="bulk-action" name="action" class="form-control i-combo">
                    <option value=""><?= __d('system',"Bulk Action") ?></option>
                    <option value="1"><?= __d('system',"Remove") ?></option>
                </select>
            </div>
            <div class="col-md-2 no-padding">
                <button class="btn btn-default" type="submit" id="btn-bulk-action">
                    <?= __d('system',"Apply") ?>
                </button>
            </div>
            <div class="pull-right item-amount">
                <?php
                $label = (count($posts)>=2)?__d("system"," items"):__d("system"," item");
                echo count($posts) . $label;
                ?>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="cb-wrap" width="5%"><input type="checkbox" class="checkbox-custom" id="checkall" /></th>
                <th width="40%"><?= __d('system',"Title") ?></th>
                <th width="15%"><?= __d("system","Visibility") ?></th>
                <th width="20%"><?= __d("system","Author") ?></th>
                <th width="55%"><?= __d('system',"Date ") ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($posts)>0): ?>
                <?php foreach($posts as $p): ?>
                    <tr>
                        <td class="cb-wrap">
                            <input type="checkbox" class="checkbox-custom checkbox-child" name="value[]" value="<?= $p["id"] ?>"/>
                        </td>
                        <td>
                            <?php
                            $prefix = "";
                            for($i=2;$i<=($p->level+1);$i++)
                            {
                                $prefix.="- ";
                            }
                            ?>
                            <?=
                            $prefix;
                            ?>
                            <?php
                            $title = $p->title;
                            foreach($p->langs as $body){
                                if($body["languageid"] == $config["SITEDFLTLG_VALUE"])
                                {
                                    $title = $body["title"];
                                    break;
                                }
                            }
                            if(empty($title))
                            {
                                $title = __d("system","[no title]");
                            }
                            echo $title;
                            ?>
                            <strong>
                                <?php
                                if($p->publishstatus==1):
                                    echo __d("system","[Drafted]");
                                elseif($p->publishstatus==2):
                                    echo __d("system","[Editor Review]");
                                elseif($p->publishstatus==3):
                                    echo __d("system","[Published]");
                                endif;
                                ?>
                            </strong>
                            <div>
                                <?= $this->Html->link(__d("system","Edit"),['Controller'=>'Posts','action'=>'modify','plugin'=>'System','prefix'=>'Backend',$p->id]); ?>|
                                <?= $this->Html->link(__d("system","Remove"),['Controller'=>'Posts','action'=>'remove','plugin'=>'System','prefix'=>'Backend',$p->id],['class'=>'text-danger']); ?>|
                                <?= $this->Html->link(__d("system","Preview"),['Controller'=>'Posts','action'=>'preview','plugin'=>'System','prefix'=>'Backend',$p->id],['target'=>'_blank']); ?>
                            </div>
                        </td>
                        <td>
                            <?php
                            switch ($p->visibility){
                                case 1 :
                                    echo __d("system","Public");
                                    break;
                                case 2 :
                                    echo __d("system","Password Protected");
                                    break;
                                case 3 :
                                    echo __d("system","Private");
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <?= $p->createdby ?>
                        </td>
                        <td>
                            <?= $p->date->i18nFormat($config["SITEDTFORM"]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="5">
                        <?php
                        if(isset($s))
                            echo __d('system',"No post found for ''{0}'' keyword.",[$s]);
                        else
                            echo __d('system',"No post available.")
                        ?>
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?= $this->Form->end(); ?>
    </div>
    <div class="box-footer">
        <?= $this->element('System.paginator'); ?>
    </div>
</div>