<?= $this->AssetCompress->script('System.Post23455',['block'=>'footer-script']); ?>
<div class="statusbar">
    <i class="fa fa-eye"></i>
    <?= __d("system","Visibility : ") ?>
    <input type="hidden" id="visibilitystatus" name="visibility" value="<?= $bean->visibility ?>"/>
    <input type="hidden" id="postpassword" name="postpassword" value="<?= $bean->postpassword ?>"/>
    <?php $visibility = [1=>__d("system","Public"),2=>__d("system","Password Protected"),3=>__d("system","Private")]; ?>
    <div id="visibilityTemplate" class="hidden">
        <?php for($i=1;$i<=3;$i++): ?>
            <div><input type="radio" name="visibility" value="<?= $i ?>" id="<?= $i ?>-item" ><label for="<?= $i ?>-item"><?= $visibility[$i] ?></label></div>
        <?php endfor; ?>
    </div>
    <strong id="printedvisibility">
        <?php
        if($bean->visibility == 1)
            echo __d("system","Public");
        else if($bean->visibility == 2)
            echo __d("system","Password Protected");
        else if($bean->visibility == 3)
            echo __d("system","Protected");
        ?>
    </strong>
    <a href="#" id="editVisible"><?= __d("system","Edit"); ?></a>
</div>