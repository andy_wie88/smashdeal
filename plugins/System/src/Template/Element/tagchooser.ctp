<?= $this->Html->script('jquery-ui.min',['block'=>'footer-script']); ?>
<?= $this->Html->script('plugins/tokenize/tokenize2',['block'=>'footer-script']); ?>
<?= $this->Html->script('System.post/tagchooser',['block'=>'footer-script']); ?>
<?= $this->Html->css('plugins/tokenize/tokenize2',['block'=>'css']); ?>
<div class="box">
    <div class="box-header">
        <h2 class="box-title"><?= __d("system", "Tags") ?></h2>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <?php
            function isSelected($bean,$id)
            {
                if(!empty($bean->terms)) {
                    foreach ($bean->terms as $term) {
                        if ($term["id"] == $id) {
                            return true;
                        }
                    }
                }
                return false;
            }
        ?>
        <select id="tokenize" class="tokenize-sample-demo1" name="tags[]" multiple>
            <?php foreach($tags  as $tag): ?>
                <option name="id" value="<?= $tag["id"] ?>" <?= (isSelected($bean,$tag["id"]))?"selected":""; ?> ><?= $tag["name"]; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>