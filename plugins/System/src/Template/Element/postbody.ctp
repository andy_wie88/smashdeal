<?= $this->Html->script('plugins/tinymce/tinymce.min',['block'=>'footer-script']) ?>
<?= $this->Html->script('plugins/tinymce/jquery.tinymce.min',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->script('System.Page5534',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.Page5534',['block'=>'css']) ?>
<?php $index = 0; ?>
<div class="box box-default">
    <div class="box-body">
        <div id="templang" class="hidden">
            <?php $index = 0; ?>
            <?php foreach($langs as $key=>$lang): ?>
                <?php if($key != $config["SITEDFLTLG_VALUE"]): ?>
                    <?php
                        if(!empty($bean->langs))
                        {
                            foreach($bean->langs as $b)
                            {
                                if($b["languageid"]==$key)
                                {
                                    $body = $b;
                                }
                            }
                        }
                    ?>
                    <div id="<?= $key ?>">
                        <input type="hidden" class="hidden-language" value="<?= (isset($body))?$body["languageid"]:$key; ?>" name="langs[<?= $index ?>][languageid]" />
                        <input type="hidden" class="hidden-title" value="<?= (isset($body))?$body["title"]:""; ?>" name="langs[<?= $index ?>][title]" />
                        <textarea type="hidden" class="hidden-body" name="langs[<?= $index ?>][body]">
                            <?= isset($body)?$body["body"]:""; ?>
                        </textarea>
                    </div>
                <?php endif; ?>
                <?php $index++; ?>
            <?php endforeach; ?>
            <?php
                foreach($bean->langs as $b)
                {
                    if($b["languageid"] == $config["SITEDFLTLG_VALUE"])
                    {
                        $defaultlang = $b;
                    }
                }
            ?>
        </div>
        <div class="clearfix min-padding-top no-padding">
            <div class="col-md-4 pull-right no-padding min-padding-bottom">
                <?php $index=0; ?>
                <?php
                foreach($langs as $key=>$lang){
                    if($key==$config["SITEDFLTLG_VALUE"])
                        $defaultlangindex = $index;
                    $index++;
                }
                $index = 0;
                ?>
                <select class="form-control" name="lang" id="langs" data-current="<?= $config["SITEDFLTLG_VALUE"] ?>" data-current-index="<?= $defaultlangindex ?>">
                    <?php foreach($langs as $key=>$lang): ?>
                        <option value="<?= $key ?>" data-index="<?= $index ?>"  <?= ($key==$config["SITEDFLTLG_VALUE"])?"selected":"" ?> ><?= $lang ?></option>
                        <?php
                        $index++;
                        ?>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <?= $this->Form->input('langs.'.$defaultlangindex.'.languageid',['type'=>'hidden','value'=>$config["SITEDFLTLG_VALUE"],'id'=>'language']) ?>
        <div class="form-group">
            <div class="col-md-12">
                <?= $this->Form->input('langs.'.$defaultlangindex.'.title',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d("system","Page Title"),'id'=>'title','value'=>(isset($defaultlang)?$defaultlang["title"]:"")]) ?>
            </div>
        </div>
        <div class="row min-padding-bottom">
        </div>
        <!-- Ini Untuk Widget yang typenya addins post -->
        <div class="form-group">
            <div class="col-md-12">
                <?= $this->Form->input('langs.'.$defaultlangindex.'.body',['label'=>false,'div'=>false,'class'=>'editor','id'=>'editor','value'=>(isset($defaultlang)?$defaultlang["body"]:"")]); ?>
            </div>
        </div>
    </div>
</div>