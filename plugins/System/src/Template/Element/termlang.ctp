<div class="box box-primary">
    <div class="box-header">
        <h2 class="box-title"><?= __d("system","Translation") ?></h2>
    </div>
    <div class="box-body">
        <?php $index = 0; ?>
        <?php foreach($langs as $lang=>$value): ?>
            <div class="form-group">
                <input name="langs[<?= $index ?>][languageid]" value="<?= $lang ?>" type="hidden" />
                <label for="<?= $lang ?>" class="col-md-3 control-label">
                    <?= trim(explode("-",$value)[0]) ?>
                </label>
                <?php
                    if(isset($bean->langs)) {
                        foreach ($bean->langs as $l) {
                            if ($l["languageid"] == $lang) {
                                $b = $l;
                                break;
                            }
                        }
                    }
                ?>
                <div class="col-md-9">
                    <input name="langs[<?= $index ?>][name]" value="<?= (isset($b)?$b->name:"") ?>" class="form-control" />
                </div>
                <?php unset($b); ?>
                <?php $index++; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>