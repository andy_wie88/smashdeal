<div class="widget-box draggable clearfix" data-widget="<?= $widget->code ?>">
    <div class="widget-title">
        <h3><?= $widget->displayname ?></h3>
        <div class="widget-action">
            <a href="">
                <i class="fa fa-caret-down"></i>
            </a>
        </div>
    </div>
    <div class="widget-inside" style="display:none">
        <?php if(isset($widget_config)): ?>
        <?= $this->element('widgetconfig',['widget'=>$widget,'widget_config'=>$widget_config]); ?>
        <?php else: ?>
        <?= $this->element('widgetconfig',['widget'=>$widget]); ?>
        <?php endif; ?>
    </div>
    <div class="widget-description">
        <p><?= $widget->description; ?></p>
    </div>
</div>