<?php
$filename = "";
foreach($bean->metas as $meta) {
    if ($meta["name"] == "file_path") {
        $filename = $meta["value"];
        break;
    }
}
?>

<?php
    if(preg_match("/(mp4|ogv|mkv)$/",$filename)):
        $url = $this->Url->build(DS."files".DS.$filename);
        if(!isset($isajax)) {
            echo $this->Html->script('plugins/mediaelement/mediaelement-and-player.min', ['block' => 'footer-script']);
            echo $this->Html->script('System.media/mediarenderer', ['block' => 'footer-script']);
            echo $this->Html->css('plugins/mediaelement/mediaelementplayer.min', ['block' => 'css']);
        }
        else
        {
            echo $this->Html->script('plugins/mediaelement/mediaelement-and-player.min');
            echo $this->Html->script('System.media/mediarenderer');
            echo $this->Html->css('plugins/mediaelement/mediaelementplayer.min');
        }
        ?>
        <video width="100%" height="400px" style="margin-bottom: 20px;" src="<?= $url ?>" type="<?= $bean->postmime; ?>" id="player1"
               controls="controls" preload="none"></video>
 <?php
    elseif(preg_match("/(mp3|oga)$/",$filename)):
        $url = $this->Url->build(DS."files".DS.$filename);
        if(!isset($isajax)) {
            echo $this->Html->script('plugins/mediaelement/mediaelement-and-player.min', ['block' => 'footer-script']);
            echo $this->Html->script('System.media/mediarenderer', ['block' => 'footer-script']);
            echo $this->Html->css('plugins/mediaelement/mediaelementplayer.min', ['block' => 'css']);
        }
        else
        {
            echo $this->Html->script('plugins/mediaelement/mediaelement-and-player.min');
            echo $this->Html->script('System.media/mediarenderer');
            echo $this->Html->css('plugins/mediaelement/mediaelementplayer.min');
        }
        ?>
        <audio width="100%" style="margin-bottom: 20px;" src="<?= $url ?>" type="<?= $bean->postmime; ?>" id="player1"
       controls="controls" preload="none"></audio>
 <?php
    elseif(preg_match("/(xls?x)$/",$filename)):
        $image = "excel.png";
    elseif(preg_match("/(doc?x|ppt?x|pdf)$/",$filename)):
        $image =  "word.png";
    elseif(preg_match("/(zip|rar|7z)$/",$filename)):
        $image =  "compressed.png";
    elseif(preg_match("/(gif|jpe?g|png)$/",$filename)):
        $image = DS."files".DS.$filename;
    else:
        $file = "file.png";
    endif;
?>
<?php if(preg_match("/(gif|jpe?g|png)$/",$filename)): ?>
    <?php if(!$plain): ?>
        <?= $this->Html->image($image,['class'=>'img','width'=>'100%','style'=>'margin-bottom:10px']) ?>
    <?php else: ?>
        <?= $this->Html->image($image) ?>
    <?php endif; ?>
<?php else: ?>
    <?php if(isset($image)): ?>
        <?= $this->Html->image($image,['class'=>'img','width'=>'128px']) ?>
    <?php endif; ?>
<?php endif; ?>
