<?= $this->AssetCompress->script('System.Post243455',['block'=>'footer-script']); ?>
<?= $this->AssetCompress->css('System.Post243455',['block'=>'css']); ?>
<div class="statusbar">
    <i class="fa fa-calendar"></i>
    <?= __d("system","Publish : ") ?>
    <input type="hidden" id="date" name="date" value="<?= (!empty($bean->date))?$bean->date->i18nFormat($config['SITEDTFORM']):""; ?>"/>
    <strong id="printedtime">
        <?php if(!isset($bean->date)): ?>
            <?= __d("system","immediately") ?>
        <?php else: ?>
            <?=  $bean->date->i18nFormat($config['SITEDTFORM']) ?>
        <?php endif; ?>
    </strong>
    <a href="#" id="editPublishTime"><?= __d("system","Edit"); ?></a>
</div>