<?php
    $config = $widget["config"];
?>
<div class="clearfix">
    <form class="form-horirzontal">
        <div class="form-group">
            <input type="hidden" name="[widgetid]" value="<?= $widget["id"] ?>" />
            <label for=""><?= __d("system","Title:") ?></label>
            <input type="text" name="[config][title]" value="<?= (isset($widget_config["title"])?$widget_config["title"]:$widget["displayname"]) ?>" class="form-control form-control-sm"/>
        </div>
        <?php if(isset($config["configform"])): ?>
            <?= $this->cell($config["configform"],["widget"=>$widget,'widget_config'=>isset($widget_config)?$widget_config:null]) ?>
        <?php endif; ?>
        <div class="padding-top">
            <div class="pull-left">
                <a href="#" class="remove"><?= __d("system","Delete") ?></a>|
                <a href="#" class="close-box"><?= __d("system","Close") ?></a>
            </div>
            <div class="pull-right">
                <button class="submit btn btn-primary btn-sm" type="submit"><?= __d("system","Save") ?></button>
            </div>
        </div>
    </form>
</div>