<div class="form-horizontal">
    <div class="form-group">
        <label class="col-md-4 control-label"><?= __d("system","URL"); ?></label>
        <div class="col-md-8 no-padding-left">
            <input type="text" class="permalink form-control" value="http://" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label"><?= __d("system","Link Label"); ?></label>
        <div class="col-md-8 no-padding-left">
            <input type="text" class="link-label form-control" />
        </div>
    </div>
    <div class="button-group">
        <button id="customlinkadd" class="btn btn-default btn-sm pull-right icms-apply-btn" data-target="<?= $widget->id ?>" data-component-target="#<?= $widget->id ?>-form-component">
            <?= __d("system","Add to Menu") ?>
        </button>
    </div>
</div>