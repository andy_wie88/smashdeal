<div class="form-group">
    <label for="" class="col-md-12">
        <?= __d("system","Navigation Menu Label") ?>
    </label>
    <div class="col-md-12">
        <input type="text" class="form-control" name="menu_details[<?= $index ?>][config][label]" value="<?= isset($menu["config"]["label"])?$menu["config"]["label"]:"" ?>" />
    </div>
</div>
<div class="form-group">
    <label for="" class="col-md-12">
        <?= __d("system","URL") ?>
    </label>
    <div class="col-md-12">
        <input type="text" class="form-control" name="menu_details[<?= $index ?>][permalink]" value="<?= isset($menu["permalink"])?$menu["permalink"]:"" ?>" />
    </div>
</div>