<?php
$defaullang = $this->request->session()->read('Setting')["SITEDFLTLG_VALUE"];
$label = "";
if($menu["config"]["isoriginal"]=="true"){
    foreach($category->term_langs as $lang){
        if($lang["languageid"]==$defaullang){
            $label = $lang["name"];
            break;
        }
    }
}
else{
    $label = $menu["config"]["label"];
}
?>
<?php
$url = ['controller'=>$menuobj["controller"],'action'=>$menuobj["action"],'prefix'=>$menuobj["prefix"],"plugin"=>$menuobj["plugin"]];
$url = array_merge($url,$menu["extraconfig"]);
?>
<?= $this->Html->link($label,$url) ?>