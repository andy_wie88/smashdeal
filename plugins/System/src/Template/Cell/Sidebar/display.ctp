<section class="sidebar">
	<ul class="sidebar-menu">
		<li class="header"><?= __d('system',"Main Navigation"); ?></li>
		<?php foreach($menus as $key=>$menu): ?>			
			<?php if(count($menu->children_menus)>0): ?>
                <li class="treeview <?= ($active->parent_menu["id"]==$menu->id)?"active":"" ?>">
                    <?= $this->element('System.singlelink',['menu'=>$menu]) ?>    
                    <ul class="treeview-menu">
                        <?php foreach($menu->children_menus as $key=>$m): ?>
                        <li class="<?= ($m["id"]==$active->id)?"active":"" ?>">
                            <?= $this->element('System.singlelink',['menu'=>$m]); ?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php else: ?>
                <li class="<?= ($active->id==$menu->id)?"active":""?>">
                    <?= $this->element('System.singlelink',['menu'=>$menu]); ?>
                </li>
            <?php endif; ?>
		<?php endforeach; ?>
	</ul>
</section>