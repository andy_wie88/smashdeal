<div class="form-group">
    <label for=""><?= __d("system","Custom Menu:") ?></label>
    <select class="form-control form-control-sm" name="[config][nav_menu]" value="<?= (isset($widget_config["nav_menu"])?$widget_config["nav_menu"]:"") ?>">
        <option value=""><?= __d("system","--Choose Menu--"); ?></option>
        <?php foreach($nav_menus as $key=>$menu): ?>
            <option value="<?= $key ?>"><?= $menu ?></option>
        <?php endforeach; ?>
    </select>
</div>