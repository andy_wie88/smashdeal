<div class="col-md-12 icms-box no-padding">
    <div class="hidden" id="pagelinkid" data-value="<?= $menu ?>"></div>
    <div class="page-wrapper min-padding-top">
        <?php
        function isChecked($bean,$id,$dftcate)
        {
            if(isset($bean->terms)) {
                foreach ($bean->terms as $term) {
                    if ($term["id"] == $id) {
                        return true;
                    }
                }
            }
            if($bean->id==null || !isset($bean->id))
                if($id==$dftcate)
                    return true;

            return false;
        }
        function generatedList($index = 0,$list,$defaultlng)
        {
            echo "<ul class='tree-list' id='".$index."-list'>";
            foreach($list as $l)
            {
                $label = "";
                foreach($l["langs"] as $lang){
                    if($lang["languageid"]==$defaultlng)
                    {
                        $label = $lang["title"];
                    }
                }
                echo "<li><input id='".$l->id."' type='checkbox' name='terms[][id]' data-post='".$l->id."' data-slug='".$l->slug."' data-label='".$label."' value='".$l->id."' /><label>&nbsp;".$label."</label>";
                $index++;
                if(!empty($l->children))
                {
                    generatedList($index,$l->children);
                }
                echo "</li>";
            }
            echo "</ul>";
        }
        $defaultlang = $this->request->session()->read('Config')["SITEDFLTLG_VALUE"];
        generatedList(0,$pages,$defaultlang);
        ?>
    </div>
</div>
<div class="button-group">
    <button id="pagelinkadd" class="btn btn-default btn-sm pull-right icms-apply-btn" data-target="<?= $widget->id ?>" data-component-target="#<?= $widget->id ?>-form-component">
        <?= __d("system","Add to Menu") ?>
    </button>
</div>