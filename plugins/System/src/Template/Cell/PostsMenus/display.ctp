<div class="col-md-12 icms-box min-padding">
    <div class="hidden" id="postlinkid" data-value="<?= $menu ?>"></div>
    <ul class="icms-none-list">
        <?php foreach($posts as $post): ?>
            <?php
                $defaultlang = $this->request->session()->read('Config')["SITEDFLTLG_VALUE"];
                $label = "";
                foreach($post["langs"] as $lang){
                    if($lang["languageid"]==$defaultlang)
                    {
                        $label = $lang["title"];
                    }
                }
            ?>
            <li>
                <input type="checkbox" data-post="<?= $post->id ?>" data-slug="<?= $post->slug ?>" data-label="<?= $label ?>" data-value='<?= json_encode(["title"=>$post->title]) ?>'/><span class="mlabel"><?= $label ?></span>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="button-group">
    <button id="postlinkadd" class="btn btn-default btn-sm pull-right icms-apply-btn" data-target="<?= $widget->id ?>" data-component-target="#<?= $widget->id ?>-form-component">
        <?= __d("system","Add to Menu") ?>
    </button>
</div>