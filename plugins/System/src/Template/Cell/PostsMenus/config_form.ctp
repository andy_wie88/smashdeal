<div>
    <?php
    $label = "";
    $defaullang = $this->request->session()->read('Config')['SITEDFLTLG_VALUE'];
    foreach($post->langs as $lang){
        if($lang["languageid"]==$defaullang){
            $label =  $lang["title"];
            break;
        }
    }
    ?>
    <div class="form-group">
        <label for="" class="col-md-12">
            <?= __d("system","Navigation Menu Label") ?>
        </label>
        <div class="col-md-12">
            <input type="text" id="<?= $index ?>-label" class="form-control pagelabel" data-target="#<?= $index ?>-isoriginal" name="menu_details[<?= $index ?>][config][label]" value="<?= ($menu["config"]["isoriginal"]=="true")?$label:$menu["config"]["label"] ?>" />
        </div>
    </div>
    <input type="hidden" id="<?= $index ?>-isoriginal" name="menu_details[<?= $index ?>][config][isoriginal]" value="<?= $menu["config"]["isoriginal"] ?>" />
    <input type="hidden" name="menu_details[<?= $index ?>][config][postid]" value="<?= $menu["config"]["postid"] ?>" />
    <div class="icms-info-box">
        <?= __d("system","Orginal : ") ?>
        <a id="<?= $index ?>-reset-original" href="#">
            <?= $label ?>
        </a>
    </div>
    <script>
        var isoriginalclick = false;
        $("#<?= $index ?>-label").change(function(e){
            if(!isoriginalclick){
                $($(this).attr("data-target")).val("false");
            }else {
                console.log("trigger");
                isoriginalclick = false;
            }
        });
        $("#<?= $index ?>-reset-original").click(function(e){
            e.preventDefault();
            isoriginalclick = true;
            $("#<?= $index ?>-isoriginal").val("true");
            $("#<?= $index ?>-label").val($(this).text().trim());
        });
    </script>
</div>