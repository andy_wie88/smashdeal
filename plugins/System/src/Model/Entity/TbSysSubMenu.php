<?php
namespace System\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbSysSubMenu Entity
 *
 * @property string $menuid
 * @property string $submenuid
 * @property int $index
 */
class TbSysSubMenu extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'menuid' => false,
        'submenuid' => false
    ];
}
