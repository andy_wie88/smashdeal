<?php
namespace System\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbSysUserPreference Entity
 *
 * @property string $id
 * @property string $label
 * @property string $value
 * @property string $moduleid
 * @property string $userid
 * @property string $groupname
 * @property int $valuetype
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class TbSysUserPreference extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
