<?php
namespace System\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbSysMenu Entity
 *
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $label
 * @property string $icon
 * @property string $description
 * @property string $keyword
 * @property int $linktype
 * @property int $rendertype
 * @property string $extraconfig
 * @property bool $isneedextra
 * @property int $level
 * @property int $index
 * @property bool $isadmin
 * @property bool $isajax
 * @property bool $isactive
 * @property string $parentid
 * @property string $controller
 * @property string $action
 * @property string $plugin
 * @property string $prefix
 * @property string $url
 * @property string $moduleid
 * @property int $authtype
 * @property string $authref
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 *
 * @property \System\Model\Entity\ChildrenMenu[] $children_menus
 * @property \System\Model\Entity\ParentMenu $parent_menu
 * @property \App\Model\Entity\SubMenu[] $sub_menus
 */
class TbSysMenu extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
