<?php
namespace System\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * TbSysMenuGroupDetail Entity
 *
 * @property string $id
 * @property string $groupid
 * @property int $menutype
 * @property string $menuid
 * @property string $permalink
 * @property string $extraconfig
 * @property string $config
 */
class TbSysMenuGroupDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];

    protected $_virtual = [
        'configForm'
    ];

    protected function _getConfigForm()
    {
        $widget =  TableRegistry::get('System.TbSysWidgets');
        $configForm = ($widget->get($this->_properties["widgetid"])->get("config")!=null)?$widget->get($this->_properties["widgetid"])->get("config")["configForm"]:"";
        return $configForm;
    }
}
