<?php
namespace System\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbSysProfile Entity
 *
 * @property string $id
 * @property string $firstname
 * @property string $lastname
 * @property string $fullname
 * @property string $displayname
 * @property \Cake\I18n\Time $birthday
 * @property string $city
 * @property string $country
 * @property string $address
 * @property string $aboutme
 * @property string $profilepic
 * @property string $website
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class TbSysProfile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}
