<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Database\Schema\Table as Schema;
use Cake\Database\Type;
use Cake\Database\Type\JsonType;
Type::map('json','Cake\Database\Type\JsonType');

/**
 * TbSysMenuGroupDetails Model
 *
 * @method \System\Model\Entity\TbSysMenuGroupDetail get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysMenuGroupDetail newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroupDetail[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroupDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysMenuGroupDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroupDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroupDetail findOrCreate($search, callable $callback = null)
 */
class TbSysMenuGroupDetailsTable extends Table
{

    protected function _initializeSchema(Schema $table)
    {
        $table->columnType('config','json');
        $table->columnType('extraconfig','json');

        return $table;
    }
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_menu_group_details');
        $this->displayField('id');
        $this->primaryKey('id');


        $this->belongsTo('MenuGroup',['className'=>'System.TbSysMenuGroups','foreignKey'=>'groupid']);
        $this->hasOne('Menu',['className'=>'System.TbSysMenus','foreignKey'=>'menuid']);
        /**
         * 1=>static menu
         * 2=>from menu
         */
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('groupid');

        $validator
            ->integer('menutype')
            ->allowEmpty('menutype');

        $validator
            ->allowEmpty('menuid');

        $validator
            ->allowEmpty('permalink');

        $validator
            ->allowEmpty('extraconfig');

        $validator
            ->allowEmpty('config');

        return $validator;
    }
}
