<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysMenuAccesses Model
 *
 * @method \System\Model\Entity\TbSysMenuAccess get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysMenuAccess newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysMenuAccess[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuAccess|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysMenuAccess patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuAccess[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuAccess findOrCreate($search, callable $callback = null)
 */
class TbSysMenuAccessesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_menu_accesses');
        $this->displayField('menuid');
        $this->primaryKey(['menuid', 'userroleid']);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('menuid', 'create');

        $validator
            ->allowEmpty('userroleid', 'create');

        return $validator;
    }
}
