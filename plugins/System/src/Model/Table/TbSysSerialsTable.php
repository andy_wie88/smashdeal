<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysSerials Model
 *
 * @method \System\Model\Entity\TbSysSerial get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysSerial newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysSerial[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysSerial|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysSerial patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysSerial[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysSerial findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysSerialsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_serials');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('pattern', 'create')
            ->notEmpty('pattern');

        $validator
            ->integer('serial')
            ->requirePresence('serial', 'create')
            ->notEmpty('serial');

        $validator
            ->integer('number')
            ->requirePresence('number', 'create')
            ->notEmpty('number');

        $validator
            ->integer('autoreset')
            ->requirePresence('autoreset', 'create')
            ->notEmpty('autoreset');

        $validator
            ->integer('lastresetpath')
            ->requirePresence('lastresetpath', 'create')
            ->notEmpty('lastresetpath');

        $validator
            ->requirePresence('lastgenerated', 'create')
            ->notEmpty('lastgenerated');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
