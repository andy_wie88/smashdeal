<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysUserPasswordLogs Model
 *
 * @method \System\Model\Entity\TbSysUserPasswordLog get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysUserPasswordLog newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysUserPasswordLog[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserPasswordLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysUserPasswordLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserPasswordLog[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserPasswordLog findOrCreate($search, callable $callback = null)
 */
class TbSysUserPasswordLogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_user_password_logs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Uuid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->dateTime('changedate')
            ->requirePresence('changedate', 'create')
            ->notEmpty('changedate');

        $validator
            ->requirePresence('changeat', 'create')
            ->notEmpty('changeat');

        $validator
            ->allowEmpty('userid');

        return $validator;
    }
}
