<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysMenuRightAccesses Model
 *
 * @method \System\Model\Entity\TbSysMenuRightAccess get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysMenuRightAccess newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRightAccess[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRightAccess|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysMenuRightAccess patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRightAccess[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRightAccess findOrCreate($search, callable $callback = null)
 */
class TbSysMenuRightAccessesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_menu_right_accesses');
        $this->displayField('menurightid');
        $this->primaryKey(['menurightid', 'userroleid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('menurightid', 'create');

        $validator
            ->allowEmpty('userroleid', 'create');

        return $validator;
    }
}
