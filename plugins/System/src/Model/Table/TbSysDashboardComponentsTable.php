<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysDashboardComponents Model
 *
 * @method \System\Model\Entity\TbSysDashboardComponent get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysDashboardComponent newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardComponent[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardComponent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysDashboardComponent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardComponent[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardComponent findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysDashboardComponentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_dashboard_components');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        
        $this->belongsTo('Modules',['className'=>'System.TbSysModules','foreignKey'=>'moduleid']);
        $this->belongsTo('Themes',['className'=>'System.TbSysThemes','foreignKey'=>'moduleid']);

        $this->belongsToMany('Users',[
            'className'=>'System.TbSysUsers',
            'joinTable'=>'tb_sys_dashboard_authorizations',
            'foreignKey'=>'dashboardcomponentid',
            'targetForeignKey'=>'userid',
            'saveStrategy'=>'replace',
            'dependent'=>true
        ]);

        $this->belongsToMany('Dashboards',[
            'className'=>'System.TbSysDashboards',
            'joinTable'=>'tb_sys_dashboard_details',
            'foreignKey'=>'dashboardcomponentid',
            'targetForeignKey'=>'dashboardid',
            'saveStrategy'=>'replace',
            'dependent'=>true
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('note');

        $validator
            ->allowEmpty('cell');

        $validator
            ->allowEmpty('plugin');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->allowEmpty('createby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
