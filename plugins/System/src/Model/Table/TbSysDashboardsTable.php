<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysDashboards Model
 *
 * @method \System\Model\Entity\TbSysDashboard get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysDashboard newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysDashboard[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboard|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysDashboard patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboard[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboard findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysDashboardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_dashboards');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->belongsTo('TbSysUser',[
            'className'=>'System.TbSysUsers',
            'foreignKey'=>'userid'
        ]);

        $this->belongsToMany("DashboardComponents",[
            'className'=>'System.TbSysDashboardComponents',
            'joinTable'=>'tb_sys_dashboard_details',
            'foreignKey'=>'dashboardid',
            'targetForeignKey'=>'dashboardcomponentid',
            'saveStrategy'=>'replace',
            'dependent'=>true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('userid', 'create')
            ->notEmpty('userid');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->allowEmpty('createby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findStatus($query, array $options)
    {
        $s = strtolower($options['s']);
        $status = (strtolower($s)=="active")?TRUE:FALSE;
        $query->contain('TbSysUser');
        $query->where(['isactive'=>$status]);
        return $query;
    }

    public function findOwner($query, array $options)
    {
        $s = strtolower($options['s']);
        $o = $options['o'];
        $query->contain('TbSysUser');
        $status = (strtolower($s)=="active")?TRUE:FALSE;
        $query->where(['and'=>['isactive'=>$status,'userid'=>$o]]);
        return $query;
    }

    public function findName($query, array $options)
    {

        $s = strtolower($options['s']);
        $status = $options["status"];
        $query->contain('TbSysUser');
        $o = (isset($options["o"]))?$options["o"]:null;

        if($status==null)
        {
            if($o==null)
            {
                $query->where([
                    'or'=>
                        ['lower(name) LIKE'=>'%'.$s.'%'],
                        ['lower(description) LIKE'=>'%'.$s.'%'],
                        ['lower(TbSysUser.username) LIKE'=>"%".$s."%"]
                    ]);
            }
            else
            {
                $query->where([
                    'and'=>[
                        'or'=>[
                            ['lower(name) LIKE'=>'%'.$s.'%'],
                            ['lower(description) LIKE'=>'%'.$s.'%'],
                            ['lower(TbSysUser.username) LIKE'=>"%".$s."%"]
                        ],
                        'userid' => $o]
                    ]);
            }
        }
        else
        {
            $bools = (strtolower($status)=="active")?TRUE:FALSE;
            if($o==null){
                $query->where(
                    [
                    'and'=>[
                        ['or'=>
                            ['lower(name) LIKE'=>'%'.$s.'%'],
                            ['lower(description) LIKE'=>'%'.$s.'%'],
                            ['lower(TbSysUser.username) LIKE'=>"%".$s."%"]
                        ],
                        'TbSysDashboards.isactive'=>$bools]
                    ]);
            }
            else
            {
                $query->where([
                    'and'=>[
                        'or'=>[
                            ['lower(name) LIKE'=>'%'.$s.'%'],
                            ['lower(description) LIKE'=>'%'.$s.'%'],
                            ['lower(TbSysUser.username) LIKE'=>"%".$s."%"]
                        ],
                        'TbSysDashboards.isactive'=>$bools,
                        "userid"=>$o
                    ]]);
            }
        }
        return $query;
    }
}
