<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysWidgetGroups Model
 *
 * @method \System\Model\Entity\TbSysWidgetGroup get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroup newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroup[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroup[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroup findOrCreate($search, callable $callback = null)
 */
class TbSysWidgetGroupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_widget_groups');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->hasMany('Details',['className'=>'System.TbSysWidgetGroupDetails','foreignKey'=>'groupid','saveStrategy'=>'replace','dependent'=>true]);

        $this->belongsToMany('Widgets',
        [
            'className'=>'System.TbSysWidgets',
            'joinTable'=>'tb_sys_widget_group_details',
            'foreignKey'=>'groupid',
            'targetForeignKey'=>'widgetid',
            'sort'=>['TbSysWidgetGroupDetails.index'=>'ASC'],
            'saveStrategy'=>'replace'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('moduleid', 'create')
            ->notEmpty('moduleid');

        $validator
            ->boolean('isadmin')
            ->requirePresence('isadmin', 'create')
            ->notEmpty('isadmin');

        $validator
            ->allowEmpty('note');

        $validator
            ->allowEmpty('label');

        return $validator;
    }
}
