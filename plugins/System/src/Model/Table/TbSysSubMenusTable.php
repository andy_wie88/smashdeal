<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysSubMenus Model
 *
 * @method \System\Model\Entity\TbSysSubMenu get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysSubMenu newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysSubMenu[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysSubMenu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysSubMenu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysSubMenu[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysSubMenu findOrCreate($search, callable $callback = null)
 */
class TbSysSubMenusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_sub_menus');
        $this->displayField('menuid');
        $this->primaryKey(['menuid', 'submenuid']);

        $this->belongsTo('HostMenu',['className'=>'System.TbSysMenus','foreignKey'=>'menuid']);
        $this->belongsTo('SubMenu',['className'=>'System.TbSysMenus','foreignKey'=>'submenuid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('menuid', 'create');

        $validator
            ->allowEmpty('submenuid', 'create');

        $validator
            ->integer('index')
            ->requirePresence('index', 'create')
            ->notEmpty('index');

        return $validator;
    }
}
