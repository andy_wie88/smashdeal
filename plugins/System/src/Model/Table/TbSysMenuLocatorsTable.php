<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysMenuLocators Model
 *
 * @method \System\Model\Entity\TbSysMenuLocator get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysMenuLocator newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysMenuLocator[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuLocator|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysMenuLocator patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuLocator[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuLocator findOrCreate($search, callable $callback = null)
 */
class TbSysMenuLocatorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_menu_locators');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Uuid');

        $this->hasOne('MenuGroup',['className'=>'Smashdeal.TbSysMenuGroups','foreignKey'=>'menugroupid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->allowEmpty('label');

        $validator
            ->allowEmpty('note');

        $validator
            ->requirePresence('themeid', 'create')
            ->notEmpty('themeid');

        $validator
            ->allowEmpty('menugroupid');

        return $validator;
    }
}
