<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysModules Model
 *
 * @method \System\Model\Entity\TbSysModule get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysModule newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysModule[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysModule|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysModule patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysModule[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysModule findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysModulesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_modules');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->hasMany('System.TbSysMenus',['dependent'=>true,'cascadeCallbacks' => true,'foreignKey'=>'moduleid']);
        $this->hasMany('System.TbSysDashboardComponents',['dependent'=>true,'cascadeCallbacks' => true,'foreignKey'=>'moduleid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('displayname', 'create')
            ->notEmpty('displayname');

        $validator
            ->allowEmpty('description');

        $validator
            ->boolean('isactive')
            ->requirePresence('isactive', 'create')
            ->notEmpty('isactive');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findStatus($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = (strtolower($s)=="active")?TRUE:FALSE;
        $query->where(['isactive'=>$status]);
        return $query;
    }

    public function findName($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        if($status==null)
            $query->where(['or'=>[['lower(name) LIKE'=>'%'.$s.'%'],['lower(displayname) LIKE'=>'%'.$s.'%']]]);
        else{
            $bools = (strtolower($status)=="active")?TRUE:FALSE;
            $query->where(
                [
                'AND'=>
                    ['or'=>[
                            ['lower(name) LIKE'=>'%'.$s.'%'],
                            ['lower(displayname) LIKE'=>'%'.$s.'%']
                        ]
                    ],
                    'isactive'=>$bools
                ]);
        }
        return $query;
    }
}
