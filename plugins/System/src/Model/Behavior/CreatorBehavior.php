<?php
namespace System\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

/**
 * Creator behavior
 */
class CreatorBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
}
