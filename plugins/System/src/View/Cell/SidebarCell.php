<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * Sidebar cell
 */
class SidebarCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
    	$this->loadModel('System.TbSysMenus');
        $controller = $this->request->params["controller"];
        $action = ($this->request->action=="search")?"index":$this->request->params["action"];
        $plugin = $this->request->params["plugin"];
        $prefix =  $this->request->params["prefix"];
    	$menus = $this->TbSysMenus->find('all',['contain'=>['ChildrenMenus'=>function($q)
    		{
    			return $q->order(['ChildrenMenus.index'=>'ASC'])->where(['ChildrenMenus.isactive'=>true,'ChildrenMenus.isajax'=>false]);
    		}]])->where(['TbSysMenus.isadmin'=>true,'TbSysMenus.level'=>1,'TbSysMenus.isactive'=>true])->order(['TbSysMenus.index'=>'ASC']);
        
        $activeMenu = $this->TbSysMenus->find('all',['contain'=>['ParentMenu']])->where(['and'=>['TbSysMenus.controller'=>$controller,'TbSysMenus.action'=>$action,'TbSysMenus.plugin'=>$plugin,'TbSysMenus.prefix'=>$prefix]])->first();
        if($activeMenu->level == 3)
        {
            $id = $activeMenu->parent_menu["id"];
            $activeMenu = $this->TbSysMenus->get($id,['contain'=>'ParentMenu']);
        }

    	$this->set('__serialize','menus');
    	$this->set('menus',$menus->toArray());
    	$this->set('active',$activeMenu);
    }
}
