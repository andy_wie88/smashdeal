<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * PostLangRenderer cell
 */
class LangRendererCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($source = null,$sourcetype = null,$field = null, $length = null)
    {
        $defaultlang = $this->request->session()->read('Setting')['SITEDFLTLG_VALUE'];
        if($source == null || $sourcetype == null || $defaultlang == null || $field == null){
            $this->set('label','');
            return;
        }
        if($sourcetype == 'POST')
        {
            if(!isset($source['langs'])){
                $this->set('label','');
                return;
            }
            foreach($source['langs'] as $lang)
            {
                if($lang['languageid']==$defaultlang)
                {
                    if(isset($lang[$field])){
                        $label = $this->clearString($lang[$field],$length);
                        $this->set('label',$label);
                        return;
                    }
                    else
                    {
                        $this->set('label','');
                    }
                }
                $this->set('label',$source[$field]);
            }
        }
        else if($sourcetype == 'TERM')
        {

            if(isset($source['term_langs']))
            {
                if(!isset($source['term_langs'])){
                    $this->set('label','');
                    return;
                }
                foreach($source['term_langs'] as $lang)
                {
                    if($lang['languageid']==$defaultlang){
                        if(isset($lang[$field]))
                        {
                            $label = $this->clearString($lang[$field],$length);
                            $this->set('label',$label);
                            return;
                        }
                        else
                        {
                            $this->set('here','jerer');
                            $this->set('label','');
                        }
                    }
                }
                $this->set('label',$source[$field]);
            }
            else if(isset($source['langs'])) 
            {
                if(!isset($source['langs'])){
                    $this->set('label','');
                    return;
                }
                foreach($source['langs'] as $lang)
                {
                    if($lang['languageid']==$defaultlang)
                    {
                        if(isset($lang[$field]))
                        {
                            $label = $this->clearString($lang[$field],$length);
                            $this->set('label',$label);
                            return;
                        }
                        else
                        {
                            $this->set('label','');
                        }
                    }
                }
                $this->set('label',$source[$field]);
            }
        }
    }

    private function clearString($label, $length = null)
    {
        $label = strip_tags($label);
        if($length!=null){
            $labels = explode(" ", $label);
            $temp = '';
            for($i=0;$i<$length;$i++)
            {
                $temp .= " ".$labels[$i];
            }
            $label = $temp;
        }
        return $label;
    }
}
