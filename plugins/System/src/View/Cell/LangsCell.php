<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * Langs cell
 */
class LangsCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
    }
}
