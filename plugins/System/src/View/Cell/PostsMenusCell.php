<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * PostsMenus cell
 */
class PostsMenusCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($widget)
    {
        $this->loadModel('System.TbSysPosts');
        $posts = $this->TbSysPosts->find('all',['contain'=>'Langs'])->where(['and'=>['posttype'=>2,'publishstatus'=>3]]);
        $this->loadModel('System.TbSysMenus');
        $menu = $this->TbSysMenus->find('all')->where(['code'=>'DISPLAYPOST'])->first();

        $this->set('menu',$menu->id);
        $this->set('posts',$posts);
        $this->set('widget',$widget);
    }

    public function configForm($menu,$index)
    {
        $this->loadModel('System.TbSysPosts');
        $post = $this->TbSysPosts->get($menu["config"]["postid"],['contain'=>'Langs']);
        $this->set('post',$post);
        $this->set('menu',$menu);
        $this->set('index',$index);
    }

    public function menuRenderer($widget,$menu)
    {
        $this->loadModel('System.TbSysMenus');
        $this->loadModel('System.TbSysPosts');
        if($menu['config']["isoriginal"]=="true") {
            $post = $this->TbSysPosts->get($menu["config"]["postid"],['contain'=>'PostLangs']);
            $this->set('post',$post);
        }
        $menuobj = $this->TbSysMenus->get($menu["menuid"]);
        $this->set('menuobj',$menuobj);
        $this->set('menu',$menu);
        $this->set('widget',$widget);
    }
}
