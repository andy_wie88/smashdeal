<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * Custommenu cell
 */
class CustommenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $this->loadModel('System.TbSysTerms');
        $nav_menus = $this->TbSysTerms->find('list')->where(['taxonomy'=>'nav_menu']);
        $this->set('nav_menus',$nav_menus);
    }

}
