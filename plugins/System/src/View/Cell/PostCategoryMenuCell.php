<?php
namespace System\View\Cell;

use Cake\View\Cell;
use System\Controller\Component\TermsComponent;

/**
 * PostCategoryMenu cell
 */
class PostCategoryMenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($widget)
    {
        $this->loadModel('System.TbSysTerms');
        $term = new TermsComponent();
        $allcategories = $this->TbSysTerms->find('type', ['type' => "category"])->contain('TermLangs')->where(['TbSysTerms.isactive' => true])->order(['TbSysTerms.created' => 'ASC']);
        $results = $term->collectCategoryNested($allcategories->toArray());
        $this->loadModel('System.TbSysMenus');
        $menu = $this->TbSysMenus->find('all')->where(['code'=>'POSTCATEGORYLIST'])->first();

        $this->set('menu',$menu->id);
        $this->set('categories',$results);
        $this->set('widget',$widget);
    }

    public function configForm($menu,$index)
    {
        $this->loadModel('System.TbSysTerms');
        $category = $this->TbSysTerms->get($menu["config"]["categoryid"],['contain'=>'TermLangs']);
        $this->set('category',$category);
        $this->set('menu',$menu);
        $this->set('index',$index);
    }

    public function menuRenderer($widget,$menu)
    {
        $this->loadModel('System.TbSysMenus');
        $this->loadModel('System.TbSysTerms');
        if($menu['config']["isoriginal"]=="true") {
            $category = $this->TbSysTerms->get($menu["config"]["categoryid"],['contain'=>'TermLangs']);
            $this->set('category',$category);
        }
        $menuobj = $this->TbSysMenus->get($menu["menuid"]);
        $this->set('menuobj',$menuobj);
        $this->set('menu',$menu);
        $this->set('widget',$widget);
    }
}
