<?php

namespace System\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class WritingSettingForm extends Form {

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField("DFTCATE.id","string")
            ->addField("DFTCATE.value","string");
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator->notEmpty('DFTCATE.id',__d("system","Invalid Form"));
        $validator->notEmpty('DFTCATE.value',__d("system","Default Category can't be empty"));
        return $validator;
    }

    protected function _execute(array $data)
    {
        return true;
    }

}