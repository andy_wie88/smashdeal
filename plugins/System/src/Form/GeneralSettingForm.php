<?php

namespace System\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class GeneralSettingForm extends Form {

	protected function _buildSchema(Schema $schema)
	{

		return $schema->addField("SITETITLE.id","string")
			->addField("SITETITLE.value","string")
			->addField("SITETAGLN.id","string")
			->addField("SITETAGLN.value","string")
			->addField("TIMEZONE.id","string")
			->addField("TIMEZONE.value","string")
			->addField("SITEDTFORM.id","string")
			->addField("SITEDTFORM.value","string")
			->addField("SITETMFORM.id","string")
			->addField("SITETMFORM.value","string")
			->addField("SITEWKDAYS.id","string")
			->addField("SITEWKDAYS.value","string")
			->addField("SITEDFLTLG.id","string")
			->addField("SITEDFLTLG.value","string")
			->addField("SITEICON.id","string")
			->addField("SITEICON.value","string");
	}

	protected function _buildValidator(Validator $validator)
	{
		$validator->notEmpty('SITETITLE.id',__d("system","Invalid Form"));
		$validator->notEmpty('SITETITLE.value',__d("system","Site Title can't be empty"));
		$validator->notEmpty('SITETAGLN.id',__d("system","Invalid Form"));
		$validator->notEmpty('SITETAGLN.value',__d("system","Site Tagline can't be empty"));
		$validator->notEmpty('TIMEZONE.id',__d("system","Invalid Form"));
		$validator->notEmpty('TIMEZONE.value',__d("system","Site Tagline can't be empty"));
		$validator->notEmpty('SITEDTFORM.id',__d("system","Invalid Form"));
		$validator->notEmpty("SITEDTFORM.value",__d("system","Please choose one date format"));
		$validator->notEmpty("SITETMFORM.id",__d("system","Invalid Form"));
		$validator->notEmpty("SITETMFORM.value",__d("system","Please choose one time format"));
		$validator->notEmpty("SITEWKDAYS.id",__d("system","Invalid Form"));
		$validator->notEmpty("SITEWKDAYS.value",__d("system","Please choose first day of the week"));
		$validator->notEmpty("SITEDFLTLG.id",__d("system","Invalid Form"));
		$validator->notEmpty("SITEDFLTLG.value",__d("system","Please choose website default language"));
		$validator->notEmpty("SITEICON.id",__d("system","Invalid Form"));
		$validator->notEmpty("SITEICON.value",__d("system","Please choose website icon"));
		return $validator;
	}

	protected function _execute(array $data)
	{
		return true;
	}

}