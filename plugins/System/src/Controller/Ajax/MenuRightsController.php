<?php

namespace System\Controller\Ajax;

use System\Controller\BackendAppController;

class MenuRightsController extends BackendAppController
{

	public function initialize()
    {
    	parent::initialize();
    	$this->loadModel('System.TbSysMenuRights');
    }

    public function getActiveMenuRight()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;

    	$menus = $this->TbSysMenuRights->find('all',['contain'=>'TbSysMenus']);
    	
    	echo json_encode($menus->toArray());
    }

}