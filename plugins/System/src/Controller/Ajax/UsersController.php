<?php
namespace System\Controller\Ajax;

use System\Controller\BackendAppController as AppController;

/**
 * Users Controller
 *
 * @property \System\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('System.TbSysUsers');
    }

    public function getActiveUsers()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;

        $users = $this->TbSysUsers->find('all',['contain'=>'Profile'])->where(['TbSysUsers.isactive'=>true]);
        
        echo json_encode($users->toArray());
    }

}