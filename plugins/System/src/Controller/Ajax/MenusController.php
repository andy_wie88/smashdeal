<?php

namespace System\Controller\Ajax;

use Cake\Utility\Text;
use System\Controller\BackendAppController as AppController;

class MenusController extends AppController
{

	public function initialize()
    {
    	parent::initialize();
    	$this->loadModel('System.TbSysMenus');
    }

    public function getActiveMenu()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;

    	$menus = $this->TbSysMenus->find('all',['contain'=>'ParentMenu'])->where(['and'=>['TbSysMenus.linktype'=>1,'TbSysMenus.isactive'=>true,'TbSysMenus.authtype'=>1]]);
    	
    	echo json_encode($menus->toArray());
    }

    public function renderMenuSelector()
    {
        $this->ViewBuilder()->autoLayout(false);
        $menus = $this->request->data["menu"];
        foreach($menus as $key=>$menu)
        {
            $menus[$key]["id"] =  Text::uuid();
        }
        $this->set('menus',$menus);
    }

}