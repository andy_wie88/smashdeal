<?php

namespace System\Controller\Ajax;

use System\Controller\BackendAppController;

class UserRolesController extends BackendAppController
{
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('System.TbSysUserRoles');
	}

   	public function dynamicView($id)
    {
    	$this->viewBuilder()->autoLayout(false);
    	
    	$userRole = $this->TbSysUserRoles->get($id,['contain'=>['AccessMenus','MenuRights']]);

    	$this->set('userRole',$userRole);
    }

	public function getActiveUserRole()
	{
		$this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;

    	$userRoles = $this->TbSysUserRoles->find('all')->where(['TbSysUserRoles.isactive'=>true]);
    	
    	echo json_encode($userRoles->toArray());
    }
}