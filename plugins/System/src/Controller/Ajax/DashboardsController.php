<?php
namespace System\Controller\Ajax;

use System\Controller\BackendAppController;

/**
 * Dashboards Controller
 *
 * @property \System\Model\Table\DashboardsTable $Dashboards
 */
class DashboardsController extends BackendAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('System.TbSysDashboardComponents');
        $this->loadModel('System.TbSysDashboards');
    }

    public function getActiveDashboard()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        $dashboards = $this->TbSysDashboardComponents->find('all')->where(["isactive"=>true])->order('name','ASC');
    	echo json_encode($dashboards->toArray());
    }

    public function updateDashboardPosition()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        if($this->request->is("post"))
        {
            debug($this->request->data);
            $dashboard = $this->TbSysDashboards->get($this->request->data["id"],['contain'=>'DashboardComponents']);
            $dashboard = $this->TbSysDashboards->patchEntity($dashboard,$this->request->data);
            if($this->TbSysDashboards->save($dashboard))
            {
                $message = ["status"=>true];
            }
            else
            {

                $message = ["status"=>false];
            }
            echo json_encode($message);
        }
    }

    public function getAuthorizedDashboard($userid = null)
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        $dashboards = $this->TbSysDashboardComponents->find('all')->innerJoinWith('Users',function($q) use($userid)
        {
            return $q->where(['Users.id'=>$userid]);
        })->where(["TbSysDashboardComponents.isactive"=>true,])->order('name','ASC');
        echo json_encode($dashboards->toArray());
    }

}
