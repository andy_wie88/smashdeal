<?php

namespace System\Controller\Component;

use Cake\Controller\Component;

class TermsComponent extends Component
{
    public function __construct()
    {

    }

    public function collectCategories($allcategories,$tail = "—",$parent = null,$oldtail = null)
    {
        $results = [];
        if($parent==null)
        {
            foreach($allcategories as $key=>$category)
            {
                if($category->parentid == null)
                {
                    array_push($results,$category);
                    $children = $this->collectCategories($allcategories,$tail,$category->id,$tail);
                    foreach ($children as $child) {
                        array_push($results, $child);
                    }
                }
            }
        }
        else{
            foreach($allcategories as $key=>$category)
            {
                if($category->parentid == $parent)
                {
                    $category->name = $oldtail.$category->name;
                    array_push($results,$category);
                    $children = $this->collectCategories($allcategories,$tail,$category->id,$oldtail.$tail);
                    foreach($children as $child)
                    {
                        array_push($results,$child);
                    }
                }
            }
        }
        return $results;
    }

    public function collectAsListOptions($allcategories)
    {
        foreach($allcategories as $category)
        {
            $result[$category["id"]] = $category["name"];
        }
        return (isset($result))?$result:null;
    }

    public function collectCategoryNested($allcategories,$parent = null)
    {
        $results = [];
        foreach ($allcategories as $key=>$category)
        {
            if($parent==null)
            {
                if($category->parentid == null) {
                    $category["children"] = [];
                    array_push($results, $category);
                    $category["children"] = $this->collectCategoryNested($allcategories,$category->id);
                }
            }
            else{
                if($category->parentid == $parent)
                {
                    $category["children"] = [];
                    array_push($results,$category);
                    $category["children"] = $this->collectCategoryNested($allcategories,$category->id);
                }
            }
        }
        return $results;
    }
}