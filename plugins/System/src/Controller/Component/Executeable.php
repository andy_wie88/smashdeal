<?php
/**
 * Created by PhpStorm.
 * User: zeno
 * Date: 06/10/16
 * Time: 19:13
 */

namespace System\Controller\Component;

interface Execute{

    public function execute();

}