<?php
namespace System\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Pages component
 */
class PagesComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function __construct()
    {
    }

    public function collectPageNested($allpages,$parent = null)
    {
        $results = [];
        foreach ($allpages as $key=>$post)
        {
            if($parent==null)
            {
                if($post->parentid == null) {
                    $post["children"] = [];
                    array_push($results, $post);
                    $post["children"] = $this->collectPageNested($allpages, $post->id);
                }
            }
            else{
                if($post->parentid == $parent)
                {
                    $post["children"] = [];
                    array_push($results,$post);
                    $post["children"] = $this->collectPageNested($allpages, $post->id);
                }
            }
        }
        return $results;
    }
}
