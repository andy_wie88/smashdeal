<?php

namespace System\Controller\Backend;

use System\Controller\BackendAppController as AppController;
use Cake\Utility\Inflector;

class DashboardsController extends AppController
{

	public function initialize()
	{
		parent::initialize();
		$this->loadModel('System.TbSysDashboards');
	}

	public function index($id = null)
	{
		if($id == null){
			$dashboard = $this->TbSysDashboards->find('all',['contain'=>['DashboardComponents'=>function($q){
					return $q->where(["DashboardComponents.isactive"=>true])->order("TbSysDashboardDetails.index","ASC");
				}]])->where(
					["and"=>["TbSysDashboards.userid"=>$this->Auth->user('id')],["TbSysDashboards.isdefault"=>true],["TbSysDashboards.isactive"=>true]]
				)->first();
		}
		else
		{
			$dashboard = $this->TbSysDashboards->find('all',['contain'=>['DashboardComponents'=>function($q){
					return $q->where(["DashboardComponents.isactive"=>true])->order("TbSysDashboardDetails.index","ASC");
				}]])->where(['TbSysDashboards.slug'=>$id])->first();
		}
		$dashboards = $this->TbSysDashboards->find('list',[
		    'keyField' => 'slug',
		    'valueField' => 'name'
		])->where(["and"=>["TbSysDashboards.userid"=>$this->Auth->user('id')],["TbSysDashboards.isactive"=>true]]);
		$this->set('dashboards',$dashboards);
		$this->set("dashboard",$dashboard);
	}

	public function listDash($status = null)
	{
		$this->paginate = [
            'limit' => 10,
            'contain' => 'TbSysUser'
		];
		$dashboard = $this->TbSysDashboards->newEntity();
		if($this->isHasRight("DASHBOARDMGR"))
			$this->set('isMgr',true);
		else {
			$dashboard->userid = $this->Auth->user('id');
			$this->set('isMgr',false);
		}
		if($this->isHasRight("DASHBOARDMGR"))
		{
			if($status != null){		
            	$this->paginate = [
	                'limit' => 10,
	                'finder' => [
	                    'status'=>['s'=>$status]
	                ]
	            ];
			}
			$alldashboards = $this->TbSysDashboards->find('all');
		}
		else
		{
			if($status != null){		
	            $this->paginate = [
	                'limit' => 10,
	                'finder' => [
	                    'owner'=>['s'=>$status,'o'=>$this->Auth->user('id')]
	                ]
	            ];
			}
			$alldashboards = $this->TbSysDashboards->find('all')->where(["userid"=>$this->Auth->user('id')]);
		}
		$dashboards = $this->paginate($this->TbSysDashboards);
		$this->set('alldashboards',$alldashboards);
		$this->set('dashboards',$dashboards);
    	$this->set('dashboard',$dashboard);
    	$this->set('status',$status);

	}

	public function search($status = null)
	{
 		$q = $this->request->query('s');
		$dashboard = $this->TbSysDashboards->newEntity();
		if($this->isHasRight("DASHBOARDMGR"))
			$this->set('isMgr',true);
		else {
			$dashboard->userid = $this->Auth->user('id');
			$this->set('isMgr',false);
		}
		if($this->isHasRight("DASHBOARDMGR")){
	        $this->paginate = [
	            'finder'=> [
	                'name'=>['s'=>$q,'status'=>$status]
	            ]
	        ];
			$alldashboards = $this->TbSysDashboards->find('name',['s'=>$q,'status'=>null]);
	    }
	    else
	    {
	    	$this->paginate = [
	            'finder'=> [
	                'name'=>['s'=>$q,'status'=>$status,'o'=>$this->Auth->user('id')]
	            ]
	        ];
			$alldashboards = $this->TbSysDashboards->find('name',['s'=>$q,'status'=>null,'o'=>$this->Auth->user('id')]);
	    }
		$dashboards = $this->paginate($this->TbSysDashboards);
		$this->set('alldashboards',$alldashboards);
		$this->set('dashboards',$dashboards);
    	$this->set('dashboard',$dashboard);
    	$this->set('status',$status);
        $this->set('s',$q);
        $this->render('list_dash');
	}

	public function create()
	{
		$dashboard = $this->TbSysDashboards->newEntity();
		if($this->isHasRight("DASHBOARDMGR"))
			$this->set('isMgr',true);
		else {
			$dashboard->userid = $this->Auth->user('id');
			$this->set('isMgr',false);
		}
		if($this->request->is("post"))
		{
			$dashboard = $this->TbSysDashboards->patchEntity($dashboard,$this->request->data);
			$dashboard->slug = Inflector::slug($dashboard->name);
			if($this->TbSysDashboards->save($dashboard))
			{
				$this->Flash->success(__d("system",'Successfully create a new dashboard'));
				$this->redirect(['action'=>'listDash']);
			}
			else
			{
				$this->Flash->success(__d("system",'Fail  to create a new dashboard'));
			}
		}
		$this->set('dashboard',$dashboard);
	}

	public function modify($id = null)
	{
		$dashboard = $this->TbSysDashboards->get($id,['contain'=>['DashboardComponents'=>function($q){
				return $q->order("TbSysDashboardDetails.index","ASC");
			}]]);
		if($this->request->is(["post","put","patch"]))
		{
			$dashboard = $this->TbSysDashboards->patchEntity($dashboard,$this->request->data);
			$dashboard->slug = Inflector::slug($dashboard->name);
			if($this->TbSysDashboards->save($dashboard))
			{
				$this->Flash->success(__d("system",'Successfully create a new dashboard'));
				$this->redirect(['action'=>'listDash']);
			}
			else
			{
				$this->Flash->success(__d("system",'Fail  to create a new dashboard'));
			}
		}
		$this->set('dashboard',$dashboard);
	}

	public function remove($id = null)
	{
		$dashboard = $this->TbSysDashboards->get($id);
		if($this->TbSysDashboards->delete($dashboard))
		{
			$this->Flash->success(__d("system","Successfully remove dashboards"));
		}
		else
		{
			$this->Flash->error(__("system","Fail to remove dashboards"));
		}
		$this->redirect(["action"=>"listDash"]);
	}

	public function changeStatus($status, $id = null)
	{
		if($id==null)
            throw new InternalErrorException("Null id is not allowed");
        $boolStatus = false;
        switch ($status) {
            case 'inactive':
                $boolStatus = false;
                break;
            case 'active':
                $boolStatus = true;
                break;
            default:
                throw new NotImplementedException($status+" not implemented yet");
                break;
        }

        $user = $this->TbSysDashboards->get($id);
        $user->isactive = $boolStatus;
        if($this->TbSysDashboards->save($user))
        {
            $this->Flash->success(__d('system',"Success to update dashboard's status"));
            $this->redirect(["action"=>"listDash"]);
        }
        else
        {
            $this->Flash->error(__d('system',"Fail to update dashboard's status"));
            $this->redirect(["action"=>'listDash']);
        }
	}

	public function setDefault($id = null)
	{
		$dashboard = $this->TbSysDashboards->get($id);
		$dashboard->isdefault = 1;
		if($dashboard->isactive == true){
			if($this->TbSysDashboards->save($dashboard))
			{
				$this->TbSysDashboards->updateAll(["isdefault"=>0],["id !="=>$id]);
				$this->Flash->success(__d("system","Successfully set to default"));
				$this->redirect(["action"=>"listDash"]);
			}
			else
			{
				$this->Flash->error(__d("system","Fail to set to default"));
				$this->redirect(["action"=>"listDash"]);
			}
		}
		else
		{
			$this->Flash->error(__d("system","Can't set dashboard to default"));
			$this->redirect(["action"=>"listDash"]);
		}
	}

	public function bulkAction()
	{
		$this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        if($this->request->is("post"))
        {
            $action = $this->request->data["action"];
            if($action == 1)
            {
                //validate if has right to do action.
                if(true)
                {
                    if($this->bulkChangeStatus($this->request->data["value"],true))
                    {
                        $this->Flash->success(__d("system","Success activate dashboards"));
                        $this->redirect(["action"=>"listDash"]);
                    }
                    else
                    {
                        $this->Flash->success(__d("system","Failed to activate dashboards"));
                        $this->redirect(["action"=>"listDash"]);
                    }
                }
            }
            else if($action == 2)
            {
                //validate if has right to do action.
                if(true)
                {
                    if($this->bulkChangeStatus($this->request->data["value"],false))
                    {
                        $this->Flash->success(__d("system","Success deactivate dashboards"));
                        $this->redirect(["action"=>"listDash"]);
                    }
                    else
                    {
                        $this->Flash->success(__d("system","Failed to deactivate dashboards"));
                        $this->redirect(["action"=>"listDash"]);
                    }
                }
            }
        }
	}

	private function bulkChangeStatus($values,$value)
    {
        if($this->TbSysDashboards->updateAll(["isactive"=>$value],["id IN"=>$values]))
            return true;
        return false;
    }
}