<?php

namespace System\Controller\Backend;


use Cake\Network\Exception\InternalErrorException;
use System\Controller\BackendAppController as AppController;

class UserRolesController extends AppController
{

	public $paginate = [
		"limit" => 10
	];

	public function initialize()
	{
		parent::initialize();
		$this->loadModel('System.TbSysUserRoles');
	}

	public function index($status = null)
	{
		$this->paginate = [
			"limit" => 10
		];

		$userRole = $this->TbSysUserRoles->newEntity();
		$allUserRoles = $this->TbSysUserRoles->find('all');

		if($status != null){
			$this->paginate = [
                'limit' => 10,
	            'finder' => [
	                'status'=>['s'=>$status]
	            ]
	        ];
		}
		
        $userRoles = $this->paginate($this->TbSysUserRoles);

        $this->set('allUserRoles',$allUserRoles);
        $this->set('userRoles',$userRoles);
        $this->set('userRole',$userRole);
        $this->set('status',$status);
	}

	public function search($status = null)
	{
		$q = $this->request->query('s');
        $userRole = $this->TbSysUserRoles->newEntity();
        $allUserRoles = $this->TbSysUserRoles->find('name',['s'=>$q,'status'=>null]);
        $this->paginate = [
        	'limit'=>10,
            'finder'=> [
                'name'=>['s'=>$q,'status'=>$status]
            ]
        ];
        $userRoles = $this->paginate($this->TbSysUserRoles);
        $this->set('allUserRoles',$allUserRoles);
        $this->set('userRoles',$userRoles);
        $this->set('userRole',$userRole);
        $this->set('status',$status);
        $this->set('s',$q);
        $this->render('index');
	}

	public function create()
	{
		$userRole = $this->TbSysUserRoles->newEntity();
		if($this->request->is("post"))
		{
			$userRole = $this->TbSysUserRoles->patchEntity($userRole,$this->request->data);
			if($this->TbSysUserRoles->save($userRole))
			{
				$this->Flash->success(__d('system',"User Role successfully added"));
				$this->redirect(['action'=>'view',$userRole->id]);
			}
			else
			{
				$this->Flash->error(__d('system',"User Role fail to be added"));
			}
		}
		$this->set('userRole',$userRole);
	}

	public function modify($id)
	{
        $this->setExtraUrlConfig([$id]);
		$userRole = $this->TbSysUserRoles->get($id,['contain'=>['AccessMenus','MenuRights']]);
		if($this->request->is(['post','put']))
		{
			$userRole = $this->TbSysUserRoles->patchEntity($userRole,$this->request->data);
			if($this->TbSysUserRoles->save($userRole))
			{
				$this->Flash->success(__d('system',"User Role successfully added"));
				$this->redirect(['action'=>'view',$userRole->id]);
			}
			else
			{
				$this->Flash->error(__d('system',"User Role fail to be added"));
			}
		}
		$this->set('userRole',$userRole);
	}

	public function view($id)
	{	
    	$userRole = $this->TbSysUserRoles->get($id,['contain'=>['AccessMenus','MenuRights']]);

    	$this->set('userRole',$userRole);
	}

	public function remove($id)
	{
		$userRole = $this->TbSysUserRoles->get($id,['contain'=>'TbSysUsers']);
		if($this->request->is(["post","put"]))
		{
			if(!empty($this->request->data["userroleid"])){

				$ids = [];
				foreach($userRole->tb_sys_users as $key=>$user)
				{
					$ids[] = $user["id"];
				}
				$this->loadModel('System.TbSysUsers');
				try{
					if(count($userRole->tb_sys_users)>0){
						$this->TbSysUsers->updateAll(['userroleid'=>$this->request->data["userroleid"]],['id IN'=>$ids]);
					}
					$this->TbSysUserRoles->delete($userRole);
				}
				catch(InternalErrorException $ex)
				{
					$this->Flash->error(__d('system',"Fail to remove user role"));
				}
				$this->redirect(['action'=>'index']);
			}
			else
			{
				$this->Flash->error(__d('system',"Please choose replacement user role"));
			}
		}
		$this->set('userRole',$userRole);
	}

    public function changeStatus($status,$id = null)
    {
        if($id==null)
            throw new InternalErrorException("Null id is not allowed");
        $boolStatus = false;
        switch ($status) {
            case 'inactive':
                $boolStatus = false;
                break;
            case 'active':
                $boolStatus = true;
                break;
            default:
                throw new NotImplementedException($status+" not implemented yet");
                break;
        }

        $user = $this->TbSysUserRoles->get($id);
        $user->isactive = $boolStatus;
        if($this->TbSysUserRoles->save($user))
        {
            $this->Flash->success(__d('system',"Success to update user role's status"));
            $this->redirect(["action"=>"view",$id]);
        }
        else
        {
            $this->Flash->error(__d('system',"Fail to update user role's status"));
            $this->redirect(["action"=>'view',$id]);
        }
    }

    public function bulkAction()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        if($this->request->is("post"))
        {
            $action = $this->request->data["action"];
            if($action == 1)
            {
                //validate if has right to do action.
                if(true)
                {
                    if($this->bulkChangeStatus($this->request->data["value"],true))
                    {
                        $this->Flash->success("Success activate user role");
                        $this->redirect(["action"=>"index"]);
                    }
                    else
                    {
                        $this->Flash->success("Failed to activate user role");
                        $this->redirect(["action"=>"index"]);
                    }
                }
            }
            else if($action == 2)
            {
                //validate if has right to do action.
                if(true)
                {
                    if($this->bulkChangeStatus($this->request->data["value"],false))
                    {
                        $this->Flash->success("Success deactivate user role");
                        $this->redirect(["action"=>"index"]);
                    }
                    else
                    {
                        $this->Flash->success("Failed to deactivate user role");
                        $this->redirect(["action"=>"index"]);
                    }
                }
            }
        }
    }

    private function bulkChangeStatus($values,$value)
    {
        if($this->TbSysUserRoles->updateAll(["isactive"=>$value],["id IN"=>$values]))
            return true;
        return false;
    }

}