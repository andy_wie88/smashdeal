<?php

namespace System\Controller\Backend;

use System\Controller\BackendAppController as AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Utility\Text;
use ZipArchive;

class ModulesController extends AppController
{

	public function initialize()
    {
    	parent::initialize();
    	$this->loadModel('System.TbSysModules');
    }

	public function index($status = null)
	{
		$module = $this->TbSysModules->newEntity();
		$allmodules = $this->TbSysModules->find('all');
		if($status == null){		
			$modules = $this->paginate($this->TbSysModules);
		}
		else{
			$this->paginate = [
	            'finder'=> [
	                'status'=>['s'=>$status]
	            ]
	        ];
	        $modules = $this->paginate($this->TbSysModules);
		}
		$this->set('allmodules',$allmodules);
		$this->set('modules',$modules);
		$this->set('module',$module);
		$this->set('status',$status);
	}

	public function import()
	{
        $module = $this->TbSysModules->newEntity();

        if($this->request->is("post"))
        {
            $uuid = Text::Uuid();
            $file = $this->request->data["file"];
            if(isset($file["tmp_name"])) {
                $filename = $file["name"];
                $newFile = TMP . DS . "plugin" . DS . $file["name"];
                $destFile = TMP . DS . "plugin" . DS . "tmp-" . $uuid;
                if ($file["type"] == "application/zip") {

                    move_uploaded_file($file["tmp_name"],$newFile);
                    $zip = new ZipArchive();
                    $res = $zip->open($newFile);
                    if($res == true)
                    {
                        $zip->extractTo($destFile);
                        $zip->close();
                        $file = new File($newFile);
                        $file->delete();
                    }
                    if(file_exists($destFile.DS."config.ini"))
                    {
                        $config = parse_ini_file($destFile.DS."config.ini",true);

                        $folderName = $config["Config"]["folder"];

                        $isQualified = $this->isQualified($destFile.DS.$folderName,$config);

                        if($isQualified["status"])
                        {
                            $conn = ConnectionManager::get('default');
                            $conn->begin();


                            /** Core Query **/
                            if(file_exists($destFile.DS.$folderName.DS."config".DS."schema".DS."core.sql")){
                                $core =  file_get_contents($destFile.DS.$folderName.DS."config".DS."schema".DS."setup.sql");
                                $cores =  explode("---  Separator ---", $core);


                                foreach($cores as $key=>$sql)
                                {
                                    $conn->query($sql);
                                }
                            }
                            /** End Of Core Query **/

                            /** Setup Query **/
                            if(file_exists($destFile.DS.$folderName.DS."config".DS."schema".DS."setup.sql")){
                                $setup =  file_get_contents($destFile.DS.$folderName.DS."config".DS."schema".DS."setup.sql");
                                $setups =  explode("---  Separator ---", $setup);


                                foreach($setups as $key=>$sql)
                                {
                                    $conn->query($sql);
                                }
                            }
                            /** End Of Setup Queries **/

                            $folder = new Folder($destFile.DS.$folderName);
                            $folder->copy(PLUGINS.DS.$folderName);

                            $conn->commit();
                            $this->redirect(["action"=>"index"]);
                        }
                        else
                        {
                            $this->Flash->error($isQualified["message"]);
                        }
                    }
                    else{
                        $this->Flash->error(__d("system","Invalid Plugin"));
                    }
                }
                else{
                    $this->Flash->error(__d("system","Invalid File Type"));
                }

                $folder = new Folder($destFile);
                $folder->delete();
            }
            else{
                $this->Flash->error(__d("system","Invalid File"));
            }
        }

        $this->set('module',$module);
	}

	private function isQualified($folder,$config)
	{
	    $modules = $config["Dependencies"]["modules"];
		foreach($modules as $module)
		{
			if($this->TbSysModules->find('all')->where(["code"=>$module])->isEmpty())
			{
			    $first = true;
			    $moduleString = "";
                foreach($modules as $m)
                {
                    if(!$first)
                        $moduleString += ",";
                    $moduleString .= $m;
                    $first = false;
                }
				return ["status"=>false,"message"=>__d("system","One or More Module Depedencies is not fullfited. Here is the depedencies {0}",$moduleString)];
			}
		}

		if(!isset($config["Config"]["code"]))
		    return ["status"=>false,"message"=>__d("system","Invalid Plugin")];

        if(!$this->TbSysModules->find('all')->where(['code'=>$config["Config"]["code"]])->isEmpty())
            return ["status"=>false,"message"=>__d("system","Plugin exist")];

        if(file_exists($folder.DS."config".DS."schema".DS."core.sql") && !file_exists($folder.DS."config".DS."schema".DS."destroy.sql"))
            return ["status"=>false,"message"=>__d("system","Invalid Plugin")];

		return ["status"=>true];
	}

	public function search($status = null)
	{
		$q = $this->request->query('s');
		$module = $this->TbSysModules->newEntity();
		$allmodules = $this->TbSysModules->find('name',['s'=>$q,'status'=>null]);
		$this->paginate = [
            'finder'=> [
                'name'=>['s'=>$q,'status'=>$status]
            ]
        ];
        $modules = $this->paginate($this->TbSysModules);
        $this->set('allmodules',$allmodules);
		$this->set('modules',$modules);
		$this->set('module',$module);
		$this->set('status',$status);
		$this->set('s',$q);
		$this->render('index');
	}

	public function changeStatus($id,$status)
	{
		if($id==null)
            throw new InternalErrorException("Null id is not allowed");
        $boolStatus = false;
        switch ($status) {
            case 'inactive':
                $boolStatus = false;
                break;
            case 'active':
                $boolStatus = true;
                break;
            default:
                throw new NotImplementedException($status+" not implemented yet");
                break;
        }
		$module = $this->TbSysModules->get($id,['contain'=>['TbSysMenus','TbSysDashboardComponents']]);
		if($module->code == "ICMSCORE")
		{
			$this->Flash->error(__d('system',"Can't change icms core plugin status"));
		}
		else
		{
			$module->isactive = $boolStatus;
			if($this->TbSysModules->save($module)){
				$this->TbSysModules->TbSysMenus->updateAll(['isactive'=>$boolStatus],['moduleid'=>$module->id]);
				$this->TbSysModules->TbSysDashboardComponents->updateAll(['isactive'=>$boolStatus],['moduleid'=>$module->id]);
				$this->Flash->success(__d('system',"Success to change plugin status"));
			}
			else
			{
				$this->Flash->error(__d('system',"Fail to change plugin status"));
			}
		}
		$this->redirect($this->referer());
	}

	public function remove($id){

		if($id!=null)
		{
			$module = $this->TbSysModules->get($id);
			if($module!=null)
			{
				$folderName = $module->name;
				if($this->TbSysModules->delete($module))
				{
                    if(file_exists(PLUGINS.DS.$folderName.DS."config".DS."schema".DS."destroy.sql"))
                    {
                        $conn = ConnectionManager::get("default");
                        $conn->open();
                        $destroy =  file_get_contents(PLUGINS.DS.$folderName.DS."config".DS."schema".DS."destroy.sql");
                        $destroys =  explode("---  Separator ---", $destroy);


                        foreach($destroys as $key=>$sql)
                        {
                            $conn->query($sql);
                        }

                        $conn->commit();
                    }

					$folder = new Folder(PLUGINS.DS.$folderName);
					$folder->delete();

					$this->Flash->success('Plugin successfully removed');
					$this->redirect(['action'=>'index']);
				}
				else
				{
					$this->redirect(["action"=>"index"]);
					$this->Flash->error('Fail to remove plugin');
				}
			}
			else
			{
				$this->redirect(["action"=>"index"]);
				$this->Flash->error('Fail to remove plugin');
			}
		}

	}

}