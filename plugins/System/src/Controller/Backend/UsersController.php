<?php

namespace System\Controller\Backend;

use System\Controller\BackendAppController as AppController;
use Cake\Utility\Text;
use Cake\I18n\Time;
use Cake\Network\Exception\NotImplementedException;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\NotFoundException;
use Cake\Auth\DefaultPasswordHasher;

class UsersController extends AppController
{
    public $paginate = [
        'limit' => 10
    ];

	public function initialize()
    {
    	parent::initialize();
    	$this->loadModel('System.TbSysUsers');
        $this->loadComponent('Csrf');
        $this->loadComponent('System.Date');
    }

    public function index($status = null)
    {
		$this->paginate = [
            'limit' => 10,
			'contain'=>['Profile']
		];
    	$user = $this->TbSysUsers->newEntity();
    	$allusers = $this->TbSysUsers->find('all');
		if($status != null){		
            $this->paginate = [
                'limit' => 10,
                'finder' => [
                    'status'=>['s'=>$status]
                ]
            ];
		}
    
        $users = $this->paginate($this->TbSysUsers);
		
		$this->set('allusers',$allusers);
		$this->set('users',$users);
    	$this->set('user',$user);
    	$this->set('status',$status);
    }

    public function search($status = null)
    {
        $q = $this->request->query('s');
        $user = $this->TbSysUsers->newEntity();
        $allusers = $this->TbSysUsers->find('name',['s'=>$q,'status'=>null]);
        $this->paginate = [
            'finder'=> [
                'name'=>['s'=>$q,'status'=>$status]
            ]
        ];
        $users = $this->paginate($this->TbSysUsers);
        $this->set('allusers',$allusers);
        $this->set('users',$users);
        $this->set('user',$user);
        $this->set('status',$status);
        $this->set('s',$q);
        $this->render('index');
    }

    public function create()
    {
    	$user = $this->TbSysUsers->newEntity();
        $user->email = "";
     	if($this->request->is("post")) 
    	{
            $password = 'admin123';
            $this->request->data["password"] = $password;
            $this->request->data["confirm_password"] = $password;
            $this->request->data = $this->compactData($this->request->data);
            $user = $this->TbSysUsers->patchEntity($user,$this->request->data);
            if($this->TbSysUsers->save($user))
            {
                //send password lo..$password to email
                $this->Flash->success(__d('system',"New user successfully added"));
                $this->redirect(['controller'=>'Users','action'=>'view','plugin'=>'System','prefix'=>'Backend',$user->id]);
            }
            else
            {
                $this->Flash->error(__d('system',"Fail to add new user"));
            }
    	}
    	$this->set('user',$user);
    }

    public function modify($id = null)
    {
        if($id==null)
            throw new InternalErrorException("Null id is not allowed");
        $this->setExtraUrlConfig([$id]);
        $user = $this->TbSysUsers->get($id,['contain'=>'Profile']);
        if($this->request->is(["post","put","patch"]))
        {
            $this->request->data["id"]= $id;
            $this->request->data = $this->compactData($this->request->data);      
            $user = $this->TbSysUsers->patchEntity($user,$this->request->data);
            if($this->TbSysUsers->save($user))
            {
                $this->Flash->success(__d('system',"User successfully modified"));
                $this->redirect(['controller'=>'Users','action'=>'view','plugin'=>'System','prefix'=>'Backend',$user->id]);
            }
            else
            {
                $this->Flash->error(__d('system',"Fail to modify user"));
            }
        }

        $this->set('user',$user);
    }

    private function compactData($data)
    {
        if(!empty($data["profile"]["birthday"]))
        {
            $timestamp = $this->Date->convert($data["profile"]["birthday"],$session = $this->request->session()->read('Config'));
            $time = new Time($timestamp);
            $data["profile"]["birthday"] = $time;
        }
        return $data;
    }

    public function view($id = null)
    {
        if($id==null)
            throw new InternalErrorException("Null id is not allowed");

        $this->setExtraUrlConfig([$id]);
        $user = $this->TbSysUsers->get($id,['contain'=>['Profile','Role']]);
        $this->set('user',$user);        

        if($this->request->is("ajax")){
            $this->viewBuilder()->autoLayout(false);
            $this->render('dynamic_view');
        }
    }

    public function changeStatus($status,$id = null)
    {
        if($id==null)
            throw new InternalErrorException("Null id is not allowed");
        $boolStatus = false;
        switch ($status) {
            case 'inactive':
                $boolStatus = false;
                break;
            case 'active':
                $boolStatus = true;
                break;
            default:
                throw new NotImplementedException($status+" not implemented yet");
                break;
        }

        $user = $this->TbSysUsers->get($id);
        $user->isactive = $boolStatus;
        if($this->TbSysUsers->save($user))
        {
            $this->Flash->success(__d('system',"Success to update user's status"));
            $this->redirect(["action"=>"view",$id]);
        }
        else
        {
            $this->Flash->error(__d('system',"Fail to update user's status"));
            $this->redirect(["action"=>'view',$id]);
        }
    }

    public function changePassword()
    {
        if($this->request->is(["post","patch","put"]))
        {
            $userid = $this->Auth->user('id');
            $user = $this->TbSysUsers->get($userid);
            if($user!=null) {
                $password = $this->request->data["password"];
                $new_password = $this->request->data["new_password"];
                $confirm_password = $this->request->data["confirm_password"];
                $valid = true;
                if(!(new DefaultPasswordHasher)->check($password,$user->password))
                {
                    $valid = false;
                    $this->set("change_error",__d("smashdeal","Don't match current password"));
                }
                else if($new_password!=$confirm_password)
                {
                    $valid = false;
                    $this->set("change_error",__d("smashdeal","Password and Confirm Password doesn't match"));
                }
                if($valid)
                {
                    $user->password = $new_password;
                    if($this->TbSysUsers->save($user))
                    {
                        $this->Flash->success(__d("smashdeal","Password Changed"));
                    }
                    else{
                        $this->Flash->error(__d("smashdeal","Fail to Change Password"));
                    }
                }
            }
        }
        $this->set('user',$user);
        $this->render('profile');
    }

    public function resetPassword($id = null)
    {
        if($id==null)
            throw new InternalErrorException("Null id is not allowed");
        $user = $this->TbSysUsers->get($id);
        if(empty($user))
            throw new NotFoundException("User with id "+$id+" not found at the server");
        if($this->request->is(["post","put","patch"])){
            $user->isresetpass = true;
            $password = $this->request->data["password"];
            $confirm_password = $this->request->data["confirm_password"];
            if($password==$confirm_password){
                $user->password = $password;
                if($this->TbSysUsers->save($user))
                {
                    //Send email to user the new password
                    $this->Flash->success(__d('system',"Success to reset user's password"));
                    $this->redirect(["action"=>"view",$id]);
                }
                else
                {
                    $this->Flash->error(__d('system',"Fail to reset user's password"));
                    $this->redirect(["action"=>'view',$id]);
                }
            }
            else
            {
                $this->Flash->error(__d("system","Password and Confirm Password don't match"));
            }
        }
        $this->set('user',$user);
    }

    public function bulkAction()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        if($this->request->is("post"))
        {
            $action = $this->request->data["action"];
            if($action == 1)
            {
                //validate if has right to do action.
                if(true)
                {
                    if($this->bulkChangeStatus($this->request->data["value"],true))
                    {
                        $this->Flash->success("Success activate users");
                        $this->redirect(["action"=>"index"]);
                    }
                    else
                    {
                        $this->Flash->success("Failed to activate users");
                        $this->redirect(["action"=>"index"]);
                    }
                }
            }
            else if($action == 2)
            {
                //validate if has right to do action.
                if(true)
                {
                    if($this->bulkChangeStatus($this->request->data["value"],false))
                    {
                        $this->Flash->success("Success deactivate users");
                        $this->redirect(["action"=>"index"]);
                    }
                    else
                    {
                        $this->Flash->success("Failed to deactivate users");
                        $this->redirect(["action"=>"index"]);
                    }
                }
            }
        }
    }

    private function bulkChangeStatus($values,$value)
    {
        if($this->TbSysUsers->updateAll(["isactive"=>$value],["id IN"=>$values]))
            return true;
        return false;
    }

    public function firsttime()
    {
        $user = $this->TbSysUsers->get($this->Auth->user('id'),['contain'=>'Profile']);
        if(!$user->isfirsttime)
            $this->redirect($this->referer());
        if($this->request->is("put"))
        {
            $this->request->data = $this->compactData($this->request->data);
            if(!empty($user->profile["profilepic"]))
            {
                unlink(WWW_ROOT.DS."files".DS.$user->profile["profilepic"]);
            }
            if(!empty($this->request->data["profile"]["profilepic"]))
            {
                $img = $this->request->data["profile"]["profilepic"];
                $uuid = Text::uuid();
                $ext = $this->getExtension($img["type"]);
                $newFilename = WWW_ROOT.DS."files".DS.$uuid.$ext;

                $this->request->data["profile"]["profilepic"] = $uuid.$ext;

                move_uploaded_file($img["tmp_name"], $newFilename);
            }
            $user = $this->TbSysUsers->patchEntity($user,$this->request->data);
            $user->isfirsttime = false;
            if($this->TbSysUsers->save($user))
            {
                $this->Flash->success(__d('system',"Welcome to iCms Admin Page"));
                $this->redirect(["action"=>"logout"]);
            }
            else
            {
                $this->Flash->error(__d('system',"You have make mistake,please check your entry"));
            }
        }
        $this->set('user',$user);
    }

    public function login()
    {
        $this->viewBuilder()->layout('login');
        $user = $this->TbSysUsers->newEntity();
        $this->set('user',$user);
        if($this->request->is("get"))
        {
            $this->request->session()->write('referer',$this->referer());
            $this->render('login_step_one');
        }
        else if($this->request->is("post"))
        {
            if($this->request->data["phrase"] == "one")
            {
                $user = $this->TbSysUsers->find('all')->contain(['Profile'])->where([
                    'and'=>
                        ['username'=>$this->request->data["username"],
                        'isactive'=>true
                        ]
                    ])->first();
                if($user==null)
                {
                    $this->set('error',__d('system',"Username is not available"));
                    $this->render('login_step_one');
                }
                else
                {
                    $this->set('user',$user);
                    $this->render('login_step_two');
                }
            }
            else if($this->request->data["phrase"] == "two")
            {
                $user = $this->Auth->identify();
                if($user)
                {
                    $this->request->session()->delete("Flash.auth");
                    $this->Auth->setUser($user);
                    if($user["isfirsttime"]){
                        $this->redirect(["controller"=>"Users","action"=>"firsttime","prefix"=>"Backend","plugin"=>"System"]);
                    }
                    $referer = $this->request->session()->read('referer');
                    $this->request->session()->delete('referer');
                    if(strpos($referer,"login")!==false && strpos($referer,"logout") !== false)
                    {
                        $this->redirect($referer);
                    }
                    else
                        $this->redirect(["controller"=>"Dashboards","action"=>"index",'plugin'=>"System","prefix"=>"Backend"]);
                }
                else
                {
                    $user = $this->TbSysUsers->find('all')->contain(['Profile'])->where([
                    'and'=>
                        ['username'=>$this->request->data["username"],
                        'isactive'=>true
                        ]
                    ])->first();
                    $this->set('user',$user);
                    $this->set('error',__d('system',"Username or password is invalid"));
                    $this->render('login_step_two');
                }
            }
            else
            {
                $this->set('error',__d('system',"Username is not found"));
                $this->render('login_step_one');
            }
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function profile($id)
    {
        $user = $this->TbSysUsers->get($id,['contain'=>['Profile','Role']]);
        if($this->request->is("put"))
        {
            $this->request->data = $this->compactData($this->request->data);
            if(!empty($user->profile["profilepic"]))
            {
                unlink(WWW_ROOT.DS."files".DS.$user->profile["profilepic"]);
            }
            if(!empty($this->request->data["profile"]["profilepic"]))
            {
                $img = $this->request->data["profile"]["profilepic"];
                $uuid = Text::uuid();
                $ext = $this->getExtension($img["type"]);
                $newFilename = WWW_ROOT.DS."files".DS.$uuid.$ext;

                $this->request->data["profile"]["profilepic"] = $uuid.$ext;

                move_uploaded_file($img["tmp_name"], $newFilename);
            }
            $user = $this->TbSysUsers->patchEntity($user,$this->request->data);
            if($this->TbSysUsers->save($user))
            {
                $this->Flash->success(__d('system',"Profile Successfully updated"));
            }
            else
            {
                $this->Flash->error(__d('system',"You have make mistake,please check your entry"));
            }
        }
        $this->set('user',$user);
    }

    private function getExtension($type)
    {
        switch ($type) {
            case 'image/jpeg':
                return ".jpeg";
                break;
            case 'image/png':
                return ".png";
                break;
            default:
                return ".jpeg";
                break;
        }
    }

    public function dashboardAuth($id = null)
    {
        $user = $this->TbSysUsers->get($id,['contain'=>['DashboardComponents']]);
        if($this->request->is(["post","put","patch"]))
        {
            $user = $this->TbSysUsers->patchEntity($user,$this->request->data);
            if($this->TbSysUsers->save($user))
            {
                $this->Flash->success(__d("system","Successfully added dashboard auth"));
                $this->redirect(["action"=>"dashboardAuth",$id]);
            }
            else
            {
                $this->Flash->error(__d("system","Fail to save dashboard auth"));
            }
        }
        $this->set('user',$user);
    }

}