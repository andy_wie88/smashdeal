<?php

namespace System\Controller\Backend;

use System\Controller\BackendAppController as AppController;
use System\Form\GeneralSettingForm;
use System\Form\MediaSettingForm;
use System\Form\ReadingSettingForm;
use System\Form\UserPreferenceForm;
use System\Form\WritingSettingForm;

class ConfigsController extends AppController
{

	public function initialize()
	{
		parent::initialize();
		$this->loadModel('System.TbSysConfigs');
		$this->loadModel('System.TbSysUserPreferences');
        $this->loadComponent('System.Terms');
	}

	private function collectConfigs($groupname)
	{
		$rawconfigs = $this->TbSysConfigs->find('all')->where(['groupname'=>$groupname]);
		return $this->groupingConfigs($rawconfigs);
	}

	private function groupingConfigs($rawconfigs)
	{
		$configs = [];
		foreach($rawconfigs->toArray() as $key=>$config)
		{
			$configs[$config["code"]]["id"] = $config["id"];
			$configs[$config["code"]]["value"] = $config["value"];
		}
		return $configs;
	}

	private function collectLookupValue($groupname,$value,$orderby)
	{
		$this->loadModel('System.TbSysLookupDetails');
		$rawconfigs = $this->TbSysLookupDetails->find('list',[
				"keyField"=>"id",
				"valueField"=>$value
			])->contain('TbSysLookup')->where(["TbSysLookup.code"=>$groupname])->order(['TbSysLookupDetails.'.$orderby=>"ASC"]);
		return $rawconfigs;
	}

	private function isDirty($ori,$key,$value)
	{
		if($ori[$key]["value"] == $value)
		{
			return false;
		}
		return true;
	}

	public function general()
	{
		$configs = $this->collectConfigs("GENERAL");
		$timezones = $this->collectLookupValue('TIMEZONE',"value","value");
		$dateformats = $this->collectLookupValue('DATEFORM',"value","label");
		$timeformats = $this->collectLookupValue('TIMEFORM',"value","label");
		$weekdays = $this->collectLookupValue('WEEKDAY',"label","value");
        $this->loadModel('System.TbSysLookupDetails');
        $this->loadModel('System.TbSysWebLanguages');
        $rweblangs = $this->TbSysWebLanguages->find('all')->select('languageid')->toArray();
        $weblangs = [];
        foreach($rweblangs as $lang)
        {
            $weblangs[] = $lang["languageid"];
        }
        $languages = $this->TbSysLookupDetails->find('list',[
                "keyField"=>"id",
				"valueField"=>"label"
			])->where(['TbSysLookupDetails.id IN'=>$weblangs]);
		$generalConfigForm = new GeneralSettingForm();
		if($this->request->is("post","put"))
		{
			try{
				foreach($this->request->data as $key=>$value)
				{
					if($this->isDirty($configs,$key,$value["value"]))
					{
						$data = $this->TbSysConfigs->get($value["id"]);
						$data->value = $value["value"];
						$this->TbSysConfigs->save($data);
					}
				}
				//rewrite all config at session & cookies;
				$this->rewriteConfiguration(true);
				$this->Flash->success(__d("system","Successfully update configurations"));
				$this->redirect(["action"=>"general"]);
			}
			catch(Exception $ex)
			{
				$this->Flash->error(__d("system","Fail to update configurations"));
			}
		}	
		else if($this->request->is("get"))
		{
			$this->request->data = $configs;
		}
		$this->set('generalConfig',$generalConfigForm);
		$this->set('timezones',$timezones);
		$this->set('dateformats',$dateformats);
		$this->set('timeformats',$timeformats);
		$this->set('weekdays',$weekdays);
		$this->set('languages',$languages);
	}

	private function collectUserPreferences()
	{
		$rawconfigs = $this->TbSysUserPreferences->find('all')->where(["userid"=>$this->Auth->user('id')]);
		$configs = [];
		foreach($rawconfigs->toArray() as $key=>$config)
		{
			$configs[$config["groupname"]]["id"] = $config["id"];
			$configs[$config["groupname"]]["value"] = $config["value"];
		}
		return $configs;
	}

	public function userpreference()
	{
		$userpreferences = $this->collectUserPreferences();
		$colorschemas = [
			[
				"value" => "skin-blue",
				"text" => __d("system","Blue")
			],
			[
				"value" => "skin-blue-light",
				"text" => __d("system","Blue Light")
			],
			[
				"value" => "skin-yellow",
				"text" => __d("system","Yellow")
			],
			[
				"value" => "skin-yellow-light",
				"text" => __d("system","Yellow Light")
			],
			[
				"value" => "skin-green",
				"text" => __d("system","Green")
			],
			[
				"value" => "skin-green-light",
				"text" => __d("system","Green Light")
			],
			[
				"value" => "skin-purple",
				"text" => __d("system","Purple")
			],
			[
				"value" => "skin-purple-light",
				"text" => __d("system","Purple Light")
			],
			[
				"value" => "skin-red",
				"text" => __d("system","Red")
			],
			[
				"value" => "skin-red-light",
				"text" => __d("system","Red Light")
			],
			[
				"value" => "skin-black",
				"text" => __d("system","Black")
			],
			[
				"value" => "skin-black-light",
				"text" => __d("system","Black Light")
			]
		];
		$this->loadModel('System.TbSysInterfaceLanguages');
		$interfacelanguage = $this->TbSysInterfaceLanguages->find('all')->select('languageid');
		$langs = [];
		foreach($interfacelanguage->toArray() as $interface)
		{
			$langs[] = $interface["languageid"];
		}

		$this->loadModel('System.TbSysLookupDetails');
		$rawconfigs = $this->TbSysLookupDetails->find('list',[
				"keyField"=>"id",
				"valueField"=>"label"
			])->contain('TbSysLookup')->where(["And"=>
					["TbSysLookup.code"=>"LANGUAGE"],
					["TbSysLookupDetails.id IN"=>$interfacelanguage]
				])->order(['TbSysLookupDetails.VALUE'=>"ASC"]);

		$languages = $rawconfigs;
		$userpref = new UserPreferenceForm();
		if($this->request->is("post","put"))
		{
			try{
				foreach($this->request->data as $key=>$value)
				{
					if($this->isDirty($userpreferences,$key,$value["value"]))
					{
						$data = $this->TbSysUserPreferences->get($value["id"]);
						$data->value = $value["value"];
						$this->TbSysUserPreferences->save($data);
					}
				}
				$this->rewriteConfiguration(true);
				$this->Flash->success(__d("system","Successfully update preferences"));
				$this->redirect(["action"=>"userpreference"]);
			}
			catch(Exception $ex)
			{
				$this->Flash->error(__d("system","Fail to update preferences"));
			}
		}
		else if($this->request->is("get"))
		{
			$this->request->data = $userpreferences;
		}
		$this->set("userpref",$userpref);
		$this->set('languages',$languages);
		$this->set('colorschemas',$colorschemas);
	}

	public function writing()
    {
        $configs = $this->collectConfigs("WRITING");
        $writingConfigForm = new WritingSettingForm();
        $this->loadModel('System.TbSysTerms');
        $allcategories = $this->TbSysTerms->find('type',['type'=>"category"])->where(['TbSysTerms.isactive'=>true])->order(['TbSysTerms.created'=>'ASC']);
        $results = $this->Terms->collectCategories($allcategories->toArray(),"&nbsp;&nbsp;");
        $collections = $this->Terms->collectAsListOptions($results);
        if($this->request->is("post","put"))
        {
            try{
                foreach($this->request->data as $key=>$value)
                {
                    if($this->isDirty($configs,$key,$value["value"]))
                    {
                        $data = $this->TbSysConfigs->get($value["id"]);
                        $data->value = $value["value"];
                        $this->TbSysConfigs->save($data);
                    }
                }
                //rewrite all config at session & cookies;
                $this->rewriteConfiguration(true);
                $this->Flash->success(__d("system","Successfully update configurations"));
                $this->redirect(["action"=>"writing"]);
            }
            catch(Exception $ex)
            {
                $this->Flash->error(__d("system","Fail to update configurations"));
            }
        }
        else if($this->request->is("get"))
        {
            $this->request->data = $configs;
        }
        $this->set('writingConfig',$writingConfigForm);
        $this->set('listcategories',$collections);
    }

    public function reading()
    {
        $configs = $this->collectConfigs("READING");
        $readingForm = new ReadingSettingForm();
        if($this->request->is("post","put"))
        {
            try{
                foreach($this->request->data as $key=>$value)
                {
                    if($this->isDirty($configs,$key,$value["value"]))
                    {
                        $data = $this->TbSysConfigs->get($value["id"]);
                        $data->value = $value["value"];
                        $this->TbSysConfigs->save($data);
                    }
                }
                //rewrite all config at session & cookies;
                $this->rewriteConfiguration(true);
                $this->Flash->success(__d("system","Successfully update configurations"));
                $this->redirect(["action"=>"reading"]);
            }
            catch(Exception $ex)
            {
                $this->Flash->error(__d("system","Fail to update configurations"));
            }
        }
        else if($this->request->is("get"))
        {
            $this->request->data = $configs;
        }
        $this->set('readingForm',$readingForm);
    }

    public function media()
    {
        $configs = $this->collectConfigs("MEDIA");
        $mediaForm = new MediaSettingForm();
        if($this->request->is("post","put"))
        {
            try{
                foreach($this->request->data as $key=>$value)
                {
                    if($this->isDirty($configs,$key,$value["value"]))
                    {
                        $data = $this->TbSysConfigs->get($value["id"]);
                        $data->value = $value["value"];
                        $this->TbSysConfigs->save($data);
                    }
                }
                //rewrite all config at session & cookies;
                $this->rewriteConfiguration(true);
                $this->Flash->success(__d("system","Successfully update configurations"));
                $this->redirect(["action"=>"media"]);
            }
            catch(Exception $ex)
            {
                $this->Flash->error(__d("system","Fail to update configurations"));
            }
        }
        else if($this->request->is("get"))
        {
            $this->request->data = $configs;
        }
        $this->set('mediaConfig',$mediaForm);
    }

    public function permalink()
    {
        $configs = $this->collectConfigs("PERMALINK");
        $mediaForm = new MediaSettingForm();
        if($this->request->is("post","put"))
        {
            try{
                foreach($this->request->data as $key=>$value)
                {
                    if($this->isDirty($configs,$key,$value["value"]))
                    {
                        $data = $this->TbSysConfigs->get($value["id"]);
                        $data->value = $value["value"];
                        $this->TbSysConfigs->save($data);
                    }
                }
                //rewrite all config at session & cookies;
                $this->rewriteConfiguration(true);
                $this->Flash->success(__d("system","Successfully update configurations"));
                $this->redirect(["action"=>"permalink"]);
            }
            catch(Exception $ex)
            {
                $this->Flash->error(__d("system","Fail to update configurations"));
            }
        }
        else if($this->request->is("get"))
        {
            $this->request->data = $configs;
        }
        $this->set('permalinkConfig',$mediaForm);
    }

}