<?php
namespace System\Controller\Backend;

use System\Controller\BackendAppController as AppController;

/**
 * Widgets Controller
 *
 * @property \System\Model\Table\WidgetsTable $Widgets
 */
class WidgetsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('System.TbSysWidgetGroups');
        $this->loadModel('System.TbSysWidgets');
    }

    public function configuration()
    {
        $groups = $this->TbSysWidgetGroups->find('all')
            ->contain(['Widgets'])
            ->where(['isadmin'=>false,'moduleid'=>$this->request->session()->read("Config")["THEME"]]);
        $widgets = $this->TbSysWidgets->find('all')->where(['isadmin'=>false,"isactive"=>true]);
        $this->set('groups',$groups);
        $this->set('widgets',$widgets);
    }

}
