<?php

namespace System\Controller;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\Utility\Inflector;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Number;

class FrontAppController extends AppController
{

    public function rewriteConfiguration($rewrite = false)
    {
        $session = $this->request->session();
        if($rewrite)
        {
            $session->delete("Setting");
        }
        $this->loadModel('System.TbSysLookupDetails');
        $this->loadModel('System.TbSysUserPreferences');
        // if($session->read('Setting')==null || Configure::read('debug')==true){
            $this->loadModel('System.TbSysConfigs');
            $configs = $this->TbSysConfigs->find('all')->select(["code","value","valuetype"])->where(['userid'=>'SYSTEM']);
            $configString = [];
            foreach($configs as $config)
            {
                $value = $config["value"];
                if($config["valuetype"]==2)
                {
                    $value = $this->TbSysLookupDetails->get($config["value"])["value"];
                }
                $configString[$config["code"]] = $value;
                $configString[$config["code"]."_VALUE"] = $config["value"];
            }

            $session->write('Setting',$configString);
        // }

        $this->Cookie->configKey('dateform','encryption', false);
        $this->Cookie->write('dateform',$configString["SITEDTFORM"]);

        I18n::locale('id_ID');
        $this->Cookie->configKey('lang', 'encryption', false);
        $this->Cookie->write('lang',$configString["SITEDFLTLG"]);
        // $this->CookieP

        $configString = $session->read('Setting');
        $this->set('config_front',$configString);
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->rewriteConfiguration(true);

        $this->loadModel('System.TbSysThemes');

        if(!empty($this->request->session()->read('Setting')["THEME"]))
        {
            $theme = $this->TbSysThemes->get($this->request->session()->read("Setting")["THEME"]);
            $this->ViewBuilder()->theme($theme->name);
            if($this->request->is("mobile"))
            {
                $this->ViewBuilder()->layout($theme->name.".m/default");
            }
        }
        else{
            $this->ViewBuilder()->layout('System.default_frontend');
        }
    }

    public function renderView($view_name = null)
    {
        try{
            if($view_name==null)
            {
                $view_name = Inflector::underscore($this->request->params["action"]);
            }
            if($this->request->is("mobile"))
            {
                $this->render('m/'.$view_name);
            }
            else
            {
                $this->render($view_name);
            }
        }
        catch(MissingTemplateException $ex)
        {
            if($view_name==null)
            {
                $view_name = Inflector::underscore($this->request->params["action"]);
            }
            $this->render($view_name);
        }
    }

}