$(window).scroll(function(e){
    var scrollTop = $(window).scrollTop();
    if (scrollTop >= 50) {
        if(!$(".toolbar").hasClass("sticky"))
            $(".toolbar").addClass("sticky")
                .fadeIn(400);
    }   
    else
    {
        if($(".toolbar").hasClass("sticky"))
            $(".toolbar")
                .removeClass("sticky");
    }
});