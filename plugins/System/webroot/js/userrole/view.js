$(document).ready(function(e)
{

	$("input[type='checkbox']").iCheck({
		checkboxClass: 'icheckbox_polaris',
	    radioClass: 'iradio_square',
	    increaseArea: '20%' // optional
  	});
	$(".cb-readonly").iCheck('disable');

});