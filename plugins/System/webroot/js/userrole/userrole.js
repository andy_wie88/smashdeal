$(document).ready(function(e)
{
	$("input[type='checkbox']").iCheck({
		checkboxClass: 'icheckbox_polaris',
	    radioClass: 'iradio_square',
	    increaseArea: '20%' // optional
  	});
	$(".cbreadonly").iCheck('disable');

	$(".quick-prev").popdisplay({
		'header':'Quick Preview Detail User Role',
		'fullscreen' : true
	});
});