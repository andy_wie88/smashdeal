$(document).ready(function () {
    var app = getCookie("APP");
    if(app!="")
    {
        app="/"+app;
    }
    $("textarea.editor").tinymce({
        selector: 'textarea',
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link charmap preview anchor',
            'searchreplace visualblocks code fullscreen',
            'table contextmenu paste code image media responsivefilemanager'
        ],
        toolbar1: 'styleselect | bold italic blockquote horizontalline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link anchor table  | ',
        toolbar2: 'responsivefilemanager image media | forecolor backcolor | code fullscreen | undo redo',
        image_advtab: true ,
        external_filemanager_path: app+"/js/filemanager/",
        external_plugins: { "filemanager" : app+"/js/filemanager/plugin.min.js"},
        filemanager_title:"Media Selector",
        relative_urls:false
    });

    $("#langs").change(function(e)
    {
        e.preventDefault();

        var curIndex = $(this).attr("data-current-index");
        var newLang = $(this).val();
        var newIndex = $(this).find("option:selected").attr("data-index");

        $("#templang").append("<div id='"+$(this).attr("data-current")+"'><input type='hidden' class='hidden-language' value='"+$("#language").val()+"' name='langs["+curIndex+"][languageid]' /><input type='hidden' class='hidden-title' value='"+$("#title").val()+"' name='langs["+curIndex+"][title]'><textarea class='hidden-body' name='langs["+curIndex+"][body]'>"+$("#editor").val()+"</textarea></div>");


        $("#title").attr("name","langs["+newIndex+"][title]");
        $("#language").attr("name","langs["+newIndex+"][languageid]");
        $("#editor").attr("name","langs["+newIndex+"][body]");

        if($("#"+newLang).length>0)
        {
            $("#title").val($("#"+newLang).find(".hidden-title").val());
            $("#language").val($("#"+newLang).find(".hidden-language").val());
            $("#editor").val($("#"+newLang).find(".hidden-body").val());
            $("#"+newLang).remove();
        }
        else
        {
            $("#title").val("");
            $("#language").val(newLang);
            $("#editor").val("");

        }

        $(this).attr("data-current",newLang);
        $(this).attr("data-current-index",newIndex);
    })
});