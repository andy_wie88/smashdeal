$(document).ready(function(e)
{
    $("#editPublishTime").click(function(e)
    {
        e.preventDefault();


        $("#editPublishTime").hide();

        var wrapper = $(this).parent();

        var content = $("<div class='clearfix'><div class='col-md-12 no-padding min-padding-top'><input id='datepick' type='text' class='form-control input-sm'/></div></div>").hide();

        var okbutton = $("<a id='statusokbutton' href='#' class='btn btn-default btn-sm'>OK</a>");
        $(wrapper).delegate("#statusokbutton", "click", function (e) {
            e.preventDefault();
            if($("#datepick").val()!="") {
                $("#date").val($("#datepick").val());
                $("#printedtime").html($("#datepick").val());
            }
            else
            {
                $("#date").val("");
                $("#printedtime").html("immediately");
            }
            $(content).slideUp('slow').delay(1000).remove();
            $("#editPublishTime").show();
        });

        var cancelbutton = $("<a id='statuscancelbutton' href='#'>Cancel</a>");
        $(wrapper).delegate("#statuscancelbutton", "click", function (e) {
            e.preventDefault();
            $(content).slideUp('slow').delay(1000).remove();
            $("#editPublishTime").show();
        });

        $(content).append("<div class='col-md-12 no-padding min-padding-top'>" + $(okbutton).prop("outerHTML") + $(cancelbutton).prop("outerHTML") + "</div>");

        $(wrapper).append($(content));

        var lang = getCookie("lang").split("_")[0];
        var dateformat = dpconverter(getCookie("dateform"));
        $("#datepick").datepicker({language:lang,format:dateformat});

        $("#datepick").val($("#date").val());

        $(content).show('slow');
    });
});