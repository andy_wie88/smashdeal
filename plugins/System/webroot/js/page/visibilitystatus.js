$(document).ready(function () {
    $("#editVisible").click(function (e) {
        e.preventDefault();

        $("#editVisible").hide();

        var wrapper = $(this).parent();

        var visibilityInput = $("#visibilityTemplate").clone();
        $(visibilityInput).removeClass("hidden").attr("id", "visibilitySelect");
        var checkedVal = $("#visibilitystatus").val();

        var content = $("<div class='clearfix'></div>").hide();

        var okbutton = $("<a id='statusokbutton' href='#' class='btn btn-default btn-sm'>OK</a>");
        $(wrapper).delegate("#statusokbutton", "click", function (e) {
            e.preventDefault();
            $("#printedvisibility").html($("label[for='" + $("#visibilitySelect").find("input:checked").attr("id") + "']").html());
            $("#visibilitystatus").val($("#visibilitySelect").find("input:checked").val());

            if ($("#visibilitySelect").find("input:checked").val() == 2) {
                $("#postpassword").val($("#ppass").val());
            }

            $(content).slideUp('slow').delay(1000).remove();
            $("#editVisible").show();
        });

        var cancelbutton = $("<a id='statuscancelbutton' href='#'>Cancel</a>");
        $(wrapper).delegate("#statuscancelbutton", "click", function (e) {
            e.preventDefault();
            $(content).slideUp('slow').delay(1000).remove();
            $("#editVisible").show();
        });

        $(content).append("<div class='col-md-12 no-padding min-padding-right'>" + $(visibilityInput).prop('outerHTML') + "</div><div class='col-md-12 no-padding'>" + $(okbutton).prop("outerHTML") + $(cancelbutton).prop("outerHTML") + "</div>");

        $(content).appendTo($(wrapper));

        $("#visibilitySelect input").iCheck({
            checkboxClass: 'icheckbox_polaris',
            radioClass: 'iradio_polaris',
            increaseArea: '20%' // optional
        });

        $("#visibilitySelect input").on('ifChecked', function (e) {
            if ($(this).val() == 2) {
                $(this).parent().parent().append("<div id='postpass'><label>Password : </label><div class='col-md-12 no-padding'><input id='ppass' type='text' class='form-control input-sm'/></div></div>");
            }
        });

        $("#visibilitySelect input").on('ifUnchecked', function (e) {
            if ($(this).val() == 2) {
                $("#postpass").remove();
            }
        });

        $("#visibilitySelect input[value='" + checkedVal + "']").iCheck('check');
        if (checkedVal == 2) {
            console.log($("#postpassword").val());
            $("#ppass").val($("#postpassword").val());
        }
        $(content).slideDown('slow');
    });
});