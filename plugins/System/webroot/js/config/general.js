$(document).ready(function(e)
{
	
	$("input[type='radio']").iCheck({
		checkboxClass: 'icheckbox_polaris',
	    radioClass: 'iradio_polaris',
	    increaseArea: '20%' // optional
  	});

	$("input[type='checkbox']").iCheck({
		checkboxClass: 'icheckbox_polaris',
		radioClass: 'iradio_square',
		increaseArea: '20%' // optional
	});
	
});