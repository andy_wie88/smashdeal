$(document).ready(function(e)
{
    $( "#leftdashboard, #rightdashboard" ).sortable({
      	placeholder: "ui-state-bordered",
      	connectWith: ".connected"
    }).disableSelection();

    $( ".connected" ).on("sortstop",function(e)
    {
        var lefts = $("#leftdashboard").sortable("toArray");
        var rights = $("#rightdashboard").sortable("toArray");
        var data = {"id":$("#uid").val(),"dashboard_components":[]};
        var index = 1;
        for(left in lefts)
        {
            data.dashboard_components.push({"id":lefts[left],"_joinData":{"pos":1,"index":index}});
            index++;
        }
        index=1;
        for(right in rights)
        {
            data.dashboard_components.push({"id":rights[right],"_joinData":{"pos":2,"index":index}});
            index++;
        }
        $.ajax({
            type:"POST",
            url:$("#updatedash").attr("href"),
            data:data
        });
    });
 
});