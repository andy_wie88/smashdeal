$(document).ready(function(e)
{
	var addedItems = [];
	var item = 0;

	var isAddedMenu = function(addedItem)
	{
		if(addedItems.length > 0)
		{
			for(var idx in addedItems){
				if(addedItems[idx].uid == addedItem.uid)
				{
					return true;
				}
			}
		}
		return false;
	}

	var removeAdded = function(uid)
	{
		if(addedItems.length>0)
		{
			for(item in addedItems)
			{
				if(addedItems[item].uid == uid)
				{
					addedItems.splice(item,1);
				}
			}
		}
	}

	var deleteSingleItem = function(component)
	{
		var idx= $(component).parent().parent().attr("id");
		var uid = $("#"+idx+" .uid").val();
		removeAdded(uid);
		$("#"+idx).remove();
	}

	var initDashboardAdd = function()
	{
		$("#toolbar").removeClass("hidden");
		$("#dashboardwrap").removeClass("hidden");
		$("#lovDashboard").attr("data-source",$("#toolbar").attr("data-source")+"/"+$("#userid").val());
		$("#lovDashboard").lov({
			'title' : 'Search Dashboard',
			'width' : 1000,
			'height' : 400,
			'source' : {
				datafields : 
				[
					{name : 'code', type : 'string'},
					{name : 'name', type : 'string'},
					{name : 'note', type: 'string'}
				]
			},
			'columns' : [
				{'text':'Code',datafield:'code'},
				{'text':'Name',datafield:'name'},
				{'text':'Description',datafield:'note'}
			],
			'searchType' : 'row',
			'filterable' : true,
			'selectionmode' : 'multiplerows',
			'callback' :  function(datas)
			{
				if(datas.length>0)
				{
					for(idx in datas)
					{
						if(!isAddedMenu(datas[idx]))
						{
							if($("#leftdashboard>li:last-child>input.index").length>0){
								var index = parseInt($("#leftdashboard>li:last-child>input.index").val())+1;
							}
							else
								var index = 1;
							$("#leftdashboard").append("<li id='"+datas[idx].uid+"'>"+
									"<input type='hidden' name='dashboard_components["+item+"][id]' class='uid' value='"+datas[idx].uid+"'/>"+
									"<input type='hidden' name='dashboard_components["+item+"][_joinData][pos]' value='1' class='pos'/>"+
									"<input type='hidden' class='index' name='dashboard_components["+item+"][_joinData][index]' value='"+index+"'/>"+
									datas[idx].name + 
									"<div class='pull-right'><button class='btn btn-sm btn-delete btn-danger'><i class='fa fa-trash'/></button></div>"
								+"</li>");
							addedItems.push(datas[idx]);
							$("#"+datas[idx].uid+" .btn-delete").on("click",function(e)
							{
								e.preventDefault();
								deleteSingleItem($(this));
							});
							item++;
						}
					}
				}
			}
		});
	}

	var hideDashboardAdd = function()
	{
		$("#toolbar").addClass("hidden");
		$("#dashboardwrap").addClass("hidden");
		destroyDasboardAdd();
	}

	var destroyDasboardAdd = function()
	{
		$("#lovDashboard").remove();
		$("#toolbar").append($("<div id='lovDashboard'></div>"));
		addedItems = [];
		$("#leftdashboard").empty();
		$("#rightdashboard").empty();
	}

	$("#userLov").lov({
		'title' : 'Search User',
		'width' : 300,
		'height' : 400,
		'source' : {
			datafields : 
			[
				{name : 'username', type : 'string'}
			]
		},
		'columns' : [
			{'text':'Name',datafield:'username'}
		],
		'required' : true,
		'searchType' : 'row',
		'filterable' : true,
		'selectionmode' : 'singlerow',
		'displayfield' : 'username',
		'callback' :  function(datas)
		{
			if(datas.length>0)
			{
				var data = datas[0];
				$("#userid").val(data.uid);
				destroyDasboardAdd();
				initDashboardAdd();
			}
		},
		'removeCallback' : function()
		{
			$("#userid").val("");
			hideDashboardAdd();
		}
	});

	(function(){

		if($("#userid").val()!="")
		{
			initDashboardAdd();
		}

		if($("#leftdashboard li").length>0)
		{
			item = $("#leftdashboard li").length;
			$("#leftdashboard li .btn-delete").on("click",function(e)
			{
				e.preventDefault();
				deleteSingleItem($(this));
			});
			$("#leftdashboard li").each(function()
			{
				var id = $(this).attr("id");
				var uid = $("#"+id+" .uid").val();
				addedItems.push({uid:uid});
			});
		}

		if($("#rightdashboard li").length>0)
		{
			item = $("#rightdashboard li").length;
			$("#rightdashboard li .btn-delete").on("click",function(e)
			{
				e.preventDefault();
				deleteSingleItem($(this));
			});
			$("#rightdashboard li").each(function()
			{
				var id = $(this).attr("id");
				var uid = $("#"+id+" .uid").val();
				addedItems.push({uid:uid});
			});
		}

	})();

	$( "#leftdashboard, #rightdashboard" ).sortable({
      	placeholder: "ui-state-bordered-small",
      	connectWith: ".connected"
    }).disableSelection();

    $( "#leftdashboard, #rightdashboard" ).on("sortstop",function(e)
    {
    	var lefts = $("#leftdashboard").sortable("toArray");
    	var index = 1;
    	for(left in lefts){
    		$("#"+lefts[left]+" .pos").val(1);
    		$("#"+lefts[left]+" .index").val(index);
    		index++;
    	}
    	var rights = $("#rightdashboard").sortable("toArray"); 
    	index = 1;
    	for(right in rights){
    		$("#"+rights[right]+" .pos").val(2);
    		$("#"+rights[right]+" .index").val(index);
    		index++;
    	}
    	//biz tu di push ke get ke database
    });

});	