$(document).ready(function(e) {

    $("input[type='checkbox']").iCheck({
        checkboxClass: 'icheckbox_polaris',
        radioClass: 'iradio_square',
        increaseArea: '20%' // optional
    });

    $("input[type='checkbox']").on('ifChecked',function(e)
    {
        if($(this).attr("data-target")!==undefined) {
            $($(this).attr("data-target")).iCheck("check");
        }
        else{
            $("#clone-"+$(this).attr("id")).iCheck("check");
        }
    });

    $("input[type='checkbox']").on('ifUnchecked',function(e)
    {
        if($(this).attr("data-target")!==undefined) {
            $($(this).attr("data-target")).iCheck("uncheck");
        }
        else{
            $("#clone-"+$(this).attr("id")).iCheck("uncheck");
        }
    });
});