$(document).ready(function () {
    var ids = "";
    var idsArray = [];
    var first = true;
    var link = $("#widget-group").attr("data-url");
    $.each($(".sortable"),function (index,value) {
        if(!first){
            ids += ",";
        }
        ids += "#" + $(value).attr("id");
        idsArray.push("#"+$(value).attr("id"));
        first = false;
    });

    $("#drop-remove").droppable({
        accept: ".widget-box",
        drop : function(event, ui)
        {
            $(ui.draggable).remove();
            $("#drop-remove").removeClass("drop-remove-larger");
        },
        over : function(event, ui)
        {
            $("#drop-remove").addClass("drop-remove-larger");
        },
        out : function(event,ui)
        {
            $("#drop-remove").removeClass("drop-remove-larger");
        }
    });

    $(".widget-group .draggable" ).draggable({
        connectToSortable: ".sortable",
        handle : '.widget-title',
        helper: "clone",
        revert: "invalid"
    });


    $(ids).sortable({
        connectWith: ".sortable, #drop-remove",
        placeholder: "ui-state-bordered-small",
        revert: "invalid",
        stop: function(event,ui)
        {
            var item = $(ui.item).parent().find(".widget-box").length;
            var id = $(ui.item).parent().attr("id")+"_"+$(ui.item).attr("data-widget")+"_"+item;
            $(ui.item).attr("id",id);
            $(ui.item).removeAttr("style");
            $(ui.item).find(".widget-action>a").unbind("click");
            $(ui.item).find(".widget-action>a").click(function(e)
            {
                e.preventDefault();
                if($(this).parent().parent().parent().find(".widget-inside:hidden").length>0)
                {
                    $(this).find("i").removeClass("fa-caret-down").addClass("fa-caret-up");
                }
                else {
                    $(this).find("i").removeClass("fa-caret-up").addClass("fa-caret-down");
                }
                $(this).parent().parent().parent().find(".widget-inside").slideToggle();
            });
            $(ui.item).find(".widget-inside a.close-box").unbind("click");
            $(ui.item).find(".widget-inside a.close-box").click(function(e)
            {
                e.preventDefault();
                $(this).parent().parent().parent().parent().parent().parent().find(".widget-action i").removeClass("fa-caret-up").addClass("fa-caret-down");
                $(this).parent().parent().parent().parent().parent().slideUp();
            });
            $(ui.item).find(".widget-inside a.remove").unbind("click");
            $(ui.item).find(".widget-inside a.remove").click(function(e)
            {
                e.preventDefault();
                $(this).parent().parent().parent().parent().parent().parent().remove();
                collectWidget('ordering');
            });
            $(ui.item).find(".widget-inside button.submit").unbind("click");
            $(ui.item).find(".widget-inside button.submit").click(function(e)
            {
                e.preventDefault();
                collectWidget('ordering');
            });
            collectWidget('ordering');
        }
    });

    collectWidget = function (saveType) {
        var widget = [];
        widget["savetype"] = saveType;
        $.each(idsArray,function(index1, value)
        {
            var groupname = "widget[" + index1 + "]";
            widget.push({name: groupname + "[groupid]", value: $(value).attr("data-id")});
            $.each($(value + " .widget-box"), function (index2, value) {
                var configs = $(value).find("form").serializeArray();
                for (idx in configs) {
                    var config = configs[idx];
                    var widgetName = groupname + "[widgets]["+index2+"]" + config["name"];
                    widget.push({name: widgetName, value: config["value"]});
                }
            });
        });
        $.ajax({
            url:link,
            data:widget,
            method:"POST",
            dataType:"JSON"
        });
    }

    $(".widget-action>a").unbind("click");
    $(".widget-action>a").click(function(e)
    {
        e.preventDefault();
        if($(this).parent().parent().parent().find(".widget-inside:hidden").length>0)
        {
            $(this).find("i").removeClass("fa-caret-down").addClass("fa-caret-up");
        }
        else {
            $(this).find("i").removeClass("fa-caret-up").addClass("fa-caret-down");
        }
        $(this).parent().parent().parent().find(".widget-inside").slideToggle();
    });

    $(".widget-inside a.close-box").unbind("click");
    $(".widget-inside a.close-box").click(function(e)
    {
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().parent().find(".widget-action i").removeClass("fa-caret-up").addClass("fa-caret-down");
        $(this).parent().parent().parent().parent().parent().slideUp();
    });

    $(".widget-inside a.remove").unbind("click");
    $(".widget-inside a.remove").click(function(e)
    {
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().parent().remove();
        collectWidget('ordering');
    });
    $(".widget-inside button.submit").unbind("click");
    $(".widget-inside button.submit").click(function(e)
    {
        e.preventDefault();
        collectWidget('ordering');
    });
});