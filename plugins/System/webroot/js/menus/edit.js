enableEffect = function()
{
    $(".widget-action>a").unbind("click");
    $(".widget-action>a").click(function(e)
    {
        e.preventDefault();
        if($(this).parent().parent().parent().find(".widget-inside:hidden").length>0)
        {
            $(this).find("i").removeClass("fa-caret-down").addClass("fa-caret-up");
        }
        else {
            $(this).find("i").removeClass("fa-caret-up").addClass("fa-caret-down");
        }
        $(this).parent().parent().parent().find(".widget-inside").slideToggle();
    });

    $(".widget-inside a.close-box").unbind("click");
    $(".widget-inside a.close-box").click(function(e)
    {
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().find(".widget-action i").removeClass("fa-caret-up").addClass("fa-caret-down");
        $(this).parent().parent().parent().slideUp();
    });

    $(".widget-inside a.remove").unbind("click");
    $(".widget-inside a.remove").click(function(e)
    {
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().remove();
    });
}

$(document).ready(function(e)
{
    $("input[type='checkbox']").iCheck({
        checkboxClass: 'icheckbox_polaris',
        radioClass: 'iradio_square',
        increaseArea: '20%' // optional
    });
    function dump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') { //Array/Hashes/Objects
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Strings/Chars/Numbers etc.
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }

    // forcePlaceholderSize : true,
    // placeholder:'placeholder',
    // handle : ".widget-title",
    // item : 'li',
    // toleranceElement : '> div',

    updateMenu = function(menus,parentid)
    {
        for(menu in menus)
        {
            $("#"+menus[menu]["id"]+" .index").val(menu);
            $("#"+menus[menu]["id"]+" .parentid").val(parentid);
            if('children' in menus[menu]){
                updateMenu(menus[menu]['children'],menus[menu]['id']);
            }
        }
    }

    $("#menugroupitem").nestedSortable({
        forcePlaceholderSize: true,
        handle: '.widget-title',
        items: 'li',
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        isTree: true,
        expandOnHover: 700,
        startCollapsed: false,
        isAllowed: function(placeholder, placeholderParent, currentItem){
            if($(placeholderParent).hasClass("no-child"))
                return false;
            return true;
        },
        relocate : function(){
            hiered = $('ol#menugroupitem').nestedSortable('toHierarchy', {startDepthCount: 0});
            updateMenu(hiered,"");
        }
    });

    enableEffect();
});