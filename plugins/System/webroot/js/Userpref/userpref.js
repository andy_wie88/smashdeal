$(document).ready(function(e)
{
	
	$("input[type='radio']").iCheck({
		checkboxClass: 'icheckbox_polaris',
	    radioClass: 'iradio_polaris',
	    increaseArea: '20%' // optional
  	});
	
	$("input[type='radio']").on("ifChecked",function(event)
	{
		$("body").removeAttr("class");
		$("body").addClass("hold-transition " + $(event.target).val() + " fixed sidebar-mini")
	});

});