$(document).ready(function(e)
{
	var lang = getCookie("lang").split("_")[0];
	var dateformat = dpconverter(getCookie("dateform"));
	$("input.datepick").datepicker({language:lang,format:dateformat});

	$("#role-quick-prev").popdisplay({
		'header':'Quick Preview Detail User Role',
		'fullscreen' : true
	});
});