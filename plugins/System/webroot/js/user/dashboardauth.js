$(document).ready(function(e)
{


	var addedItems = [];
	var item = 0;

	var isAddedMenu = function(addedItem)
	{
		if(addedItems.length > 0)
		{
			for(var idx in addedItems){
				if(addedItems[idx].uid == addedItem.uid)
				{
					return true;
				}
			}
		}
		return false;
	}

	var removeAdded = function(uid)
	{
		if(addedItems.length>0)
		{
			for(item in addedItems)
			{
				if(addedItems[item].uid == uid)
				{
					addedItems.splice(item,1);
				}
			}
		}
	}

	var activeiCheck = function(selector)
	{
		$(selector).iCheck({
			checkboxClass: 'icheckbox_polaris',
		    radioClass: 'iradio_square',
		    increaseArea: '20%' // optional
	  	});
	}

	var deleteSingleItem = function(component)
	{
		var idx= $(component).parent().parent().attr("id");
		var uid = $("#"+idx+" .uid").val();
		removeAdded(uid);
		$("#"+idx).remove();
	}

	$("#lovDashboard").lov({
		'title' : 'Search Dashboard',
		'width' : 1000,
		'height' : 400,
		'source' : {
			datafields : 
			[
				{name : 'code', type : 'string'},
				{name : 'name', type : 'string'},
				{name : 'note', type: 'string'}
			]
		},
		'columns' : [
			{'text':'Code',datafield:'code'},
			{'text':'Name',datafield:'name'},
			{'text':'Description',datafield:'note'}
		],
		'searchType' : 'row',
		'filterable' : true,
		'selectionmode' : 'multiplerows',
		'callback' :  function(datas)
		{
			if(datas.length>0)
			{
				for(idx in datas)
				{
					if(!isAddedMenu(datas[idx]))
					{
						var id = "item-"+item;
						$("#table-container").append("<tr id='"+id+"'><td><input type='checkbox' class='checkbox-child'/></td><td><input type='hidden' class='uid' name='dashboard_components[][id]' value='"+datas[idx].uid+"'/><input type='text' class='form-control' value='"+datas[idx].name+"' readonly/></td><td><span>"+datas[idx].note+"</span></td><td><button class='btn btn-danger btn-delete'><i class='fa fa-trash'></i></button></td></tr>");
						addedItems.push(datas[idx]);
						$("#"+id+" .btn-delete").on("click",function(e)
						{
							e.preventDefault();
							deleteSingleItem($(this));
						});
						item++;
					}
				}
				activeiCheck("#table-container tbody input[type='checkbox']");
			}
		}
	});

	$("#btnBulkDelete").on('click',function(e)
	{
		e.preventDefault();
		$("#table-container input.checkbox-child:checked").each(function()
		{
			var trParent = $(this).parent().parent().parent().attr("id");
			var uid = $("#"+trParent+" .uid").val();
			removeAdded(uid);
		});
		$("#table-container input.checkbox-child:checked").parent().parent().parent().remove();
	});

	activeiCheck("input[type='checkbox']");

	if($(".item").length>0)
	{
		item = $(".item").length;
		$(".item .btn-delete").on("click",function(e)
		{
			e.preventDefault();
			deleteSingleItem($(this));
		});
		$(".item").each(function()
		{
			var id = $(this).attr("id");
			var uid = $("#"+id+" .uid").val();
			addedItems.push({uid:uid});
		});
	}

});