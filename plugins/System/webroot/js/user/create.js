$(document).ready(function(e)
{
	function randomString(length, chars) {
	    var mask = '';
	    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
	    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    if (chars.indexOf('#') > -1) mask += '0123456789';
	    if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
	    var result = '';
	    for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
	    return result;
	}

	var roleUrl = $("#roleLov").attr("data-dynamic-source");
	$("#roleLov").removeAttr("data-dynamic-source");

	$("input[type='checkbox']").iCheck({
		checkboxClass: 'icheckbox_polaris',
	    radioClass: 'iradio_square',
	    increaseArea: '20%' // optional
  	});
	var lang = getCookie("lang").split("_")[0];
	var dateformat = dpconverter(getCookie("dateform"));
	$("input.datepick").datepicker({language:lang,format:dateformat});

	var detailRenderer = function(row,column,value)
	{
		var id = randomString(5,"Aa#");
		var url = roleUrl+"/"+value;
        var html = "<span style='padding:5px;'><a id='"+id+"' class='quick-prev' href='#' data-href='"+url+"'>preview</a>";
    	$(html).find(".quick-prev").popdisplay({
			'header':'Quick Preview Data User',
			'fullscreen' : true
		});
        return html;
	}

	$("#roleLov").lov({
		'title' : 'Lov Menu',
		'width' : 1000,
		'height' : 400,
		'source' : {
			datafields : 
			[
				{name : 'name', type : 'string'},
				{name : 'description', type: 'string'}
			]
		},
		'columns' : [
			{'text':'Name',datafield:'name'},
			{'text':'Action',datafield:'uid',cellsrenderer:detailRenderer}
		],
		'searchType' : 'row',
		'filterable' : true,
		'selectionmode' : 'singlerow',
		'displayfield' : 'name',
		'callback' :  function(datas)
		{
			if(datas.length>0)
			{
				var data = datas[0];
				$("#userroleid").val(data.uid);
			}
		}
	});

});