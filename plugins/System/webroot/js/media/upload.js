(function() {

    $(document).bind('dragover', function (e) {
        var dropZone = $('#dropzone'),
            timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('dropzone-in');
        } else {
            clearTimeout(timeout);
        }
        var found = false,
            node = e.target;
        do {
            if (node === dropZone[0]) {
                found = true;
                break;
            }
            node = node.parentNode;
        } while (node != null);
        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('dropzone-in hover');
        }, 100);
    });

    var isImage = function(string)
    {
        var patt = new RegExp("\/(gif|jpe?g|png)$");
        return patt.test(string);
    }

    var url = $("#fileupload").attr("data-url");
    $('#fileupload').fileupload({
        dropZone: $('#dropzone'),
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $("#progress").addClass("hidden");
            $.each(data.result.files, function (index, file) {
                if(file.error==undefined) {
                    $("<li><img src='"+file.thumbnail+"' height='30px' style='margin-right:5px'/>" + file.name + "<span class='pull-right'><a href='"+file.editlink+"' target='_blank'>edit</a></span></li>").appendTo("#listfile");
                }
                else {
                    $("#listfile").append("<div class='callout callout-danger'>"+file.error+"<span class='pull-right'><a href='#' onclick='$(this).parent().parent().remove()'>dismiss</a></span></div>")
                }
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $("#progress").removeClass("hidden");
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
})();