drop table if exists tb_sys_modules;

---  Separator ---

drop table if exists tb_sys_configs;

---  Separator ---

drop table if exists tb_sys_lookups;

---  Separator ---

drop table if exists tb_sys_lookup_details;

---  Separator ---

drop table if exists tb_sys_addons;

---  Separator ---

drop table if exists tb_sys_route_tables;

---  Separator ---

drop table if exists tb_sys_menus;

---  Separator ---

drop table if exists tb_sys_menu_rights;

---  Separator ---

drop table if exists tb_sys_user_roles;

---  Separator ---

drop table if exists tb_sys_menu_accesses;

---  Separator ---

drop table if exists tb_sys_menu_right_accesses;

---  Separator ---

drop table if exists tb_sys_profiles;

---  Separator ---

drop table if exists tb_sys_contact_types;

---  Separator ---

drop table if exists tb_sys_user_contacts;

---  Separator ---

drop table if exists tb_sys_users;

---  Separator ---

drop table if exists tb_sys_user_password_logs;

---  Separator ---

create table tb_sys_modules(
    id varchar(100) not null primary key,
    code varchar(10) not null,
    "version" varchar(100) not null,
    "name" varchar(200) not null,
    displayname varchar(200) not null,
    description text,
    versi varchar(100),
    isactive boolean default true not null,
    issystem boolean default false not null,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);

---  Separator ---

ALTER TABLE "public"."tb_sys_modules" ADD CONSTRAINT "uq_module_code" UNIQUE( "code" );


---  Separator ---

create table tb_sys_configs(
    id varchar(100) not null primary key,
    code varchar(10) not null,
    "label" varchar(100),
    "value" varchar(500),
    moduleid varchar(100),
    userid varchar(100),
    "groupname" varchar(100),
    "valuetype" smallint,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);


---  Separator ---

COMMENT ON COLUMN tb_sys_configs.valuetype IS '1 => Static, 2 => Lookup';


---  Separator ---

ALTER TABLE "public"."tb_sys_configs" ADD CONSTRAINT "uq_config_code" UNIQUE( "code" );


---  Separator ---

create table tb_sys_lookups(
    id varchar(100) not null primary key,
    code varchar(10) not null,
    moduleid varchar(100) not null,
    "name" varchar(200),
    "description" text,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);


---  Separator ---

ALTER TABLE "public"."tb_sys_lookups" ADD CONSTRAINT "uq_lookup_code" UNIQUE( "code" );


---  Separator ---

create table tb_sys_lookup_details(
    id varchar(100) not null primary key,
    lookupid varchar(100) not null,
    "label" varchar(200) not null,
    "value" varchar(200) not null,
    "description" text,
    isactive boolean default true,
    created timestamp,
    createdby varchar(100),
    modified TIMESTAMP,
    modifiedby varchar(100)
);


---  Separator ---

create table tb_sys_addons(
    id varchar(100) not null primary key,
    code varchar(10) not null,
    "name" varchar(200) not null,
    description text,
    moduleid varchar(100) not null,
    "type" smallint,
    isactive boolean default true not null,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);

---  Separator ---

create table tb_sys_route_tables(
    "id" varchar(100) not null primary key,
    "controller" varchar(100) not null,
    "action" varchar(100) not null,
    "plugin" varchar(100),
    "prefix" varchar(100),
    url varchar(200),
    moduleid varchar(100),
    isactive boolean default true,
    isadmin boolean default false,
    issuperadminonly boolean default false,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);

---  Separator ---

create table tb_sys_menus(
    id varchar(100) not null primary key,
    code varchar(100) not null,
    "name" varchar(200) not null,
    "icon" varchar(100),
    "label" varchar(300),
    "description" text,
    "keyword" text,
    "type" smallint not null,
    rendertype smallint not null,
    extraconfig text,
    isneedextra boolean default false,
    url varchar(200),
    "level" smallint,
    "index" smallint,
    "isadmin" boolean default false,
    "isleaf" boolean default false,
    parentid varchar(100),
    routeid varchar(100),
    isactive boolean default true,
    isdisplay boolean default true,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);

CREATE TABLE tb_sys_sub_menus ( 
    "menuid" Character Varying( 100 ) NOT NULL,
    "submenuid" Character Varying( 100 ) NOT NULL,
    "index" Integer NOT NULL,
    PRIMARY KEY ( "menuid", "submenuid" ) );


---  Separator ---

create table tb_sys_menu_rights(
    id varchar(100) not null primary key,
    code varchar(10) not null,
    displayname varchar(100) not null,
    menuid varchar(100) not null,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);


---  Separator ---

create table tb_sys_user_roles(
    id varchar(100) not null primary key,
    "name" varchar(200) not null,
    "description" text,
    isactive boolean default true,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);


---  Separator ---

create table tb_sys_menu_accesses(
    menuid varchar(100) not null,
    userroleid varchar(100) not null
);


---  Separator ---

alter table tb_sys_menu_accesses
    add primary key(menuid,userroleid);

---  Separator ---

create table tb_sys_menu_right_accesses(
    menurightid varchar(100) not null,
    userroleid varchar(100) not null
);


---  Separator ---

alter table tb_sys_menu_right_accesses
    add primary key(menurightid,userroleid);


---  Separator ---

create table tb_sys_profiles(
    id varchar(100) not null primary key,
    firstname varchar(100),
    lastname varchar(100),
    fullname varchar(200),
    displayname varchar(200),
    birthday Date,
    city varchar(100),
    country varchar(100),
    address text,
    aboutme text,
    profilepic varchar(100),
    website varchar(200),
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);


---  Separator ---

create table tb_sys_contact_types(
    id varchar(100) not null primary key,
    icon varchar(100) not null,
    "name" varchar(100) not null,
    validationregex varchar(100) not null,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);


---  Separator ---

create table tb_sys_user_contacts(
    id varchar(100) not null primary key,
    profileid varchar(100) not null,
    contacttypeid varchar(100) not null,
    "value" varchar(100) not null,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);


---  Separator ---

create table tb_sys_users(
    id varchar(100) not null primary key,
    username varchar(100) not null,
    "password" varchar(100) not null,
    email varchar(200) not null,
    isactive boolean default true,
    profileid varchar(100) not null,
    userroleid varchar(100) not null,
    issuperadmin boolean default false,
    isfirsttime boolean default true,
    isresetpass boolean default false,
    created timestamp,
    createdby varchar(100),
    modified timestamp,
    modifiedby varchar(100)
);

---  Separator ---

create table tb_sys_user_password_logs(
    id varchar(100) not null primary key,
    "password" varchar(100) not null,
    changedate timestamp not null,
    changeat  varchar(100) not null,
    userid varchar(100)
);