<?php
namespace Smashdeal\Controller\Frontend;

use Cake\Event\Event;
use Smashdeal\Controller\FrontendAppController as AppController;
use CouponCode\CouponCode;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\Mailer\Email;

/**
 * Purchases Controller
 *
 * @property \Smashdeal\Model\Table\PurchasesTable $Purchases
 */
class PurchasesController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        if($this->Auth->user('id')==null){
            $request = $this->request->session()->read('redirectto');
            if($request==null)
            {
                $request = '';
            }
            $this->request->session()->write('redirectto',$this->request->url);
        }
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Smashdeal.TbDealProducts');
        $this->loadModel('Smashdeal.TbDealDirectPurchases');
        $this->loadModel('Smashdeal.TbDealUsers');
    }

    public function direct($slug)
    {
        $product = $this->TbDealProducts->find('all',['contain'=>['Langs','Metas','Terms','Medias','Merchants']])->where(['TbDealProducts.slug'=>$slug])->first();
        $user = $this->TbDealUsers->get($this->Auth->user('id'));
        $this->set('user',$user);
        $this->set('product',$product);
        if($this->request->is(["post","patch","put"]))
        {
            $this->loadModel('Smashdeal.TbDealUsers');
            $user = $this->TbDealUsers->get($this->Auth->user('id'));
            $valid = $this->isValid($this->request->data['cashtoken'],$product,$user);
            if($valid['status']){
                $purchase = $this->TbDealDirectPurchases->newEntity();
                $purchase->productid = $product->id;
                $purchase->merchantid = $product->merchantid;
                $purchase->userid = $this->Auth->user('id');
                $purchase->amount = $this->request->data['amount'];
                $purchase->cashtoken = ($this->request->data["cashtoken"]==null)?0:$this->request->data['cashtoken'];
                $purchase->retailprice = $this->request->data['retailprice'];
                $purchase->subtotal = $purchase->retailprice * $purchase->amount;
                $purchase->grandtotal = $purchase->subtotal - $purchase->cashtoken;
                if($this->TbDealDirectPurchases->save($purchase))
                {
                    $this->loadModel('Smashdeal.TbDealMerchants');
                    $merchant = $this->TbDealMerchants->get($product['merchantid']);

                    $this->loadModel('Smashdeal.TbDealClaimVouchers');
                    $voucher = $this->TbDealClaimVouchers->newEntity();
                    $voucher->claimid = $purchase['id'];
                    $voucher->merchantid = $purchase['merchantid'];
                    $voucher->claimtype = 2;
                    $coupon = new CouponCode(['parts' => 4, 'partLength' => 5]);
                    $voucher->vouchercode = $coupon->generate();
                    $validity = new Date();
                    $validity->modify("+1 Month");
                    $voucher->validity = $validity;
                    $voucher->userid =  $user['id'];

                    $this->TbDealClaimVouchers->save($voucher);
                    $email = new Email('waruna');
                    $email
                        ->template('Smashdeal.purchases/purchase_voucher','Smashdeal.default')
                        ->emailFormat('html')
                        ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                        ->to($user->email)
                        ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                        ->subject(__d("smashdeal","Dealsmash - Direct Purchase Voucher"))
                        ->viewVars(['user'=>$user,'product'=>$product,'voucher'=>$voucher,'merchant'=>$merchant])
                        ->send();
                        
                    $user = $this->TbDealUsers->get($this->Auth->user('id'));
                    $user->topupamount -= ($this->request->data["cashtoken"]==null)?0:$this->request->data['cashtoken'];
                    $this->TbDealUsers->save($user);
                    $this->set('user',$user);
                    $this->renderView('direct_two');
                }
            }
            else
            {
                $this->Flash->error($valid['message']);
            }
        }
        else
        {
            $this->renderView();
        }
    }

    public function isValid($cashtoken, $product, $user)
    {
        if($cashtoken > $user->topupamount)
        {
            return ['status'=>false,'message'=>__d("smashdeal","You have insufficient of cash token")];
        }
        return ['status'=>true];
    }

    public function claimstatus()
    {
        $this->loadModel('Smashdeal.ViewUnclaimDirectpurchase');

        $user = $this->Auth->user('id');

        if($this->request->is("mobile")){
            $this->paginate = [
                'limit'=>1,
                'contain'=>['Merchants','Purchases'],
                'finder'=>['User'=>['userid'=>$user]]
            ];
        }
        else
        {
            $this->paginate = [
                'contain'=>['Merchants','Purchases'],
                'finder'=>['User'=>['userid'=>$user]]
            ];
        }

        $unclaims = $this->paginate($this->ViewUnclaimDirectpurchase);
        $this->set('unclaims',$unclaims);
        if($this->request->is("ajax")){
            $this->ViewBuilder()->autoLayout(false);
            $this->renderView('claimstatus_ajax');
        }
        else{
            $this->renderView();
        }
    }

    public function purchaseVoucher($id = null)
    {
        if($id==null)
            throw new BadRequestException();

        $this->loadModel('Smashdeal.ViewUnclaimDirectpurchase');

        $voucher = $this->ViewUnclaimDirectpurchase->get($id,['contain'=>['Merchants','Purchases'=>function($q){return $q->contain(['TbDealProducts']);}]]);

        $this->set('voucher',$voucher);

        $this->renderView();
    }

    public function history()
    {
        $this->loadModel('Smashdeal.ViewClaimDirectpurchase');

        $user = $this->Auth->user('id');

        if($this->request->is("mobile")){
            $this->paginate = [
                'limit'=>1,
                'contain'=>['Merchants','Purchases'],
                'finder'=>['User'=>['userid'=>$user]]
            ];
        }
        else
        {
            $this->paginate = [
                'contain'=>['Merchants','Purchases'],
                'finder'=>['User'=>['userid'=>$user]]
            ];
        }

        $unclaims = $this->paginate($this->ViewClaimDirectpurchase);
        $this->set('unclaims',$unclaims);
        if($this->request->is("ajax")){
            $this->ViewBuilder()->autoLayout(false);
            $this->renderView('history_ajax');
        }
        else{
            $this->renderView();
        }
    }

}