<?php
namespace Smashdeal\Controller\Backend;

use Smashdeal\Controller\BackendAppController as AppController;
use Smashdeal\Form\GeneralConfigForm;

/**
 * Configs Controller
 *
 * @property \Smashdeal\Model\Table\ConfigsTable $Configs
 */
class ConfigsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('System.TbSysConfigs');
        $this->loadModel('System.TbSysUserPreferences');
        $this->loadComponent('System.Terms');
    }

    private function collectConfigs($groupname)
    {
        $rawconfigs = $this->TbSysConfigs->find('all')->where(['groupname'=>$groupname]);
        return $this->groupingConfigs($rawconfigs);
    }

    private function groupingConfigs($rawconfigs)
    {
        $configs = [];
        foreach($rawconfigs->toArray() as $key=>$config)
        {
            $configs[$config["code"]]["id"] = $config["id"];
            $configs[$config["code"]]["value"] = $config["value"];
        }
        return $configs;
    }

    private function collectLookupValue($groupname,$value,$orderby)
    {
        $this->loadModel('System.TbSysLookupDetails');
        $rawconfigs = $this->TbSysLookupDetails->find('list',[
            "keyField"=>"id",
            "valueField"=>$value
        ])->contain('TbSysLookup')->where(["TbSysLookup.code"=>$groupname])->order(['TbSysLookupDetails.'.$orderby=>"ASC"]);
        return $rawconfigs;
    }

    private function isDirty($ori,$key,$value)
    {
        if($ori[$key]["value"] == $value)
        {
            return false;
        }
        return true;
    }

    public function general()
    {
        $configs = $this->collectConfigs("SMGENERAL");
        $generalConfig = new GeneralConfigForm();
        if($this->request->is(["post","put"]))
        {
            try{
                foreach($this->request->data as $key=>$value)
                {
                    if($this->isDirty($configs,$key,$value["value"]))
                    {
                        $data = $this->TbSysConfigs->get($value["id"]);
                        $data->value = $value["value"];
                        $this->TbSysConfigs->save($data);
                    }
                }
                //rewrite all config at session & cookies;
                $this->rewriteConfiguration(true);
                $this->Flash->success(__d("system","Successfully update configurations"));
                $this->redirect(["action"=>"general"]);
            }
            catch(Exception $ex)
            {
                $this->Flash->error(__d("system","Fail to update configurations"));
            }
        }
        if($this->request->is("get"))
        {
            $this->request->data = $configs;
        }
        $this->set('generalConfig',$generalConfig);
    }

}
