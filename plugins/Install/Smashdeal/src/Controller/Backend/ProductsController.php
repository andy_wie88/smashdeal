<?php
namespace Smashdeal\Controller\Backend;

use Cake\Cache\Cache;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\I18n\Date;
use Cake\Utility\Text;
use Smashdeal\Controller\BackendAppController as AppController;

/**
 * Products Controller
 *
 * @property \Smashdeal\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{

    public $paginate = [
        'limit' => 10,
        'contain' => ['Langs','Metas']
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Smashdeal.TbDealProducts');
        $this->loadComponent('System.Date');
        $this->loadComponent('System.Terms');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
        $this->loadModel('System.TbSysWidgets');
        Cache::delete('product_top_widget','long');
        if(($top_widgets = Cache::read('product_top_widget','long')) === false){
            $top_widgets = $this->TbSysWidgets->find('all',['contain'=>['Groups']])->matching('Groups',function($q){
                return $q->where(['Groups.code'=>'ADMIN_PRODUCT_TOP']);
            });
            Cache::write('product_top_widget',$top_widgets->toArray(),'long');
        }
        if(($bottom_widgets = Cache::read('product_bottom_widget','long')) === false) {
            $bottom_widgets = $this->TbSysWidgets->find('all', ['contain' => ['Groups']])->matching('Groups', function ($q) {
                return $q->where(['Groups.code' => 'ADMIN_PRODUCT_BOTTOM']);
            });
            Cache::write('product_bottom_widget',$bottom_widgets->toArray(),'long');
        }
        if(($publish_widgets = Cache::read('product_publish_widget','long')) === false) {
            $publish_widgets = $this->TbSysWidgets->find('all', ['contain' => ['Groups']])->matching('Groups', function ($q) {
                return $q->where(['Groups.code' => 'ADMIN_PRODUCT_PUBLISH']);
            });
            Cache::write('product_publish_widget',$publish_widgets->toArray(),'long');
        }
        if(($attribute_widgets = Cache::read('product_attribute_widget','long')) === false) {
            $attribute_widgets = $this->TbSysWidgets->find('all', ['contain' => ['Groups']])->matching('Groups', function ($q) {
                return $q->where(['Groups.code' => 'ADMIN_PRODUCT_ATTRIBUTE']);
            });
            Cache::write('product_attribute_widget',$attribute_widgets->toArray(),'long');
        }
        $this->set('top_widgets',$top_widgets);
        $this->set('bottom_widgets',$bottom_widgets);
        $this->set('publish_widgets',$publish_widgets);
        $this->set('attribute_widgets',$attribute_widgets);
    }

    public function index($status =  null)
    {
        $product = $this->TbDealProducts->newEntity();

        $allproducts = $this->TbDealProducts->find('all');
        if($status == null)
        {
            $products = $this->paginate($this->TbDealProducts);
        }
        else {
            $this->paginate = [
                'finder' => [
                    'status' => ['s'=>$status]
                ]
            ];
            $products = $this->paginate($this->TbDealProducts);
        }

        $this->set('allproducts',$allproducts);
        $this->set('products',$products);
        $this->set('product',$product);
        $this->set('status',$status);
    }

    public function search($status = null)
    {
        $q = $this->request->query('s');
        $defaultlang = $this->request->session()->read('Config')["SITEDFLTLG_VALUE"];
        $product = $this->TbDealProducts->newEntity();
        $allproducts = $this->TbDealProducts->find('name',['s'=>$q,'status'=>$status,'defaultlang'=>$defaultlang]);
        $this->paginate = [
            'finder'=> [
                'name'=>['s'=>$q,'status'=>$status,'defaultlang'=>$defaultlang]
            ]
        ];
        $products = $this->paginate($this->TbDealProducts);
        $this->set('allproducts',$allproducts);
        $this->set('products',$products);
        $this->set('product',$product);
        $this->set('status',$status);
        $this->set('s',$q);
        $this->render('index');
    }

    private function calcBargainQuota($product)
    {
        $selisi = $product->retailprice - $product->bargainstart;
        $safetymargin = $this->request->session()->read('Config')["SAFETY_MARGIN"];
        $additional = ($selisi*($safetymargin/100));
        $quotaprice = $selisi+$additional;
        $tokenprice = $this->request->session()->read('Config')["SINGLETOKENPRICE"];
        $tokenamount = $product->bargaintoken;

        $quotatoken = ($tokenprice*(50/100))*$tokenamount;

        $bargainquota = $quotaprice/$quotatoken;

        return $bargainquota;
    }

    public function add()
    {
        $this->loadModel('System.TbSysTerms');
        if(($results = Cache::read('all_product_category','short_mem')) === false) {
            $allcategories = $this->TbSysTerms->find('type', ['type' => "product_category"])->where(['TbSysTerms.isactive' => true])->order(['TbSysTerms.created' => 'ASC']);
            $results = $this->Terms->collectCategoryNested($allcategories->toArray());
            Cache::write('all_product_category',$results,'short_mem');
        }
        $this->set('allcategories',$results);

        $mostcategories = $this->getMostUsedCategories();
        $this->set('mostcategories',$mostcategories);
        $alltags = $this->TbSysTerms->find('type', ['type' => "product_tag"]);
        Cache::write('all_product_tag',$alltags->toArray(),'short_mem');
        $this->set('tags',$alltags);
        if(($langs = Cache::read('post_language','short_mem')) === false) {
            $langs = $this->loadLanguages();
            Cache::write('post_language',$langs,'short_mem');
        }
        $product = $this->TbDealProducts->newEntity();
        $product->publishstatus = 1;
        $product->visibility = 1;
        if($this->request->is("post"))
        {
            if($this->request->data["savetype"] == "publish")
            {
                $this->request->data["publishstatus"] = 3;
            }
            else if($this->request->data["savetype"] == "draft")
            {
                if($this->request->data["publishstatus"]==3)
                {
                    $this->request->data["publishstatus"] = 1;
                }
            }
            if($this->request->data["date"]!="")
            {
                $timestamp = $this->Date->convert($this->request->data["date"],$session = $this->request->session()->read('Config'));
                $time = new Date($timestamp);
                $this->request->data["date"] = $time;
            }
            else{
                $this->request->data["date"] = new Date();
            }
            // if($this->request->data["dealend"]!="")
            // {
            //     $timestamp = $this->Date->convert($this->request->data["dealend"],$session = $this->request->session()->read('Config'));
            //     $time = new Date($timestamp);
            //     $this->request->data["dealend"] = $time;
            // }
            // else{
            //     $this->request->data["dealend"] = new Date();
            // }
            $this->collectingLangs($langs,$this->request->data["langs"]);

            if(isset($this->request->data["tags"])){
                $results = $this->collectingTags($this->request->data["tags"]);

                foreach($results as $result)
                {
                    array_push($this->request->data["terms"],$result);
                }
            }

            foreach($this->request->data["terms"] as $key=>$data)
            {
                $this->request->data["terms"][$key]["_joinData"]["objecttype"] = "product";
            }
            if(isset($this->request->data["medias"]))
            {
                $isdefault = false;
                foreach($this->request->data["medias"] as $media)
                {
                    if($media == true){
                        $isdefault = true;
                        break;
                    }
                }
            }
            if(!$isdefault)
            {
                $this->request->data["medias"][0]["isdefault"]=true;
            }
            $product = $this->TbDealProducts->newEntity($this->request->data,['associated'=>['Metas','Langs','Terms','Medias']]);
            foreach($product->langs as $body)
            {
                if($body->languageid == $this->request->session()->read('Config')["SITEDFLTLG_VALUE"])
                {
                    $content = $body;
                }
                $body->slug = Text::slug($body->name,"_");
            }
            if(isset($content))
            {
                $product->productname = $content->name;
                $product->slug = $content->slug;
                $product->description = $content->description;
            }
            $product->bargainquota = $this->calcBargainQuota($product);
            if($this->request->data["publishstatus"] == 3 && !$this->isHasRight("PRODUCTPUBLISH"))
            {
                $this->Flash->error(__d("system","You have no right to publish a product"));
            }
            else{
                $product->slug = Text::slug($product->productname,"_");
                $product->isclosed = false;
                if($this->TbDealProducts->save($product))
                {
                    //Execute After Save Component;`
                    $this->Flash->success(__d("system","Successfully add a new product"));
                    $this->redirect(['action'=>'index']);
                }
                else{
                    $this->Flash->error(__d("system","Fail to add a new product"));
                }
            }
        }
        $this->loadModel('Smashdeal.TbDealMerchants');
        $merchants = $this->TbDealMerchants->find('list')->where(['isactive'=>true]);
        $this->set('merchants',$merchants);
        $this->set('product',$product);
        $this->set('langs',$langs);
    }

    public function edit($id)
    {
        $this->loadModel('System.TbSysTerms');
        if(($results = Cache::read('all_product_category','short_mem')) === false) {
            $allcategories = $this->TbSysTerms->find('type', ['type' => "product_category"])->where(['TbSysTerms.isactive' => true])->order(['TbSysTerms.created' => 'ASC']);
            $results = $this->Terms->collectCategoryNested($allcategories->toArray());
            Cache::write('all_product_category',$results,'short_mem');
        }
        $this->set('allcategories',$results);

        $mostcategories = $this->getMostUsedCategories();
        $this->set('mostcategories',$mostcategories);
        $alltags = $this->TbSysTerms->find('type', ['type' => "product_tag"]);
        Cache::write('all_product_tag',$alltags->toArray(),'short_mem');
        $this->set('tags',$alltags);
        if(($langs = Cache::read('post_language','short_mem')) === false) {
            $langs = $this->loadLanguages();
            Cache::write('post_language',$langs,'short_mem');
        }
        try {
            $product = $this->TbDealProducts->get($id, ['contain' => ['Metas', 'Langs', 'Terms','Medias']]);
        }
        catch (RecordNotFoundException $ex)
        {
            $this->Flash->error(__d("system","Record not found"));
            $this->redirect(["action"=>'index']);
        }
        if($this->request->is(["put","patch","post"]))
        {
            if($product->isclosed==true)
            {
                $this->Flash->error(__d("smashdeal","Product has been closed can't be edit anymore"));
                $this->redirect(['action'=>'edit',$id]);
                return;
            }
            if($this->request->data["savetype"] == "publish")
            {
                $this->request->data["publishstatus"] = 3;
            }
            else if($this->request->data["savetype"] == "draft")
            {
                if($this->request->data["publishstatus"]==3)
                {
                    $this->request->data["publishstatus"] = 1;
                }
            }

            if($this->request->data["date"]!="")
            {
                $timestamp = $this->Date->convert($this->request->data["date"],$session = $this->request->session()->read('Config'));
                $time = new Date($timestamp);
                $this->request->data["date"] = $time;
            }
            else{
                $this->request->data["date"] = new Date();
            }
            // if($this->request->data["dealend"]!="")
            // {
            //     $timestamp = $this->Date->convert($this->request->data["dealend"],$session = $this->request->session()->read('Config'));
            //     $time = new Date($timestamp);
            //     $this->request->data["dealend"] = $time;
            // }
            // else{
            //     $this->request->data["dealend"] = new Date();
            // }
            $this->collectingLangs($langs,$this->request->data["langs"]);

            if(isset($this->request->data['tags'])){
                $results = $this->collectingTags($this->request->data["tags"]);

                foreach($results as $result)
                {
                    array_push($this->request->data["terms"],$result);
                }
            }

            foreach($this->request->data["terms"] as $key=>$data)
            {
                $this->request->data["terms"][$key]["_joinData"]["objecttype"] = "product";
            }

            $this->collectingMeta($product->metas,$this->request->data["metas"]);
            unset($product->metas);
            unset($product->medias);
            if(isset($this->request->data["medias"]))
            {
                $isdefault = false;
                foreach($this->request->data["medias"] as $media)
                {
                    if($media == true){
                        $isdefault = true;
                        break;
                    }
                }
            }
            if(!$isdefault)
            {
                $this->request->data["medias"][0]["isdefault"]=true;
            }
            $product = $this->TbDealProducts->patchEntity($product,$this->request->data);
            foreach($product->langs as $body)
            {
                if($body->languageid == $this->request->session()->read('Config')["SITEDFLTLG_VALUE"])
                {
                    $content = $body;
                }
                $body->slug = Text::slug($body->name,"_");
            }
            if(isset($content))
            {
                $product->productname = $content->name;
                $product->slug = $content->slug;
                $product->description = $content->description;
            }
            $product->bargainquota = $this->calcBargainQuota($product);
            if($this->request->data["publishstatus"] == 3 && !$this->isHasRight("PRODUCTPUBLISH"))
            {
                $this->Flash->error(__d("system","You have no right to publish a post"));
            }
            else{
                $product->slug = Text::slug($product->productname,"_");
                if($this->TbDealProducts->save($product))
                {
                    $this->Flash->success(__d("system","Successfully modify a post"));
                    $this->redirect(['action'=>'index']);
                }
                else{
                    $this->Flash->error(__d("system","Fail to modify a post"));
                }
            }
        }
        $this->loadModel('Smashdeal.TbDealMerchants');
        $merchants = $this->TbDealMerchants->find('list')->where(['isactive'=>true]);
        $this->set('merchants',$merchants);
        $this->set('product',$product);
        $this->set('langs',$langs);
        $this->render('add');
    }


    private function collectingMeta($old_metas,&$new_metas)
    {
        $index  = 0;
        foreach($old_metas as $old_meta)
        {
            foreach($new_metas as $new_meta)
            {
                if($new_meta["name"]!=$old_meta["name"])
                {
                    $additionals[$index]["name"] = $old_meta["name"];
                    $additionals[$index]["value"] = $old_meta["value"];
                }
            }
            $index++;
        }
        if(isset($additionals))
        {
            foreach($additionals as $additional){
                array_push($new_metas,$additional);
            }
        }
    }

    private function loadLanguages(){
        $this->loadModel('System.TbSysLookupDetails');
        $this->loadModel('System.TbSysWebLanguages');
        $langs = $this->TbSysWebLanguages->find('all');
        $results = [];
        foreach($langs as $lang)
        {
            $results[$lang["languageid"]] = $this->TbSysLookupDetails->get($lang['languageid'])->label;
        }
        return $results;
    }

    private function collectingLangs($langs,&$new_langs)
    {
        $index = 0;
        foreach($langs as $lang_id=>$lg)
        {
            $found = false;
            foreach($new_langs as $lang)
            {
                if($lang["languageid"]==$lang_id)
                {
                    $found = true;
                    break;
                }
            }
            if(!$found)
            {
                $additionals[$index]["languageid"] = $lang_id;
                $additionals[$index]["name"] = "";
                $additionals[$index]["slug"] = "";
                $additionals[$index]["description"] = "";
                $index++;
            }
        }
        if(isset($additionals))
            foreach($additionals as $additional)
                array_push($new_langs,$additional);
    }

    private function collectingTags($tags)
    {
        $results = [];
        if(isset($tags)) {
            $alltags = $this->TbSysTerms->find('type', ['type' => "product_tag"]);
            foreach ($tags as $tag) {
                $found = false;
                foreach ($alltags as $key => $t) {
                    if ($t["id"] == $tag) {
                        $found = true;
                        $results[]["id"] = $t["id"];
                    }
                }
                if (!$found) {
                    $results[] = [
                        "name" => $tag,
                        "slug" => Text::slug($tag),
                        "taxonomy" => "product_tag"
                    ];
                }
            }
        }
        return $results;
    }

    private function getMostUsedCategories()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute("select * from (SELECT *,(SELECT count(*) FROM tb_sys_term_relationships where termid = id) as postusage FROM tb_sys_terms WHERE taxonomy = 'product_category') as a WHERE a.postusage > 0 order by a.postusage DESC limit 10")->fetchAll('assoc');
        return $results;
    }
}
