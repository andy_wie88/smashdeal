<?php
namespace Smashdeal\Controller\Backend;

use System\Controller\BackendAppController as AppController;

/**
 * Vouchers Controller
 *
 * @property \Smashdeal\Model\Table\VouchersTable $Vouchers
 */
class VouchersController extends AppController
{

    public function initialize()
    {
    	parent::initialize();
        $this->loadModel('Smashdeal.TbDealBtPaymentConfirmations');
    }

    public function confirmation()
    {
        $confirmations = $this->TbDealBtPaymentConfirmations->find('all')->contain(['BankAccount','Purchase'])->where(['status'=>1])->order(['TbDealBtPaymentConfirmations.created'=>'DESC']);

        $this->set('confirmations',$confirmations);
    }

}