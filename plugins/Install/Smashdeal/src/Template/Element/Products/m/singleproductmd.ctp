<div class="sm-bargain-box row">
    <div class="ribbon">
        Hemat <?= round((($product['retailprice']-$product['bargainstart'])/$product['retailprice']) * 100) ?>%
    </div>
    <div class="sm-bargain-image">
        <?php
            if(isset($product->medias)){
                foreach($product->medias as $media)
                {
                    if($media["isdefault"])
                    {
                        $image = $media["mediaid"];
                        break;
                    }
                }
                if(!isset($image) && count($product->medias)>0)
                {
                    $image = $bean->medias[0]["mediaid"];
                }
            }
        ?>
        <?php if(isset($image)): ?>
            <?= $this->cell('System.MediaRenderer',['mediaId'=>$image]); ?>
        <?php else: ?>
            <?= $this->Html->image('no_image.png') ?>
        <?php endif; ?>
    </div>
    <?php $language = []; ?>
    <?php foreach($product->langs as $lng){
        if($lng["languageid"]==$config_front["SITEDFLTLG_VALUE"])
        {
            $language = $lng;
            break;
        }
    } ?>
    <div class="sm-bargain-info">
        <div class="sm-bargain-title-wrapper">
            <h3 class="sm-bargain-title"><?= (isset($language["name"])&&$language["name"]!="")?$language["name"]:$product["productname"]; ?></h3>
        </div>
        <div class="sm-bargain-info-wrapper clearfix">
            <div class="row">
                <div class="col s6 sm-extra-info sm-bargain-merchant">
                    <?= $product->merchant["name"] ?>
                </div>
                <div class="col s6 right-align sm-extra-info sm-bargain-timer-sl">
                    <?= $this->cell('Smashdeal.Remaintime',['bargainquota'=>$product['bargainquota'],'bargaintimes'=>$product->bargaintimes]) ?>
                </div>
            </div>
        </div>
        <div class="sm-bargain-info-wrapper clearfix">
            <div class="col s9 no-padding">
                <div class="sm-bargain-retail-price">
                    <?= $this->Number->currency($product->bargainstart) ?>-<?= $this->Number->currency($product->bargainend); ?>
                </div>
                <div class="sm-bargain-range-price  sm-extra-info">
                    <?= $this->Number->currency($product->retailprice); ?>
                </div>
            </div>
            <div class="col s3 no-padding sm-bargain-token center-align">
                <?= $this->Number->format($product->bargaintoken); ?><br/>
                <?= ($product->bargaintoken>=2)?__d("smashdeal","Tokens"):__d("smashdeal","Token"); ?>
            </div>
        </div>
    </div>
</div>