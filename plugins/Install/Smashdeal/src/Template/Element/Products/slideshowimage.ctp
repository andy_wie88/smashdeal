<?= $this->AssetCompress->script('System.FeatureImage',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.FeatureImage',['block'=>'css']) ?>
<?= $this->Html->script('Smashdeal.products/slideshowimage',['block'=>'footer-script']); ?>
<?php
    $index=0;
    if(isset($bean->medias)){
        $index = count($bean->medias);
    }
?>
<div class="box box-default" id="slideshow" data-index="<?= $index ?>">
    <div class="box-header">
        <h2 class="box-title"><?= __d("system","Slideshow Image") ?></h2>
    </div>
    <div class="box-body">
        <div id="image-wrapper">
            <?php if(isset($bean->medias)): ?>
                <?php foreach($bean->medias as $index=>$media): ?>
                    <div class="image-wrapper" id="<?= isset($media->id)?$media->id:$index; ?>" data-initial="true">
                        <?= $this->cell('System.MediaRenderer',['mediaId'=>$media["mediaid"]]) ?>
                        <input type="hidden" name="medias[<?= $index ?>][mediaid]" value="<?= $media["mediaid"] ?>" />
                        <input type="hidden" name="medias[<?= $index ?>][index]" value="<?= $media["index"] ?>" />
                        <input type="hidden" name="medias[<?= $index ?>][isdefault]" class="isdefault" value="<?= ($media["isdefault"]==1)?'true':'false' ?>" />
                        <a href="#" class="delete-image pull-right text-danger">delete</a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div id="get-image-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'getMedia','prefix'=>'Ajax','plugin'=>'System']); ?>"></div>
        <div>
            <div id="media-list-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaList','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
            <div id="media-upload-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'upload','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
            <div id="media-attribute-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaAttribute','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
            <a href="#" id="media-selector"><?= __d("system","select images"); ?></a>
        </div>
    </div>
</div>