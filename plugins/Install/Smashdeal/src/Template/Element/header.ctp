<header>
    <section class="fixed-header">
        <div class="container">
            <div class="row header-list">
                <div class="row">
                    <div class="col-md-4 no-padding-right">
                        <?= $this->cell('Smashdeal.DealUserToken') ?>
                    </div>
                    <div class="col-md-4">
                        <?php if(isset($q)): ?>
                            <?= $this->cell('Smashdeal.ProductSearch',['q'=>$q]); ?>
                        <?php else: ?>
                            <?= $this->cell('Smashdeal.ProductSearch'); ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        <div class="user-selection pull-right">
                            <?= $this->cell('Smashdeal.SmUsers') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>    
    <section class="header-bar sm-header-bar">
        <div class="container">
            <div class="row header-list">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="img-wrapper">
                        <h1 class="hide">
                            <?= $config_front["SITETITLE"] ?>
                        </h1>
                        <?= $this->Html->link($this->Html->image('assets_logo_mobile.png'),['controller'=>'Products','action'=>'homepage','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false]) ?>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row second-nav">
                        <div class="col-md-9">
                            <?= $this->cell('Smashdeal.MainNavigation',['config'=>$config_front]); ?> 
                        </div>
                        <div class="col-md-3">
                            <div class="user-selection pull-right">
                                <?= $this->cell('Smashdeal.MyBargain'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>