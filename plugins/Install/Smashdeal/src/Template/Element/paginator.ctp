<?php if($this->request->is("mobile")): ?>
<div class="center-align">
	<ul class="pagination">
	    <?= $this->Paginator->prev() ?>
	    <?= $this->Paginator->numbers() ?>
	    <?= $this->Paginator->next() ?>
	</ul>
</div>
<?php else: ?>
<div class="pull-right"> 
    <ul class="pagination">
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->next() ?>
    </ul>
</div>
<?php endif; ?>