<div class="container center-align">
    <div class="col m6 s12 offset-m3">
        <div class="sm-box clearfix">
            <h2 class="section-header">
                <?= __d("smashdeal","Login") ?>
            </h2>
            <?= $this->Form->create($user,['class'=>'form-horizontal','url'=>['action'=>'login']]) ?>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label"><?= __d("smashdeal", "Username") ?></label>
                    <div class="col-md-8 no-padding-md-left">
                        <?= $this->Form->input('username',['class'=>'form-control','label'=>false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label"><?= __d("smashdeal", "Password") ?></label>
                    <div class="col-md-8 no-padding-md-left">
                        <?= $this->Form->input('password',['class'=>'form-control','label'=>false]) ?>
                    </div>
                </div>
                <div class="extra-group clearfix">
                    <div class="col-md-6 text-right col-md-push-6s">
                        <?= $this->Html->link(__d("smashdeal", "Forgot Password?"),['controller'=>'Users','action'=>'forgotPassword','plugin'=>'Smashdeal','prefix'=>'Merchant']) ?>
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col-md-5 pull-right">
                        <button type="submit" class="btn btn-smd btn-sm-primary"><?= __d("smashdeal", "Login") ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>