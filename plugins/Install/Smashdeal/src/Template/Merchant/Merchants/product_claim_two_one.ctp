<div class="sm-box">
    <h1 class="section-header"><?= __d("smashdeal", "Klaim Produk - Langkah Kedua") ?></h1>
    <div class="row">
        <div class="col m12 s12">
            <?= $this->Form->create($claim,['class'=>'form-horizontal']) ?>
                <div class="col m6 s12">
                    <?= $this->Form->input('vouchercode',['label'=>'Kode Voucher','readonly'=>true]) ?>
                </div>
                <div class="col m12 s12">
                    <div class="section-header">
                        Detail Kemenangan
                    </div>
                </div>
                <div class="col s12">
                    <table class="table cart-table">
                        <thead>
                            <tr>
                                <th width="40%" class="text-center"><?= __d("smashdeal","ITEM") ?></th>
                                <th width="5%"></th>
                                <th width="10%" class="text-center"><?= __d("smashdeal","QUANTITY") ?></th>
                                <th width="20%" class="text-center"><?= __d("smashdeal","Harga Yang Dimenangkan") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="item">
                                        <strong><?= $product['productname'] ?></strong>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="item">
                                        1
                                    </div>
                                </td>
                                <td class="right-align">
                                    <div class="item">
                                        <?= $this->Number->currency($summary['price']) ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col m12 s12">
                    <div class="section-header">
                        Data Pemenang
                    </div>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('idno',['label'=>'No. KTP','value'=>$winnerUser['idno'],'readonly'=>true]) ?>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('name',['label'=>'Nama','value'=>$winnerUser['name'],'readonly'=>true]) ?>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('address',['label'=>'Alamat','value'=>$winnerUser['address'],'readonly'=>true]) ?>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('phoneno',['label'=>'No. Handphone','value'=>$winnerUser['phoneno'],'readonly'=>true]) ?>
                </div>
                <div class="col m12 s12">
                    <div class="section-header">
                        Password User
                    </div>
                    <p>Silahkan meminta user untuk meng-input password</p>
                </div>
                <div class="col m6 s12">
                    <input type="hidden" name="step" value="three"/>
                    <?= $this->Form->input('password',['label'=>'Password']) ?>
                </div>
                <div class="button-group clearfix">
                    <div class="center-align">
                        <button type="submit" class="btn btn-smd btn-sm-primary">
                            <?= __d("smashdeal","Klaim Produk") ?>
                        </button>
                    </div>
                </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>