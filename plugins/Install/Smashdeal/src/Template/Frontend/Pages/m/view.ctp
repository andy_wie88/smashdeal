<div class="container">
	<div class="single-post">
		<div class="post-header">
			<?php 
				$hascategory = false;
				foreach($post['terms'] as $term){
					if($term['taxonomy']=='category')
						$hascategory = true;
				} 
			?>
			<h2>
				<?= $this->Html->link($this->cell('System.LangRenderer',['source'=>$post,'sourcetype'=>'POST','field'=>'title']),['controller'=>'Posts','action'=>'view','plugin'=>'System','prefix'=>'Frontend',$post->slug]); ?>
			</h2>
			<div class="title-divider"></div>
			<div class="post-at"><?= __d("icms","Posted on ") ?><span class="publishtime"><?= $post['date']->i18nFormat($config_front['SITEDTFORM']) ?></span>
			</div>
		</div>
		<?php 
			$feature_image = null;
			foreach($post['metas'] as $meta)
			{
				if($meta['name']=='feature_image')
					$feature_image = $meta['value'];
			}
			if($feature_image !=null): ?>
				<div class="post-image">
					<?php echo $this->Html->link($this->cell('System.MediaRenderer',['mediaId'=>$feature_image]),['controller'=>'Posts','action'=>'view','plugin'=>'System','prefix'=>'Frontend',$post['slug']],['escape'=>false]);
					?>
				</div>
			<?php endif; ?>
		<div class="post-body">
			<?= $this->cell('System.PostBodyRenderer',['post'=>$post]) ?> 
		</div>
	</div>
</div>