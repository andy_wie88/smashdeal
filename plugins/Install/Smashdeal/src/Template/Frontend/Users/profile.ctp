<?= $this->extend('template'); ?>
<?= $this->start('profile') ?>
<?= $this->AssetCompress->script('Smashdeal.Profile23432',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('Smashdeal.Profile23432',['block'=>'css']) ?>
<div class="sm-box">
    <h2 class="section-header">
        <?= __d("smashdeal","Edit Profile") ?>
    </h2>
    <?= $this->Form->create($user,['class'=>'form-horizontal']) ?>
        <div class="form-group">
            <label for="#username" class="control-label col-md-3">
                <?= __d("smashdeal","Username") ?>
            </label>
            <div class="col-md-6">
                <?= $this->Form->input('username',['label'=>false,'div'=>false,'readonly'=>true,'class'=>'form-control']) ?>
            </div>
        </div>
        <hr class="separator">
        <div class="form-group required">
            <label for="#name" class="control-label col-md-3">
                <?= __d("smashdeal","ID Number") ?>
            </label>
            <div class="col-md-6">
                <?= $this->Form->input('idno',['label'=>false,'div'=>false,'class'=>'form-control']) ?>
                <small class='info'><?= __d("smashdeal","(Fill with your ID Number)") ?></small>
            </div>
        </div>
        <div class="form-group required">
            <label for="#name" class="control-label col-md-3">
                <?= __d("smashdeal","Name") ?>
            </label>
            <div class="col-md-6">
                <?= $this->Form->input('name',['label'=>false,'div'=>false,'class'=>'form-control']) ?><small class='info'><?= __d("smashdeal","(Please fill your name as written on your ID Card)") ?></small>
            </div>
        </div>
        <div class="form-group">
            <label for="#name" class="control-label col-md-3">
                <?= __d("smashdeal","Birthday") ?>
            </label>
            <div class="col-md-6">
                <?= $this->Form->input('birthday',['label'=>false,'div'=>false,'type'=>'text','class'=>'form-control datepick','value'=>(isset($user->birthday)?$user->birthday->i18nFormat($config_front["SITEDTFORM"]):"")]); ?>
                <small class="info"><?= __d("smashdeal","(Fill your birthday and got special offer at your birthday)"); ?></small>
            </div>
        </div>
        <div class="form-group required">
            <label for="#name" class="control-label col-md-3">
                <?= __d("smashdeal","Gender") ?>
            </label>
            <div class="col-md-3">
                <?php $option = ["M"=>__d("smashdeal","Male"),"F"=>__d("smashdeal","Female"),"O"=>__d("smashdeal","Others")]; ?>
                <?= $this->Form->input('gender',['label'=>false,'div'=>false,'options'=>$option,'class'=>'form-control']) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="#name" class="control-label col-md-3">
                <?= __d("smashdeal","Address") ?>
            </label>
            <div class="col-md-6">
                <?= $this->Form->input('address',['label'=>false,'div'=>false,'class'=>'form-control']) ?>
                <small class="info"><?= __d("smashdeal","(Please put address as  in your ID Card)"); ?></small>
            </div>
        </div>
        <div class="form-group required">
            <label for="#email" class="control-label col-md-3">
                <?= __d("smashdeal","Email") ?>
            </label>
            <div class="col-md-6">
                <?= $this->Form->input('email',['label'=>false,'div'=>false,'class'=>'form-control']) ?>
            </div>
        </div>
        <div class="form-group required">
            <label for="#phone" class="control-label col-md-3">
                <?= __d("smashdeal","Handphone") ?>
            </label>
            <div class="col-md-6">
                <?= $this->Form->input('handphoneno',['label'=>false,'div'=>false,'id'=>'handphone','class'=>'form-control']) ?>
            </div>
        </div>
        <div class="button-group clearfix">
            <div class="col-md-3 col-md-push-4">
                <button type="submit" class="btn btn-smd btn-sm-primary"><?= __d("smashdeal","Update Profile") ?></button>
            </div>
        </div>
    <?= $this->Form->end() ?>
</div>
<?= $this->end(); ?>