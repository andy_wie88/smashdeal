<?= $this->extend('template'); ?>
<?= $this->start('profile') ?>
<div class="sm-box">
    <h1 class="section-header"><?= __d("smashdeal", "Change Password") ?></h1>
    <?php if(isset($change_error)): ?>
        <div class="error-message">
            <?= $change_error ?>
        </div>
    <?php  endif; ?>
    <?= $this->Form->create($user,['class'=>'form-horizontal','url'=>['action'=>'changePassword']]) ?>
        <div class="form-group">
            <label for="" class="col-md-3 control-label">
                <?= __d("smashdeal","Current Password") ?>
            </label>
            <div class="col-md-4">
                <?= $this->Form->input('password',['class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>__d("smashdeal","Current Password"),'value'=>'']) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 control-label">
                <?= __d("smashdeal","New Password") ?>
            </label>
            <div class="col-md-4">
                <?= $this->Form->input('new_password',['class'=>'form-control','type'=>'password','div'=>false,'label'=>false,'placeholder'=>__d("smashdeal","New Password"),'value'=>'']) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 control-label">
                <?= __d("smashdeal","Confirm Password") ?>
            </label>
            <div class="col-md-4">
                <?= $this->Form->input('confirm_password',['class'=>'form-control','type'=>'password','div'=>false,'label'=>false,'placeholder'=>__d("smashdeal","Confirm Password"),'value'=>'']) ?>
            </div>
        </div>
        <div class="button-group clearfix">
            <div class="col-md-3 col-md-push-2">
                <button type="submit" class="btn btn-smd btn-sm-primary"><?= __d("smashdeal","Change Password") ?></button>
            </div>
        </div>
    <?= $this->Form->end() ?>
</div>
<?= $this->end(); ?>