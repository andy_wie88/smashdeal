<?= $this->extend('template'); ?>
<?= $this->start('profile') ?>
<div class="sm-box clearfix">
	<h1 class="section-header"><?= __d("smashdeal", "Bargin Token Journal") ?></h1>
	<div>
		<table class="table table-sm table-bordered table-stripe">
			<thead>
				<tr>
					<td class='text-center' width="5%"><?= __d("smashdeal","No."); ?></td>
					<td class='text-center' width="30%"><?= __d("smashdeal","Time") ?></td>
					<td class='text-center'><?= __d("smashdeal","Note"); ?></td>
					<td class='text-center' width="10%"><?= __d("smashdeal","Debit"); ?></td>
					<td class='text-center' width="10%"><?= __d("smashdeal","Credit"); ?></td>
				</tr>
			</thead>
			<tbody>
				<?php if(count($journals)>0): ?>
					<?php foreach($journals as $key=>$journal): ?>
						<tr>
							<td class='text-center' width="2%">
								<?= $key+1; ?>
							</td>
    						<td width="25%">
    							<?= $journal['exttime']->i18nFormat($config_front['SITEDTFORM']." ".$config_front['SITETMFORM']) ?>
    						</td>
    						<td><?=  $journal['note'] ?></td>
    						<td class='text-center' width="10%">
    							(<?= $this->Number->format($journal['debit']) ?>)
    						</td>
    						<td class='text-center' width="10%">
    							<?= $this->Number->format($journal['credit']) ?>
    						</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
				<?php endif; ?>
			</tbody>
		</table>
		<?= $this->element('System.paginator') ?>
	</div>
</div>
<?= $this->end(); ?>