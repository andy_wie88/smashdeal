<?php if($success_validation["status"]): ?>
    <div class="container sm-main-content">
        <h1 class="section-header"><?= __d("smashdeal","Successfully Validate Your Email Account!!") ?></h1>
        <p>
            <?= __d("smashdeal","Your account has been successfully activated. You can now login via this {0}",$this->Html->link(__d("smashdeal","link"),['controller'=>'Users','action'=>'login','plugin'=>'Smashdeal','prefix'=>'Frontend'])); ?>
        </p>
    </div>
<?php else: ?>
    <?php if($success_validation["type"]=="FAILED"): ?>
        <div class="container sm-main-content">
            <h1 class="section-header"><?= __d("smashdeal","Failed Validate Your Email Account!!") ?></h1>
            <p>
                <?= __d("smashdeal","Fail to validate your email account. Please refresh to try again."); ?>
            </p>
        </div>
    <?php elseif ($success_validation["type"]=="INVALID"): ?>
        <div class="container sm-main-content">
            <h1 class="section-header"><?= __d("smashdeal","Failed Validate Your Account!!") ?></h1>
            <p>
                <?= __d("smashdeal","Fail to validate your email account due to validation code expiration. New email has been send to your email. Please check your inbox") ?>
            </p>
        </div>
    <?php elseif($success_validation["type"]=="INVALID"): ?>
        <div class="container sm-main-content">
            <h1 class="section-header"><?= __d("smashdeal","Failed Validate Your Account!!") ?></h1>
            <p>
                <?= __d("smashdeal","User is not valid. Please re register your account.") ?>
            </p>
        </div>
    <?php endif; ?>
<?php endif; ?>