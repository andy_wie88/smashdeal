<?= $this->AssetCompress->script('Smashdeal.Login123',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('Smashdeal.Login123',['block'=>'css']) ?>
<div class="container sm-main-content">
    <div class="col-md-8">
        <div class="sm-box">
            <h2 class="section-header">
                <?= __d("smashdeal","Register") ?>
            </h2>
            <?php if(isset($error)): ?>
                <div class="error-message">
                    <?= $error ?>
                </div>
            <?php  endif; ?>
            <?= $this->Form->create($user,['class'=>'form-horizontal','url'=>['action'=>'register']]) ?>
                <div class="form-group required">
                    <label for="#name" class="control-label col-md-3">
                        <?= __d("smashdeal","ID No.") ?>
                    </label>
                    <div class="col-md-6">
                        <?= $this->Form->input('idno',['label'=>false,'class'=>'form-control','required'=>true]) ?>
                        <small class='info'><?= __d("smashdeal","(Fill with your ID Number)") ?></small>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="#name" class="control-label col-md-3">
                        <?= __d("smashdeal","Name") ?>
                    </label>
                    <div class="col-md-6">
                        <?= $this->Form->input('name',['label'=>false,'class'=>'form-control','required'=>true]) ?>
                        <small class='info'><?= __d("smashdeal","(Please fill your name as written on your ID Card)") ?></small>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="#email" class="control-label col-md-3">
                        <?= __d("smashdeal","Email") ?>
                    </label>
                    <div class="col-md-6">
                        <?= $this->Form->input('email',['label'=>false,'class'=>'form-control','required'=>true]) ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="#phone" class="control-label col-md-3">
                        <?= __d("smashdeal","Handphone") ?>
                    </label>
                    <div class="col-md-6">
                        <?= $this->Form->input('handphoneno',['label'=>false,'class'=>'form-control','required'=>true,"id"=>"handphone"]) ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="#username" class="control-label col-md-3">
                        <?= __d("smashdeal","Username") ?>
                    </label>
                    <div class="col-md-6">
                        <?= $this->Form->input('username',['label'=>false,'class'=>'form-control','required'=>true]) ?>
                        <small class='info'><?= __d("smashdeal","(Hanya alphanumerik dan '_' diperbolehakan. minimal 5 karakter dan maksimal 12 karakter)") ?></small>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="#password" class="control-label col-md-3">
                        <?= __d("smashdeal","Password") ?>
                    </label>
                    <div class="col-md-6">
                        <?= $this->Form->input('password',['label'=>false,'class'=>'form-control','required'=>true]) ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="#confirmpass" class="control-label col-md-3">
                        <?= __d("smashdeal","Confirm Password") ?>
                    </label>
                    <div class="col-md-6">
                        <?= $this->Form->input('confirm_password',['type'=>'password','label'=>false,'class'=>'form-control','required'=>true]) ?>
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col-md-3 col-md-push-4">
                        <button type="submit" class="btn btn-smd btn-sm-primary"><?= __d("smashdeal","Join Us!!") ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <div class="sm-box clearfix">
            <h2 class="section-header">
                <?= __d("smashdeal","Login") ?>
            </h2>
            <?= $this->Form->create($user,['class'=>'form-horizontal','url'=>['action'=>'login']]) ?>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label"><?= __d("smashdeal", "Username") ?></label>
                    <div class="col-md-8 no-padding-md-left">
                        <?= $this->Form->input('username',['class'=>'form-control','label'=>false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label"><?= __d("smashdeal", "Password") ?></label>
                    <div class="col-md-8 no-padding-md-left">
                        <?= $this->Form->input('password',['class'=>'form-control','label'=>false]) ?>
                    </div>
                </div>
                <div class="extra-group clearfix">
                    <div class="col-md-6 text-right col-md-push-6">
                        <?= $this->Html->link(__d("smashdeal", "Forgot Password?"),['controller'=>'Users','action'=>'forgotPassword','plugin'=>'Smashdeal','prefix'=>'Frontend']) ?>
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col-md-5 pull-right">
                        <button type="submit" class="btn btn-smd btn-sm-primary"><?= __d("smashdeal", "Login") ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>