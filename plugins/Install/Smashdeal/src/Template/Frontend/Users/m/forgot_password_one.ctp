<div class="container">
    <div class="sm-box">
        <h1 class="section-header center-align"><?= __d("smashdeal", "Forgot Password?") ?></h1>
        <p><?= __d("smashdeal","Please input your registered email.") ?></p>
        <?= $this->Form->create($user) ?>
            <div class="form-group">
                <label for="" class="col-md-3 control-label">
                    <?= __d("smashdeal","Email") ?>
                </label>
                <div class="col-md-6">
                    <input type="text" name="email" class="form-control" placeholder="<?= __d("smashdeal","Please input your email") ?>">
                </div>
            </div>
            <div class="col s12 center-align">
                <button type="submit" class="waves-effect waves-light btn btn-sm-primary">
                    <?= __d("smashdeal","Send Code") ?>
                </button>
            </div>
        </form>
    </div>
</div>