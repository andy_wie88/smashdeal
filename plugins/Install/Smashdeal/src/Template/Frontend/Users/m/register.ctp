<div class="container sm-main-content">
    <div class="sm-box">
        <h1 class="section-header center-align">
            <?= __d("smashdeal","Register") ?>
        </h1>
        <?php if(isset($error)): ?>
            <div class="error-message">
                <?= $error ?>
            </div>
        <?php  endif; ?>
        <?= $this->Form->create($user,['url'=>['action'=>'register']]) ?>
            <div class="row">
                <div class="input-field col s12">
                    <input type="text" name="idno" class="validate" required="true" id="idno" value="<?= isset($this->request->data['idno'])?$this->request->data['idno']:'' ?>"/>
                    <label for="#idno">
                        <?= __d("smashdeal","ID No.") ?>
                    </label>
                    <small class='info'><?= __d("smashdeal","(Fill with your ID Number)") ?></small>
                </div>
                <div class="input-field col s12">
                    <input type="text" name="name" class="validate" required="true" id="name" value="<?= isset($this->request->data['name'])?$this->request->data['name']:'' ?>"/>
                    <label for="#name">
                        <?= __d("smashdeal","Name") ?>
                    </label>
                    <small class='info'><?= __d("smashdeal","(Please fill your name as written on your ID Card)") ?></small>
                </div>
                <div class="input-field col s12">
                    <input type="email" name="email" class="validate" required="true" id="email" value="<?= isset($this->request->data['email'])?$this->request->data['email']:'' ?>"/>
                    <label for="#email">
                        <?= __d("smashdeal","Email") ?>
                    </label>
                </div>
                <div class="input-field col s12">
                    <input type="text" name="handphoneno" class="validate" required="true" id="phone" value="<?= isset($this->request->data['phone'])?$this->request->data['phone']:'' ?>"/>
                    <label for="#phones">
                        <?= __d("smashdeal","Handphone") ?>
                    </label>
                </div>
                <div class="input-field col s12">
                    <input type="text" name="username" class="validate" required="true" id="username" value="<?= isset($this->request->data['username'])?$this->request->data['usernmae']:'' ?>"/>
                    <label for="#username">
                        <?= __d("smashdeal","Username") ?>
                    </label>
                    <small class='info'><?= __d("smashdeal","(Hanya alphanumerik dan '_' diperbolehakan. minimal 5 karakter dan maksimal 12 karakter)") ?></small>
                </div>
                <div class="input-field col s12">
                    <input type="password" name="password" class="validate" required="true" id="password"/>
                    <label for="#password">
                        <?= __d("smashdeal","Password") ?>
                    </label>
                </div>
                <div class="input-field col s12">
                    <input type="password" name="confirm_password" class="validate" required="true" id="confirm_password"/>
                    <label for="#confirm_password">
                        <?= __d("smashdeal","Confirm Password") ?>
                    </label>
                </div>
            </div>
            <div class="col s12 center-align">
                <button type="submit" class="waves-effect waves-light btn btn-sm-primary">
                    <?= __d("smashdeal","Join Us!!") ?>
                </button>
            </div>
        </form>
    </div>
</div>
<div class="center-align" style="margin-bottom: 20px;font-weight: bold;font-size: 14px;">
    <?= __d("smashdeal","Already has account?"); ?>
    <?= $this->Html->link(__d("smashdeal", "Login here"),['controller'=>'Users','action'=>'login','plugin'=>'Smashdeal','prefix'=>'Frontend']) ?>
</div>