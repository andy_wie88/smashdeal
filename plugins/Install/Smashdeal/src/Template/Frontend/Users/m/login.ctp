<?= $this->AssetCompress->script('Smashdeal.Login123',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('Smashdeal.Login123',['block'=>'css']) ?>
<div class="container sm-main-content">
    <div class="sm-box clearfix">
        <h1 class="section-header center-align">
            <?= __d("smashdeal","Login") ?>
        </h1>
        <?= $this->Form->create($user,['url'=>['action'=>'login']]) ?>
            <div class="input-field col s12">
                <input type="text" name="username" class="validate" required="true" id="username"/>
                <label for="#username">
                    <?= __d("smashdeal","Username") ?>
                </label>
            </div>
            <div class="input-field col s12">
                <input type="password" name="password" class="validate" required="true" id="password"/>
                <label for="#password">
                    <?= __d("smashdeal","Password") ?>
                </label>
            </div>
            <div class="col s12 center-align">
                <button type="submit" class="waves-effect waves-light btn btn-sm-primary">
                    <?= __d("smashdeal", "Login") ?>
                </button>
            </div>
        </form>
    </div>
</div>
<div class="center-align" style="margin-bottom: 20px;font-weight: bold;font-size: 14px;">
    <?= __d("smashdeal","Don't have account?"); ?>
    <?= $this->Html->link(__d("smashdeal", "Register here"),['controller'=>'Users','action'=>'register','plugin'=>'Smashdeal','prefix'=>'Frontend']) ?>
    <br><br>
    <?= __d("smashdeal","Forgot your Password?"); ?>
    <?= $this->Html->link(__d("smashdeal", "Reset your password"),['controller'=>'Users','action'=>'forgotPassword','plugin'=>'Smashdeal','prefix'=>'Frontend']) ?>
</div>