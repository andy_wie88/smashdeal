<div class="container sm-main-content">
	<h1 class="section-header center-align">
        <?= __d("smashdeal","Successfully Register New Account") ?>
    </h1>
    <p>
        <?= __d("smashdeal","You have successfully register your new account at DealSmash. We have send you a validation email. Please check your inbox and validate your email account.") ?>
    </p>
</div>