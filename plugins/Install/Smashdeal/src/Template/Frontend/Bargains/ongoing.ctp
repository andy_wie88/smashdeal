<?= $this->extend('../Users/template'); ?>
<?= $this->start('profile') ?>
<div class="sm-box clearfix">
	<h1 class="section-header"><?= __d("smashdeal", "On Going Bargain") ?></h1>
	<?php if(count($mybargains)>0): ?>
	    <?php foreach($mybargains as $product): ?>
	        <?= $this->Html->link($this->cell('Smashdeal.SingleProductLong',['productid'=>$product->id,'config_front'=>$config_front]),['controller'=>'Bargains','action'=>'view','prefix'=>'Frontend','plugin'=>'Smashdeal',$product->slug],['escape'=>false]); ?>
	    <?php endforeach; ?>
		<?= $this->element('System.paginator') ?>
	<?php else: ?>
	    <div class="col-md-12 no-product">
	        <?= __d("smashdeal","Anda tidak mempunyai bargain yang sedang berlangsung"); ?>
	    </div>
	<?php endif; ?>
</div>
<?= $this->end(); ?>