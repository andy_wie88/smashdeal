<?= $this->AssetCompress->script('Smashdeal.viewproduct',['block'=>'footer-script']); ?>
<?php if($pass == false && $product->visibility == 2): ?>
    <div class="container sm-main-content">
        <div class="sm-box clearfix">
            <h2 class="section-header">
                <?= __d("smashdeal","Access Product") ?>
            </h2>
            <?= $this->Form->create($product) ?>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label"><?= __d("smashdeal", "Password") ?></label>
                    <div class="col-md-8 no-padding-md-left">
                        <?= $this->Form->input('password',['placeholder'=>'Product Password','class'=>'form-control','label'=>false]) ?>
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col s12 center-align">
                        <button type="submit" class="waves-effect waves-light btn btn-sm-primary">
                            <?= __d("smashdeal","Access Product"); ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php else: ?>
    <div class="container sm-main-content sm-prd-detail">
        <div class="row sm-prd-detail-row">
            <div class="no-padding">
                <div class="ribbon">
                    Hemat <?= round((($product['retailprice']-$product['bargainstart'])/$product['retailprice']) * 100) ?>%
                </div>
                <div class="carousel carousel-slider" style="height: 360px;">
                    <?php  foreach($product->medias as $media): ?>
                        <a class="carousel-item" href="#<?= $media['id'] ?>!">
                            <?= $this->cell('System.MediaRenderer',['mediaId'=>$media["mediaid"],'plain'=>true]); ?>
                        </a>
                    <?php endforeach; ?>
                </div>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('.carousel').carousel({fullWidth: true});
                    });
                </script>
            </div>
            <div class="col s12 clearfix">
                <?php $lang = []; ?>
                <?php foreach($product->langs as $lng){
                    if($lng["languageid"]==$config_front["SITEDFLTLG_VALUE"])
                    {
                        $lang = $lng;
                        break;
                    }
                } ?>
                <div class="sm-bargain-detail-header">
                    <h1 class="sm-bargain-detail-title">
                        <?= (isset($lang["name"])&&$lang["name"]!="")?$lang["name"]:$product["productname"]; ?>
                    </h1>
                    <div class="line sm-second-line clearfix">
                        <div class="row">
                            <div class="col s2 no-padding">
                                <?php if(isset($product->merchant["merchantlogo"])): ?>
                                    <?= $this->cell('System.MediaRenderer',['mediaid'=>$product->merchant["merchantlogo"]]) ?>
                                <?php endif; ?>
                            </div>
                            <div class="col s10 right-align sm-bargain-retail-price">
                                <span class="extra-label"><?= __d("smashdeal","Retail Price :"); ?></span><br/>
                                <?= $this->Number->currency($product->retailprice); ?>
                            </div>
                        </div>
                    </div>
                    <div class="line sm-third-line clearfix">
                        <div class="col s4 no-padding sm-prd-token-amount">
                            <?= $this->Number->format($product->bargaintoken); ?><br/>
                            <?= ($product->bargaintoken>=2)?__d("smashdeal","Tokens"):__d("smashdeal","Token"); ?>
                        </div>
                        <div class="col s8">
                            <div class="sm-bargain-detail-price-range">
                                <?=
                                     $this->Number->currency($product->bargainstart) ?>-<?= $this->Number->currency($product->bargainend); ?>
                                <br/>
                                <?= __d("smashdeal","Multiply : "); ?>
                                <?= $this->Number->currency($product->incrementvalue) ?></div>
                        </div>
                    </div>
                    <div class="line sm-forth-line clearfix" style="padding-top: 15px;">
                        <?= $this->cell('Smashdeal.InsertBargain::mobile',['bargainid'=>$product->id]); ?>
                    </div>
                    <div class="line sm-fifth-line clearfix">
                        <?= $this->cell('Smashdeal.Remaintime::bar',['bargainquota'=>$product->bargainquota,'bargaintime'=>$product->bargaintimes]) ?> 
                    </div>
                    <?php
                        if($product->bargainquota > $product->bargaintimes):
                    ?>
                        <div class="line sm-sixth-line clearfix center-align" style="padding-top:15px;">
                            <?= $this->Html->link(__d("smashdeal","Direct Purchase"),['controller'=>'Purchases','action'=>'direct','plugin'=>'Smashdeal','prefix'=>'Frontend',$product->slug],['class'=>'waves-effect waves-light btn btn-sm-primary']); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row sm-prd-detail-row">
            <div class="col s12 no-padding">
                <ul class="sm-tabs tabs" role="tablist">
                     <li class="tab col s6">
                        <a href="#bargainstatus" class="active" aria-controls="profile" role="tab" data-toggle="tab"><?= __d("smashdeal","Bargain Status") ?></a>
                    </li>
                    <li class="tab col s6">
                        <a href="#top25" class="active" aria-controls="profile" role="tab" data-toggle="tab"><?= __d("smashdeal","Top 25") ?></a>
                    </li>
                    <li class="tab col s6">
                        <a href="#bargainspec" class="active" aria-controls="profile" role="tab" data-toggle="tab"><?= __d("smashdeal","Bargain Conditions") ?></a>
                    </li>
                    <li class="tab col s6">
                        <a href="#prospec" aria-controls="home" role="tab" data-toggle="tab"><?= __d("smashdeal","Product Specification") ?></a>
                    </li>
                </ul>
            </div>
            <div class="tab-pane clearfix active" id="bargainstatus">
                <div class="info-wrap">
                    <span class="info">
                        <?= __d("smashdeal","Note : SH = Single Host, MH = Multi Host"); ?>
                    </span>
                </div>
                <div class="bargain-overflow">
                    <?php foreach($mybargains as $key=>$bargain): ?>
                        <div class="bargain-status-box <?= $this->cell('Smashdeal.BargainStatus::bargainClass',['bargain'=>$bargain]) ?>">
                            <div class="row no-margin">
                                <div class="col s10">
                                    <div class="price"><?= $this->Number->currency($bargain->price); ?></div>
                                    <div class="time"><?= $bargain->bargaintime->i18nFormat("dd/MM/yyyy HH:mm:ss","Asia/Jakarta") ?></div>
                                    <div class="bargain-status">
                                        <?php $label = $this->cell('Smashdeal.BargainStatus',['bargain'=>$bargain]); ?>
                                        <?php echo $label; ?>
                                    </div>
                                </div>
                                <div class="col s2">
                                    <?php if($label == "Single Host Bargain"): ?>
                                        <span class="rank">
                                            <?= $this->cell('Smashdeal.SingleHostStatus',['bargain'=>$bargain]) ?>
                                        </span>
                                    <?php else: ?>
                                        <?= $this->cell('Smashdeal.MultipleHostStatus',['bargain'=>$bargain]) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="overflow-table-container" id="top25">
                <div class="overflow-table-container">
                    <table class="striped centered">
                        <thead>
                            <tr>
                                <th width="3%" class="text-center"><?= __d("smashdeal","No. ") ?></th>
                                <th width="6%" class="text-center"><?= __d("smashdeal","Username") ?></th>
                                <th class="text-center"><?= __d("smashdeal","Rank") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($ranks  as $key=>$rank): ?>
                                <tr>
                                    <td><?= $key+1 ?></td>
                                    <td><?= $rank['username'] ?></td>
                                    <td class="text-center"><?= $this->Number->ordinal(($key+1),['locale'=>'en_EN']); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col s12 prospec" id="prospec">
                <?= (isset($lang["description"])&&$lang["description"]!="")?$lang["description"]:$product["description"]; ?>
            </div>
            <div class="col s12" id="bargainspec">
                <ul class="browser-default bargainspec">
                    <li><?= __d("smashdeal","Bargain price range allowed between {0} - {1}",$this->Number->currency($product->bargainstart),$this->Number->currency($product->bargainend)); ?></li>
                    <li><?= __d("smashdeal","Price must multiplication of {0}",$this->Number->currency($product->incrementvalue)); ?></li>
                    <li>Perolehan CASHTOKEN per 1 Guest adalah <?= $this->Number->currency(($config_front['SINGLETOKENPRICE_VALUE']*$product->bargaintoken)*($config_front['MULTIPLEHOSTPRC_VALUE']/100)); ?></li>
                    <li>Setelah Slot BARGAIN QUOTA terpenuhi FULL, maka Pemenang Bargain pada Produk ini adalah Peserta Dealsmash yang menjabat Status SINGLE HOST Juara 1st.</li>
                </ul>
            </div>
        </div>
        <div class="col s12 sm-bargain-detail-footer">
            <div class="merchant-info-wrapper clearfix">
                <h3 class="sm-prd-detail-sub-title"><?= __d("smashdeal", "Merchant Info") ?></h3>
                <div class="merchant-info col s12">
                    <p class="pull-left merchant-detail">
                        <strong><?= $product->merchant["name"] ?></strong><br/>
                        <?= $product->merchant["address"]; ?>
                        <br/>
                        <?= $product->merchant["phonenoone"] ?><br>
                        <?= $product->merchant["phonenotwo"] ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('ul.tabs').tabs();
        });
    </script>
<?php endif; ?>