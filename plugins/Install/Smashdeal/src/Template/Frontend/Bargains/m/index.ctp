<div class="container sm-main-content">
    <h1 class="section-header"><?= __d("smashdeal", "My Bargain") ?></h1>
    <?php foreach($mybargains as $product): ?>
       <?= $this->Html->link($this->cell('Smashdeal.SingleProductLong',['productid'=>$product->id,'config_front'=>$config_front]),['controller'=>'Bargains','action'=>'view','prefix'=>'Frontend','plugin'=>'Smashdeal',$product->slug],['escape'=>false]); ?>
    <?php endforeach; ?>
    <?= $this->element('System.paginator') ?>
</div>