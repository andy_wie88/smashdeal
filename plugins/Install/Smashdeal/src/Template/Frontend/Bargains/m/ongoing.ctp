<div class="container sm-main-content">
	<h1 class="section-header"><?= __d("smashdeal", "On Going Bargain") ?></h1>
	<div class="row scroll">
		<?php if(count($mybargains)>0): ?>
		    <?php foreach($mybargains as $product): ?>
		        <?= $this->Html->link($this->cell('Smashdeal.SingleProductLong::mobile',['productid'=>$product->id,'config_front'=>$config_front]),['controller'=>'Bargains','action'=>'view','prefix'=>'Frontend','plugin'=>'Smashdeal',$product->slug],['escape'=>false]); ?>
		    <?php endforeach; ?>
	        <?php if(count($mybargains)>0): ?>
		        <div class="hide paginator">
		            <?= $this->element('System.paginator') ?>
		        </div>
		    <?php endif; ?>
		<?php else: ?>
		    <div class="col-md-12 no-product">
		        <?= __d("smashdeal","Anda tidak mempunyai bargain yang sedang berlangsung"); ?>
		    </div>
		<?php endif; ?>
	</div>
	<?php if(count($mybargains)>0): ?>
        <div class="center-align" id="loader" style="margin-bottom: 20px;display: none">
            <div class="preloader-wrapper active">
                <div class="spinner-layer sm-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
	$(document).ready(function(e)
	{
	  $(window).scroll(function() {
	     if($(window).scrollTop()>150) {
	        if($("a.next").length>0){
	          console.log("here 2");
	          var href = $("a.next").attr("href");
	          $(".paginator").remove();
	          $("#loader").show();
	          $.get(href,function(data)
	          {
	            $(".scroll").append(data).fadeIn("slow");
	            $("#loader").hide();
	          });
	        }
	     }
	  });
	});
</script>