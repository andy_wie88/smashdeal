<?= $this->extend('../Users/template'); ?>
<?= $this->start('profile') ?>
<div class="sm-box clearfix">
	<h1 class="section-header"><?= __d("smashdeal", "Produk Belum diklaim") ?></h1>
	<h3 class="section-header">
		<?= __d("smashdeal","Berikut ini adalah hadiah bargain anda yang belum diklaim ke pihak merchant. : ") ?>
	</h3>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="20%"><?= __d("smashdeal","Kode Voucher") ?></th>
				<th width="20%"><?= __d("smashdeal","Tanggal Exp.") ?></th>
				<th width="20%"><?= __d("smashdeal","Product") ?></th>
				<th width="20%"><?= __d("smashdeal","Merchant") ?></th>
				<th width="20%"><?= __d("smashdeal","Sisa Pembayaran") ?></th>
				<th width="5%"><?= __d("smashdeal","") ?></th>
			</tr>
		</thead>
		<?php if(count($unclaims)>0): ?>
			<tbody>
				<?php foreach($unclaims as $key=>$unclaim): ?>
					<tr>
						<td><?= $unclaim['vouchercode'] ?></td>
						<td><?= $unclaim['validity'] ?></td>
						<td><?= $unclaim['product']['productname'] ?></td>
						<td><?= $unclaim['merchant']['name'] ?></td>
						<td class="text-right"><?= $this->Number->currency($unclaim['price']) ?></td>
						<td class="text-center">
							<?= $this->Html->link(__d("smashdeal","view"),['controller'=>'Bargains','action'=>'bargainVoucher','plugin'=>'Smashdeal','prefix'=>'Frontend',$unclaim['id']]) ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		<?php else: ?>
			<tbody>
				<tr>
					<td colspan="5"><?= __d("smashdeal","Tidak ada produk yang belum diklaim.") ?></td>
				</tr>
			</tbody>
		<?php endif; ?>
	</table>
	<?= $this->element('System.paginator') ?>
</div>
<?= $this->end(); ?>