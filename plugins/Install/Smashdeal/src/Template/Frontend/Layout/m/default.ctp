<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php if(!isset($title)): ?>
                <?= $config_front["SITETITLE"] ?> | <?= $config_front["SITETAGLN"] ?>
            <?php else: ?>
                <?= $config_front["SITETITLE"]; ?> | <?= $title ?>
            <?php endif; ?>
        </title>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans|Titillium+Web:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <?php
        	echo $this->Html->css('materialize.min');
        	echo $this->Html->css('Smashdeal.smashdeal.m');
        	echo $this->AssetCompress->css('Smashdeal.sm-mobile');
        	echo $this->Html->script('essential.min');
        	echo $this->Html->script('materialize.min');
	        echo $this->fetch('css');
        ?>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    </head>
    <body>
		<nav>
		    <div class="nav-wrapper sm-nav-wrapper">
	  			<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
	  			<h1 class="hide"><?= $config_front["SITETITLE"] ?></h1>
	  			<?= $this->Html->link($this->Html->image('assets_logo_mobile.png'),['controller'=>'Products','action'=>'homepage','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'brand-logo center']) ?>
	      		<?php if(!isset($this->request->session()->read('DealAuth')["User"])): ?>
	      			<ul id="slide-out" class="side-nav">
	      				<li>
					    	<div class="userView">
					      		<div class="background">
						      	</div>
						      	<div style="height: 40px;"></div>
					    	</div>
				    	</li>
					   	<li>
		      	 			<?= $this->Html->link("<i class='fa fa-home'></i>".__d("smashdeal","Beranda"),['controller'=>'Products','action'=>'homepage','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']); ?>
		      	 		</li>
					    <li><div class="divider"></div></li>
				        <li>
				      		<?= $this->Html->link("<i class='fa fa-sign-in'></i>".__d("smashdeal","Login"),['controller'=>'Users','action'=>'login','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false,'class'=>'collapsible-header']); ?>
				      	</li>
					    <li><div class="divider"></div></li>
				      	<li>
				      		<?= $this->Html->link("<i class='fa fa-user-plus'></i>".__d("smashdeal","Register"),['controller'=>'Users','action'=>'register','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false,'class'=>'collapsible-header']); ?>
				      	</li>
					    <li><div class="divider"></div></li>
			      		<li>
			      			<?= $this->Html->link("<i class='fa fa-ticket'></i>"."Redeem Voucher",['controller'=>'Coupons','action'=>'redeemVoucher','plugin'=>'smashdeal','prefix'=>'Frontend'],['escape'=>false,'class'=>'collapsible-header']) ?>
		      			</li>
					    <li><div class="divider"></div></li>
	        			<li>
	        				<?= $this->Html->link("<i class='fa fa-ticket'></i>".__d("smashdeal","Buy Token Online!"),['controller'=>'Vouchers','action'=>'index','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']) ?>
	        			</li>
				  	</ul>
	      		<?php else: ?>
		      		<ul id="slide-out" class="side-nav">
					    <li>
					    	<div class="userView">
					      		<div class="background">
						      	</div>
						      	<a><?= $this->Html->image('user-white.svg',['class'=>'user-img']) ?></a>
						      	<a><span class="white-text name"><?= $this->request->session()->read('DealAuth')['User']['name'] ?></span></a>
						      	<?= $this->Html->link("<span class='white-text name'> Bargain Token : ".$this->cell('Smashdeal.BTValue::mobile')."</span>",['controller'=>'Users','action'=>'bargaintoken','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]) ?>
						      	<?= $this->Html->link("<span class='white-text name'> Cash Token : ".$this->cell('Smashdeal.CTValue::mobile')."</span>",['controller'=>'Users','action'=>'cashtoken','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]) ?>
						      	<div style="padding-top:10px;"></div>
					    	</div>
				    	</li>
				    	<li class="no-padding">
		      	 			<?= $this->Html->link("<i class='fa fa-home'></i>".__d("smashdeal","Beranda"),['controller'=>'Products','action'=>'homepage','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']); ?>
		      	 		</li>
		      	 		<li>
			      			<?= $this->Html->link("<i class='fa fa-ticket'></i>"."Redeem Voucher",['controller'=>'Coupons','action'=>'redeemVoucher','plugin'=>'smashdeal','prefix'=>'Frontend'],['escape'=>false,'class'=>'collapsible-header']) ?>
		      			</li>
					    <li><div class="divider"></div></li>
	        			<li>
	        				<?= $this->Html->link("<i class='fa fa-ticket'></i>".__d("smashdeal","Buy Token Online!"),['controller'=>'Vouchers','action'=>'index','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']) ?>
	        			</li>
					    <li><div class="divider"></div></li>
					    <li class="no-padding">
					        <ul class="collapsible collapsible-accordion">
					          <li>
					            <a class="collapsible-header"><i class="fa fa-user"></i>My Profile</a>
					            <div class="collapsible-body">
					              <ul>
					                <li><?= $this->Html->link(__d("smashdeal","Edit Profile"),['controller'=>'Users','action'=>'profile','plugin'=>'Smashdeal','prefix'=>'Frontend']); ?></li>
					                <li><?= $this->Html->link(__d("smashdeal","Change Password"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend']); ?></li>
					              </ul>
					            </div>
					          </li>
					        </ul>
				      	</li>
					    <li><div class="divider"></div></li>
				      	<li class="no-padding">
					        <ul class="collapsible collapsible-accordion">
					          <li>
					            <a class="collapsible-header"><i class="fa fa-handshake-o"></i>My Bargain</a>
					            <div class="collapsible-body">
					              <ul>
					                <li><?= $this->Html->link(__d("smashdeal","On Going"),['controller'=>'Bargains','action'=>'ongoing','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
					                <li><?= $this->Html->link(__d("smashdeal","Status Klaim"),['controller'=>'Bargains','action'=>'claimstatus','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
					                <li><?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Bargains','action'=>'history','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
					              </ul>
					            </div>
					          </li>
					        </ul>
				      	</li>
					    <li><div class="divider"></div></li>
				      	<li class="no-padding">
					        <ul class="collapsible collapsible-accordion">
					          <li>
					            <a class="collapsible-header"><i class="fa fa-shopping-cart"></i>My Purchase</a>
					            <div class="collapsible-body">
					              <ul>
					                <li><?= $this->Html->link(__d("smashdeal","Status Klaim"),['controller'=>'Purchases','action'=>'claimstatus','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
					                <li><?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Purchases','action'=>'history','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
					            </div>
					          </li>
					        </ul>
				      	</li>
					    <li><div class="divider"></div></li>
				      	<li class="no-padding">
					        <ul class="collapsible collapsible-accordion">
					          <li>
					            <a class="collapsible-header"><i class="fa fa-shopping-cart"></i></i>BT Purchase</a>
					            <div class="collapsible-body">
					              <ul>
					                <li><?= $this->Html->link(__d("smashdeal","Konfirmasi Pembayaran"),['controller'=>'Vouchers','action'=>'unconfirm','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
					                <li><?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Vouchers','action'=>'history','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
					            </div>
					          </li>
					        </ul>
				      	</li>
					    <li><div class="divider"></div></li>
					    <li class="no-padding">
		      	 			<?= $this->Html->link("<i class='fa fa-sign-out'></i>".__d("smashdeal", "Logout"),['controller'=>'Users','action'=>'logout','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']) ?>
		      	 		</li>
				  	</ul>
			  	<?php endif; ?>
		    </div>
		    <div class="nav-wrapper search-nav-wrapper">
	      		<?php if(isset($q)): ?>
                    <?= $this->cell('Smashdeal.ProductSearch::mobile',['q'=>$q]); ?>
                <?php else: ?>
                    <?= $this->cell('Smashdeal.ProductSearch::mobile'); ?>
                <?php endif; ?>
	    	</div>
		</nav>
  		<section class="body">
            <?= $this->Flash->render('auth'); ?>
            <?= $this->Flash->render(); ?>
  			<?= $this->fetch('content'); ?>
  		</section>
  		<footer class="clearfix">
            <div class="row">
                <div class="col s12 center-align">
                    <h4 class="footer-link-header"><?= __d("smashdeal","Help") ?></h4>
                    <?= $this->cell('System.MenuRenderer',['locator'=>'SMFOOTERMENU','ulclass'=>'footer-link']) ?>
                </div>
                <div class="col s12 center-align">
                    <h4 class="footer-link-header"><?= __d("smashdeal","Follow Us At") ?></h4>
                    <ul class="social-link-list">
                        <li>
                            <?= $this->Html->link("<i class='sm sm-instagram'></i>",'https://www.instagram.com/dealsmash.id/',['escape'=>false,'class'=>'instagram-icon','target'=>'_blank']) ?>
                        </li>
                        <li>
                            <?= $this->Html->link("<i class='sm sm-facebook'></i>","https://www.facebook.com/dealsmash.id/",['escape'=>false,'class'=>'facebook-icon','target'=>"_blank"]) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    </body>
    <!--Start of Tawk.to Script-->
	<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/596ec86b6edc1c10b03469eb/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
	</script>
<!--End of Tawk.to Script-->
    <?php
        echo $this->fetch('footer-script');
    ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-101653729-1', 'auto');
      ga('send', 'pageview');

    </script>
    <script type="text/javascript">
    	$(".button-collapse").sideNav();
    	$(".close").click(function()
    	{
    		$(this).parent().fadeOut();
    	})
    </script>
</html>