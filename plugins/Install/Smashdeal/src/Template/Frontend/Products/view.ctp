<?= $this->AssetCompress->script('Smashdeal.viewproduct',['block'=>'footer-script']); ?>
<?php if($pass == false && $product->visibility == 2): ?>
    <div class="container  sm-main-content ">
        <div class="col-md-4 col-md-push-4 sm-prd-detail clearfix">
            <?= $this->Form->create($product,['class'=>'form-horizontal']) ?>
            <div class="col-md-7 col-sm-7 col-xs-7 no-padding-right">
                <?= $this->Form->input('password',['placeholder'=>'Product Password','class'=>'form-control','label'=>false]) ?>
            </div>
            <div class="col-md-5  col-sm-5 col-xs-5 no-padding-left">
                <button class="btn btn-smd btn-primary">
                    <?= __d("smashdeal","Access Product"); ?>
                </button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
<?php else: ?>
    <div class="container sm-main-content sm-prd-detail">
        <div class="row sm-prd-detail-row">
            <div class="col-md-8 no-padding">
                <div class="ribbon-two">
                    Hemat <?= round((($product['retailprice']-$product['bargainstart'])/$product['retailprice']) * 100) ?>%
                </div>
                <div id="carousel-example-generic" class="carousel slide sm-prd-slider" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php $first = true; foreach($product->medias as $key=>$media): ?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?= $key ?>" class="<?php if($first){echo 'active';$first=false;} ?>"></li>
                        <?php endforeach; ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php $first = true; foreach($product->medias as $media): ?>
                            <div class="item <?php if($first){ echo 'active'; $first=false; }?>">
                                <?= $this->cell('System.MediaRenderer',['mediaId'=>$media["mediaid"]]); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only"><?= __d("smashdeal","Previous") ?></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only"><?= __d("smashdeal","Nex") ?></span>
                    </a>
                </div>
            </div>
            <div class="col-md-4 clearfix">
                <?php $lang = []; ?>
                <?php foreach($product->langs as $lng){
                    if($lng["languageid"]==$config_front["SITEDFLTLG_VALUE"])
                    {
                        $lang = $lng;
                        break;
                    }
                } ?>
                <div class="sm-bargain-detail-header">
                    <h1 class="sm-bargain-detail-title">
                        <?= (isset($lang["name"])&&$lang["name"]!="")?$lang["name"]:$product["productname"]; ?>
                    </h1>
                    <div class="line sm-second-line clearfix">
                        <div class="col-md-2 col-sm-2 col-xs-2 no-padding">
                            <?php if(isset($product->merchant["merchantlogo"])): ?>
                                <?= $this->cell('System.MediaRenderer',['mediaid'=>$product->merchant["merchantlogo"]]) ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-10 text-right sm-bargain-retail-price">

                           
                            <span class="extra-label"><?= __d("smashdeal","Retail Price :"); ?></span><br/>
                            <?= $this->Number->currency($product->retailprice); ?>
                        </div>
                    </div>
                    <div class="line sm-third-line clearfix">
                        <div class="col-md-3 col-sm-3 col-xs-3 no-padding sm-prd-token-amount">
                            <?= $this->Number->format($product->bargaintoken); ?><br/>
                            <?= ($product->bargaintoken>=2)?__d("smashdeal","Tokens"):__d("smashdeal","Token"); ?>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <div class="sm-bargain-detail-price-range">
                                <small><strong><?= __d("smashdeal","Limit Nilai Bargain :") ?></strong></small><br>
                                <?=
                                     $this->Number->currency($product->bargainstart) ?>-<?= $this->Number->currency($product->bargainend); ?>
                                <br/>
                                <strong><small><?= __d("smashdeal","Multiply : "); ?></small></strong><br>
                                <?= $this->Number->currency($product->incrementvalue) ?></div>
                        </div>
                    </div>
                    <div class="line sm-forth-line clearfix">
                        <?= $this->cell('Smashdeal.InsertBargain',['bargainid'=>$product->id]); ?>
                    </div>
                    <div class="line sm-fifth-line clearfix">
                        <?= $this->cell('Smashdeal.Remaintime::bar',['bargainquota'=>$product->bargainquota,'bargaintime'=>$product->bargaintimes]) ?> 
                    </div>
                    <?php
                        if($product->bargainquota>$product->bargaintimes):
                    ?>
                        <div class="line sm-sixth-line clearfix">
                            <?= $this->Html->link(__d("smashdeal","Direct Purchase"),['controller'=>'Purchases','action'=>'direct','plugin'=>'Smashdeal','prefix'=>'Frontend',$product->slug],['class'=>'btn btn-smd btn-sm-primary']); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row sm-prd-detail-row">
            <div class="col-md-8 no-padding">
                <div>
                    <ul class="nav nav-tabs sm-nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#bargainspec" aria-controls="profile" role="tab" data-toggle="tab"><?= __d("smashdeal","Bargain Conditions") ?></a></li>
                        <li role="presentation"><a href="#top25" aria-controls="bargain-detail" role="tab" data-toggle="tab"><?= __d("smashdeal", "Top 25") ?></a></li>
                        <li role="presentation"><a href="#prospec" aria-controls="home" role="tab" data-toggle="tab"><?= __d("smashdeal","Product Specification") ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="bargainspec">
                            <ul>
                                <li><?= __d("smashdeal","Bargain price range allowed between {0} - {1}",$this->Number->currency($product->bargainstart),$this->Number->currency($product->bargainend)); ?></li>
                                <li><?= __d("smashdeal","Price must multiplication of {0}",$this->Number->currency($product->incrementvalue)); ?></li>
                                <li>Perolehan CASHTOKEN per 1 Guest adalah <?= $this->Number->currency(($config_front['SINGLETOKENPRICE_VALUE']*$product->bargaintoken)*($config_front['MULTIPLEHOSTPRC_VALUE']/100)); ?></li>
                                <li>Setelah Slot BARGAIN QUOTA terpenuhi FULL, maka Pemenang Bargain pada Produk ini adalah Peserta Dealsmash yang menjabat Status SINGLE HOST Juara 1st.</li>
                            </ul>
                        </div>
                        <div class="tab-pane clearfix" id="top25" role="tabpanel">
                            <div class="overflow-table-container">
                                <table class="table table-sm table-bordered table-stripe">
                                    <thead>
                                        <tr>
                                            <th width="3%" class="text-center"><?= __d("smashdeal","No. ") ?></th>
                                            <th class="text-center"><?= __d("smashdeal","Username") ?></th>
                                            <th width="30%" class="text-center"><?= __d("smashdeal","Rentang Harga") ?></th>
                                            <th class="text-center"><?= __d("smashdeal","Rank") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($ranks  as $key=>$rank): ?>
                                            <tr>
                                                <td><?= $key+1 ?></td>
                                                <td><?= $rank['username'] ?></td>
                                                <td><?= $this->cell('Smashdeal.RangePrice',[$rank['bargainstart'],$rank['bargainend'],$rank['incrementvalue'],$rank['price']]) ?></td>
                                                <td class="text-center"><?= $this->Number->ordinal(($key+1),['locale'=>'en_EN']); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="prospec">
                            <?= (isset($lang["description"])&&$lang["description"]!="")?$lang["description"]:$product["description"]; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sm-bargain-detail-footer">
                <div class="merchant-info-wrapper clearfix">
                    <h3 class="sm-prd-detail-sub-title"><?= __d("smashdeal", "Merchant Info") ?></h3>
                    <div class="merchant-info">
                        <p class="pull-left merchant-icon hidden-xs hidden-sm"><i class="fa fa-map-marker"></i></p>
                        <p class="pull-left merchant-detail">
                            <strong><?= $product->merchant["name"] ?></strong><br/>
                            <?= $product->merchant["address"]; ?>
                            <br/>
                            <?= $product->merchant["phonenoone"] ?><br>
                            <?= $product->merchant["phonenotwo"] ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>