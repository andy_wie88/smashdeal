<?= $this->Html->script('Smashdeal.products/m/homepage',['block'=>'footer-script']) ?>
<?= $this->cell('Smashdeal.HomepageSlideShow::mobile') ?>
<div class="container">
    <h2 class="section-header"><?= __d("smashdeal","All Products") ?></h2>
    <div class="row scroll">
        <?php if(count($products)>0): ?>
            <?php  foreach($products as $product): ?>
                <div class="col-xs-12 product-wrapper">
                    <?= $this->Html->link($this->element('Smashdeal.Products/m/singleproductmd',['product'=>$product]),['controller'=>'Products','action'=>'view','plugin'=>'Smashdeal','prefix'=>'Frontend',$product->slug],['escape'=>false]); ?>
                </div>
            <?php endforeach; ?>
            <div class="hide paginator">
                <?= $this->element('System.paginator') ?>
            </div>
        <?php else: ?>
            <div class="col s12 no-product">
                <?= __d("smashdeal","We currently has no product to display"); ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if(count($products)>0): ?>
        <div class="center-align" id="loader" style="margin-bottom: 20px;display: none">
            <div class="preloader-wrapper active">
                <div class="spinner-layer sm-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="container">
    <h2 class="section-header">
        <?= __d("smashdeal","Semua Kategori") ?>
    </h2>
    <div class="row s12 category-list-wrapper">
        <ul class="category-list">
            <?php foreach($categories as $category): ?>
                <li>
                    <?= $this->Html->link($this->cell('System.LangRenderer',['source'=>$category,'sourcetype'=>'TERM','field'=>'name']),['controller'=>'Products','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Frontend',$category['slug']]) ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>