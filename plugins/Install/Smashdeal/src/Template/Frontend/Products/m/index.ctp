<?= $this->Html->script('Smashdeal.products/m/homepage',['block'=>'footer-script']) ?>
<?php if(isset($categories)): ?>
    <?php if(count($categories->toArray())>0): ?>
        <div>
            <ul class="category-nav">
                <?php foreach($categories as $cat): ?>
                    <li>
                        <?= $this->Html->link($this->cell('System.LangRenderer',['source'=>$cat,'sourcetype'=>'TERM','field'=>'name']),['controller'=>'Products','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Frontend',$cat['slug']]) ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
<?php endif; ?>
<div class="container">
    <h2 class="section-header center-align">
        <?php if(isset($q)): ?>
            <?= __d("smashdeal","Search Product With Keyword ''{0}''",$q); ?>
        <?php elseif(isset($category)): ?>
            <?php foreach($category->langs as $lng){if($lng["languageid"]==$config_front["SITEDFLTLG_VALUE"]){$lang = $lng;break;}} ?>
            <?= __d("smashdeal","{0}",(isset($lang) && $lang["name"]!="")?$lang["name"]:$category["name"]); ?>
        <?php else: ?>
            <?= __d("smashdeal","All Products") ?>
        <?php endif; ?>
    </h2>
    <div class="row scroll">
        <?php if(count($products)>0): ?>
            <?php  foreach($products as $product): ?>
                <div class="col-xs-12 product-wrapper">
                    <?= $this->Html->link($this->element('Smashdeal.Products/m/singleproductmd',['product'=>$product]),['controller'=>'Products','action'=>'view','plugin'=>'Smashdeal','prefix'=>'Frontend',$product->slug],['escape'=>false]); ?>
                </div>
            <?php endforeach; ?>
            <div class="hide paginator">
                <?= $this->element('System.paginator') ?>
            </div>
        <?php else: ?>
            <div class="col s12 no-product">
                <?php if(isset($q)): ?>
                    <?= __d("smashdeal","We currently has no product to display with keyword ''{0}''",$q); ?>
                <?php elseif(isset($category)): ?>
                    <?php foreach($category->langs as $lng){if($lang["languageid"]==$config_front["SITEDFLTLG_VALUE"]){$lang = $lng;break;}} ?>
                    <?= __d("smashdeal","No Product Found at ''{0}'' Category",(isset($lang) && $lang["name"]!="")?$lang["name"]:$category["name"]); ?>
                <?php else: ?>
                    <?= __d("smashdeal","We currently has no product to display"); ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if(count($products)>0): ?>
        <div class="center-align" id="loader" style="margin-bottom: 20px;display: none">
            <div class="preloader-wrapper active">
                <div class="spinner-layer sm-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>