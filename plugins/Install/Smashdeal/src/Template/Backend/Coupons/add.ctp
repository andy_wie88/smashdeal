<?= $this->AssetCompress->script('Smashdeal.Coupon23432',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('Smashdeal.Coupon23432',['block'=>'css']) ?>
<?php if(!isset($tobeprints)): ?>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal", "Generate Coupon") ?></h2>
    </div>
    <div class="box-body">
        <?= $this->Form->create($coupon,['class'=>'form-horizontal']) ?>
        <div class="form-group">
            <label for="TokenProduct" class="col-md-2 control-label">
                <?= __d("smashdeal","Token Amount") ?>
            </label>
            <div class="col-md-2">
                <?= $this->Form->input('tokenproductid',['label'=>false,'options'=>$tokenproducts,'class'=>'form-control']) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="TokenProduct" class="col-md-2 control-label">
                <?= __d("smashdeal","Token Amount") ?>
            </label>
            <div class="col-md-1">
                <?= $this->Form->input('amount',['label'=>false,'type'=>'number','min'=>1,'value'=>1,'class'=>'form-control']) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="TokenProduct" class="col-md-2 control-label">
                <?= __d("smashdeal","Exp. Date") ?>
            </label>
            <div class="col-md-2">
                <?= $this->Form->input('expdate',['label'=>false,'type'=>'text','class'=>'form-control date']) ?>
            </div>
        </div>
        <div class="col-md-2 col-md-push-2">
            <button type="submit" class="btn btn-primary"><?= __d("smashdeal", "Generate") ?></button>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>
<?php else: ?>
    <div class="box box-default">
        <div class="box-header">
            <h2 class="box-title"><?= __d("smashdeal", "List Of Generated Coupon") ?></h2>
        </div>
        <div class="box-body no-padding">
            <table class="table table-border">
                <thead>
                <tr>
                    <th><?= __d("smashdeal","Coupon Code") ?></th>
                    <th><?= __d("smashdeal","Exp. Date") ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($tobeprints as $tobeprint): ?>
                    <tr>
                        <td class="courier"><?= $tobeprint["couponcode"] ?></td>
                        <td><?= $tobeprint["expdate"]->i18nFormat($config["SITEDTFORM"]) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>
