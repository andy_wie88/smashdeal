<?= $this->AssetCompress->script('Smashdeal.Prod234324',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('Smashdeal.Prod234324',['block'=>'css']) ?>
<div class="box box-default">
    <div class="box-header">
        <h3 class="box-title">
            <?= __d("smashdeal","List Of Token Products") ?>
        </h3>
    </div>
    <div class="box-body no-padding">
        <?= $this->Form->create($token,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
        <div class="list-action clearfix">
            <div class="col-md-3 no-padding">
                <select id="bulk-action" name="action" class="form-control i-combo">
                    <option value=""><?= __d('system',"Bulk Action") ?></option>
                    <option value="1"><?= __d('system',"Remove") ?></option>
                </select>
            </div>
            <div class="col-md-2 no-padding">
                <button class="btn btn-default" type="submit" id="btn-bulk-action">
                    <?= __d('system',"Apply") ?>
                </button>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="cb-wrap" width="5%"><input type="checkbox" class="checkbox-custom" id="checkall" /></th>
                <th><?= __d("smashdeal","Token Amount") ?></th>
                <th><?= __d("smashdeal","Price") ?></th>
                <th width="100px" class="text-center"><?= __d("smashdeal","Discount") ?></th>
                <th width="150px" class="text-center"><?= __d("smashdeal","Valid Coupon") ?></th>
                <th width="100px"></th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($tokens)>0): ?>
                <?php foreach($tokens as $t): ?>
                    <tr>
                        <td class="cb-wrap">
                            <input type="checkbox" class="checkbox-custom checkbox-child" name="value[]" value="<?= $t["id"] ?>"/>
                        </td>
                        <td>
                            <?= $t["tokenamount"] ?>
                            <?= ($t["tokenamount"]>1)?__d("smashdeal"," Tokens"):__d("smashdeal","Token") ?>
                        </td>
                        <td>
                            <?= $this->Number->currency($t["price"]); ?>
                        </td>
                        <td class="text-center">
                            <?= $t["discount"]."%"; ?>
                        </td>
                        <th class="text-center">
                            <?= (isset($t["valid_coupons"]))?count($t["valid_coupons"]):0; ?>
                        </th>
                        <td style="text-align:center">
                            <div class="btn-group btn-group-sm">
                                <?= $this->Html->link("<i class='fa fa-edit'></i>",['controller'=>'Tokens','action'=>'edit','plugin'=>'Smashdeal','prefix'=>'Backend',$t["id"]],['class'=>'btn btn-default btn-sm','escape'=>false]) ?>
                                <?= $this->Html->link("<i class='fa fa-trash'></i>",['controller'=>'Tokens','action'=>'remove','plugin'=>'Smashdeal','prefix'=>'Backend',$t["id"]],['class'=>'btn btn-danger btn-sm','escape'=>false]); ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="4">
                        <?php if(isset($s)): ?>
                            <?= __d("smashdeal","No Token Product with keyword ''{0}'' found",$s); ?>
                        <?php else: ?>
                            <?= __d("smashdeal","No Token Product Available") ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <?= $this->element('System.paginator'); ?>
    </div>
</div>