<?= $this->Form->create($category,['class'=>'form-horizontal']); ?>
    <div class="box box-default">
        <div class="box-header">
            <h2 class="box-title"><?= __d("system", "Category") ?></h2>
        </div>
        <div class="box-body">
            <div class="form-group">
                <div class="col-md-12">
                    <?= $this->Form->input('name',['class'=>'form-control','label'=>__d("system","Name")]) ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->input('parentid',['class'=>'form-control','label'=>__d('system',"Parent"),'options'=>$categoriesoption,'empty'=>__d("system","-- Root --"),'escape'=>false,'type'=>'select']) ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->input('description',['class'=>'form-control','label'=>__d("system","Description")]) ?>
                </div>
            </div>
        </div>
    </div>
<?php $index = 0; ?>
<?php if(!empty($add_bottom_widgets)): ?>
    <?php foreach($add_bottom_widgets as $key=>$widget): ?>
        <?php $elementname =  (!empty($widget["pluginname"]))?$widget["pluginname"].".".$widget["component"]:$widget["component"]; ?>
        <?= $this->element($elementname,['index'=>$index,'bean'=>$category]); ?>
        <?php $index++; ?>
    <?php endforeach ;?>
<?php endif; ?>
    <button class="btn btn-primary" type="submit"><?= __d("system","save"); ?></button>
<?= $this->Form->end(); ?>