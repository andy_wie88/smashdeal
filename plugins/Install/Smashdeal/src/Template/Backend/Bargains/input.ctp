<?php Cake\I18n\I18n::locale($config["SITEDFLTLG"]); ?>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal","List Of Non-Profit Bargain") ?></h2>
    </div>
    <div class="box-body no-padding">
        <table class="table">
            <thead>
            <tr>
                <th><?= __d("smashdeal","No.") ?></th>
                <th><?= __d("smashdeal","Product Name") ?></th>
                <th><?= __d("smashdeal","Retail Price") ?></th>
                <th><?= __d("smashdeal","Money Owned") ?></th>
                <th><?= __d("smashdeal","Difference") ?></th>
                <th><?= __d("smashdeal","Bargain Quota") ?></th>
                <th><?= __d("smashdeal","Lower Bargain") ?></th>
                <th><?= __d("smashdeal","Bargain By") ?></th>
                <th><?= __d("smashdeal","Action") ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($bargains as $key=>$bargain): ?>
                <tr>
                    <td><?= $key+1 ?></td>
                    <td><?= $bargain["productname"] ?></td>
                    <td><?= $this->Number->currency($bargain["retailprice"]) ?></td>
                    <td><?= $this->Number->currency($bargain["moneyowned"]) ?></td>
                    <td><?= $this->Number->currency($bargain["difference"]) ?></td>
                    <td><?= $this->cell('Smashdeal.Remaintime',['bargainquota'=>$bargain->bargainquota,'bargaintimes'=>$bargain->bargaintimes]) ?></td>
                    <td><?= $this->Number->currency($bargain["lowerprice"]) ?></td>
                    <td><?= $bargain["username"] ?><?= ($bargain["isfake"])?"(FAKE)":""; ?></td>
                    <td>
                        <?= $this->Html->link('input deal',['controller'=>'Bargains','action'=>'input','plugin'=>'Smashdeal','prefix'=>'Backend',$bargain["id"]]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <?= $this->element('System.paginator'); ?>
    </div>
</div>