<?= $this->AssetCompress->css('System.Use54dds',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Use54dds',['block'=>'footer-script']) ?>
<?php
$allAmount = count($allmerchants->toArray());
$activeAmount = 0;
$inactiveAmount = 0;
foreach($allmerchants as $mdl){
    if($mdl["isactive"]==true)
        $activeAmount++;
    else
        $inactiveAmount++;
}
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
            <small>
                <strong>
                    <?php if($status==null): ?>
                        <?= "All (".$allAmount.")" ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link("All (".$allAmount.")",['controller'=>'Merchants','action'=>'search','plugin'=>'Smashdeal','prefix'=>'Backend',"s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link("All (".$allAmount.")",['controller'=>'Merchants','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Backend']); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    |
                    <?php if($status=="active"): ?>
                        <?= "Active (".$activeAmount.")" ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Merchants','action'=>'search','plugin'=>'Smashdeal','prefix'=>'Backend',"active","s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Merchants','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Backend',"active"]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    |
                    <?php if($status=="inactive"): ?>
                        <?= "Inactive (".$inactiveAmount.")" ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Merchants','action'=>'search','plugin'=>'Smashdeal','prefix'=>'Backend',"inactive","s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Merchants','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Backend',"inactive"]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </strong>
            </small>
        </h3>
        <div class="box-tools col-md-4 no-padding">
            <?= $this->Form->create($merchant,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
            <div class="input-group input-group-sm">
                <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <?= $this->Form->create($merchant,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
        <div class="list-action clearfix">
            <div class="col-md-2 no-padding">
                <select id="bulk-action" name="action" class="form-control i-combo">
                    <option value=""><?= __d('system',"Bulk Action") ?></option>
                    <option value="1"><?= __d('system',"Activate") ?></option>
                    <option value="2"><?= __d('system',"Non-Activate") ?></option>
                </select>
            </div>
            <div class="col-md-2 no-padding">
                <button class="btn btn-default" type="submit" id="btn-bulk-action">
                    <?= __d('system',"Apply") ?>
                </button>
            </div>
            <div class="pull-right item-amount">
                <?php
                $label = ($allAmount>=2)?" items":" item";
                echo $allAmount . $label;
                ?>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="cb-wrap" width="3%">
                    <input type="checkbox" class="checkbox-custom" id="checkall" />
                </th>
                <th width="15%"><?= __d('system',"Name") ?></th>
                <th width="30%"><?= __d('system',"Address") ?></th>
                <th width="15%"><?= __d('system',"Phone One") ?></th>
                <th width="15%"><?= __d('system',"Phone Two") ?></th>
                <th width="5%"><?= __d('system',"Status") ?></th>
                <th width="10%" class="text-center"><?= __d('system',"Action") ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($merchants)>0): ?>
                <?php foreach($merchants as $u): ?>
                    <tr>
                        <td class="cb-wrap">
                            <input type="checkbox" name="value[]" value="<?= $u["id"] ?>" class="cb-item checkbox-custom checkbox-child" data-value="<?= $u["id"] ?>"/>
                        </td>
                        <td><?= $u['name'] ?></td>
                        <td><?= $u['address'] ?></td>
                        <td><?= $u['phonenoone'] ?></td>
                        <td><?= $u['phonenoone'] ?></td>
                        <td style="text-align:center">
                            <input type="checkbox" class="cbreadonly" <?= ($u["isactive"])?"checked":"" ?> readonly/>
                        </td>
                        <td style="text-align:center">
                            <div class="btn-group btn-group-sm">
                                <?= $this->Html->link("<i class='fa fa-edit'></i>",['controller'=>'Merchants','action'=>'edit','plugin'=>'Smashdeal','prefix'=>'Backend',$u["id"]],['class'=>'btn btn-default btn-sm','escape'=>false]) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="7">
                        <?php
                        if(isset($s))
                            echo __d('system',"No merchant found for ''{0}'' keyword.",[$s]);
                        else
                            echo __d('system',"No merchant available.");
                        ?>
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?= $this->Form->end(); ?>
    </div>
    <div class="box-footer">
        <?= $this->element('System.paginator'); ?>
    </div>
</div>