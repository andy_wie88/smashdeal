<div style="max-width: 600px; width:600px; margin:auto">
    <div style="content: '';display:table; clear: both;width: 100%">
       	<div style="background:#fff;color:#333;padding:10px;">
			<p>Hallo <span style="font-weight: bold;color:#01688c"><?= $user['name'] ?></span>,</p>
			<p>Kami dari Team Dealsmash.id mengucapkan Selamat Bergabung kepada Anda. Untuk Mengaktifkan Akun Anda, Silahkan Lakukan Validasi email dengan menklik link dibawah ini.</p>
			<p style="text-align: center">
				<a href="<?= $this->Url->build(['controller'=>'Users','action'=>'validation','plugin'=>'Smashdeal','prefix'=>'Frontend',$user->id,$user->activationcode],['fullBase'=>true]) ?>" style="display: inline-block; background-color:#01688c;padding:5px 10px;border-radius: 10px;color:#fff;text-decoration: none;">Aktifkan Akun</a>
			</p>
			<p style="text-align: center">
				atau, jika link diatas tidak berkerja
			</p>
			<p style="text-align: center">
				<?= $this->Url->build(['controller'=>'Users','action'=>'validation','plugin'=>'Smashdeal','prefix'=>'Frontend',$user->id,$user->activationcode],['fullBase'=>true]); ?>
			</p>
			<p>
				Ayo Jangan Terlambat !!! Ikutilah Setiap Peluang <strong>BARGAIN</strong> sedini mungkin di setiap produk kami, dan dapatkan keuntungannya menjabat sebagai Status <strong>SINGLE HOST</strong> ataupun Status <strong>MULTIPLE HOST</strong>. Kami berharap Anda adalah Deal-Smasher yang Handal dan Beruntung.
			</p>
			<p>
				Salam,<br/>
				<span style="font-weight: bold;color:#01688c">Team Dealsmash</span>
			</p>
		</div>
    </div>
</div>