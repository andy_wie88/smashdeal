<?php
    use Cake\I18n\I18n;

    I18n::locale('id_ID');
?>
<div style="max-width: 900px; margin:auto;width: 800px;">
    <div style="content: '';display:table; clear: both;width: 100%">
        <div style="width: 100%; background-color:#fff;padding:15px;">
            <p>Dear <span style="font-weight: bold;color:#01688c"><?= $user['name'] ?></span>,</p>
            <p>Terima kasih telah melakukan pembelian melalui Dealsmash. Produk yang anda beli dengan kode voucher dibawah ini :</p>
            <div style="padding:20px; font-size: 16px; text-transform: uppercase; color:#01688c; font-weight: bold; border:4px dashed #01688c;text-align: center">
                <?= $voucher['vouchercode']; ?>
            </div>
            <p>sudah berhasil diklaim. silahkan lanjutkan ke proses pembayaran dan serah terima barang. </p>
            <p>
                Salam,<br/>
                <span style="font-weight: bold;color:#01688c">Team Dealsmash</span>
            </p>
        </div>
    </div>
</div>