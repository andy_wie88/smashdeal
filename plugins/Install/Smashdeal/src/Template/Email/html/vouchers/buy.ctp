<?php
	use Cake\I18n\I18n;

	I18n::locale('id_ID');
?>
<div style="max-width:800px;width:800px;margin:auto">
    <div style="content: '';display:table; clear: both;width: 100%">
        <div style="width:100%; background:#fff;color:#333;padding:10px;">
			<p style="text-align:center; padding: 20px;">
				Terima Kasih <?= $user['name'] ?> telah melakukan pembelian <strong style="color:#01688c">Bargain Token</strong> dari <a href="http://dealsmash.id" style="color:#01688c">dealsmash.id</a>
			</p>
			<div style="width: 100%;border-top:1px solid #ddd;padding-bottom: 10px;"></div>
			<p style="text-align: center">
				Lakukan konfimasi pembayaran di link dibawah ini :
				<div style="text-align: center; padding: 15px;">
					<a href="<?= $this->Url->build(['controller'=>'Vouchers','action'=>'paymentConfirmation','plugin'=>'Smashdeal','prefix'=>'Frontend',$purchase['invoiceno']],['fullBase'=>true]) ?>" style="display: inline-block; background-color:#01688c;padding:5px 10px;border-radius: 10px;color:#fff;text-decoration: none;">Konfirmasi Pembayaran</a>
				</div>
			</p>
			<p style="text-align: center">
				<strong>Catatan : </strong>Sebelum melakukan konfirmasi pembayaran, silahkan lakukan pembayaran ke salah satu bank kami dibawah ini :
			</p>
			<p>
				<table style="border-collapse: collapse; width: 100%">
					<thead>
						<tr style="background-color:#333;color:#fff;padding-top:5px;padding-bottom: 5px;">
							<td style="padding-top:5px;padding-bottom: 5px;">
								Bank
							</td>
							<td style="padding-top:5px;padding-bottom: 5px;">
								No. Rekening
							</td>
							<th style="padding-top:5px;padding-bottom: 5px;">
								Nama Pemilik
							</th>
						</tr>
					</thead>
					<tbody>
						<tr style="padding-top:5px;padding-bottom: 5px;">
							<td style="padding-top:5px;padding-bottom: 5px;">BCA</td>
							<td style="padding-top:5px;padding-bottom: 5px;">8280 780 988</td>
							<td style="padding-top:5px;padding-bottom: 5px;">Barisan Usaha Media Indonesia, CV.</td>
						</tr>
					</tbody>
				</table>
			</p>
			<div style="width: 100%;border-top:1px solid #ddd;padding-bottom: 10px;"></div>
			<div>
				<h1 style="font-size: 1.5em">
					Rincian Pesanan
				</h1>
				<p><strong>Invoice No : </strong><?= $purchase['invoiceno'] ?></p>
				<p><strong>Tanggal : </strong><?= $purchase['created']->i18nFormat("dd/MM/yyyy") ?></p>
			</div>
			<p>
				<table style="border-collapse: collapse; width: 100%">
					<thead>
						<tr style="background-color:#333;color:#fff;padding-top:5px;padding-bottom: 5px;">
							<td style="padding-top:5px;padding-bottom: 5px;">
								Produk
							</td>
							<td style="padding-top:5px;padding-bottom: 5px;">
								Total
							</td>
						</tr>
					</thead>
					<tbody>
						<tr style="padding-top:5px;padding-bottom: 5px;">
							<td style="padding-top:5px;padding-bottom: 5px;"><?= $purchase['tokenamount'] ?> Bargain Token</td>
							<td style="padding-top:5px;padding-bottom: 5px;text-align: right;"><?= $this->Number->currency($purchase['price']) ?></td>
						</tr>
					 	<?php if($purchase->discount!=0): ?>
                            <tr>
                            	<td style="text-align: right;padding-top:5px;padding-bottom: 5px;">
                                    <?= __d("smashdeal","Diskon ") ?>
                                </td>
                                <td style="text-align: right;padding-top:5px;padding-bottom: 5px;">
                                    <?php
                                        $discount = ($purchase->price * ($purchase->discount/100));
                                        echo $this->Number->currency($discount);
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
						<tr>
                            <td style="text-align: right;padding-top:5px;padding-bottom: 5px;">
                                <?= __d("smashdeal","Kode Unik :") ?>
                            </td>
                            <td style="text-align: right;padding-top:5px;padding-bottom: 5px;">
                                <?= $this->Number->currency($purchase->uniquenumber); ?>
                            </td>
                        </tr>
                        <tr style="background-color: #01688c;color:#fff">
                            <td style="text-align: right;padding-top:5px;padding-bottom: 5px;">
                            	Grand Total :
                        	</td>
                            <td style="text-align: right;padding-top:5px;padding-bottom: 5px;">
                                <?= $this->Number->currency($purchase->grandtotal); ?>
                            </td>
                        </tr>
					</tbody>
				</table>
			</p>
			<div style="width: 100%;border-top:1px solid #ddd;padding-bottom: 10px;"></div>
			<p style="text-align: center">
				Lakukan konfimasi pembayaran di link dibawah ini :
				<div style="text-align: center; padding: 15px;">
					<a href="<?= $this->Url->build(['controller'=>'Vouchers','action'=>'paymentConfirmation','plugin'=>'Smashdeal','prefix'=>'Frontend',$purchase['invoiceno']],['fullBase'=>true]) ?>" style="display: inline-block; background-color:#01688c;padding:5px 10px;border-radius: 10px;color:#fff;text-decoration: none;">Konfirmasi Pembayaran</a>
				</div>
			</p>
			<p style="text-align: center">
				<strong>Catatan : </strong>Sebelum melakukan konfirmasi pembayaran, silahkan lakukan pembayaran ke salah satu bank kami dibawah ini :
			</p>
			<p>
				<table style="border-collapse: collapse; width: 100%">
					<thead>
						<tr style="background-color:#333;color:#fff;padding-top:5px;padding-bottom: 5px;">
							<td style="padding-top:5px;padding-bottom: 5px;">
								Bank
							</td>
							<td style="padding-top:5px;padding-bottom: 5px;">
								No. Rekening
							</td>
							<th style="padding-top:5px;padding-bottom: 5px;">
								Nama Pemilik
							</th>
						</tr>
					</thead>
					<tbody>
						<tr style="padding-top:5px;padding-bottom: 5px;">
							<td style="padding-top:5px;padding-bottom: 5px;">BCA</td>
							<td style="padding-top:5px;padding-bottom: 5px;">8280 780 988</td>
							<td style="padding-top:5px;padding-bottom: 5px;">Barisan Usaha Media Indonesia, CV.</td>
						</tr>
					</tbody>
				</table>
			</p>
			<div style="width: 100%;border-top:1px solid #ddd;padding-bottom: 10px;"></div>
			<p>
				<strong>Catatan : </strong>Konfirmasi Pembayaran akan diproses dalam 1 x 24 Jam
			</p>
			<p>
				Salam,<br/>
				<span style="font-weight: bold;color:#01688c">Team Dealsmash</span>
			</p>
		</div>  
    </div>
</div>