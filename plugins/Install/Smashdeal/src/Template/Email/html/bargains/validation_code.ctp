<div style="max-width: 500px; width: 500px; margin:auto">
    <div style="content: '';display:table; clear: both;width: 100%">
		<div style="width: 100%; background:#fff;color:#333;padding:10px;">
			<p>Dear <span style="font-weight: bold;color:#01688c"><?= $user['name'] ?></span>,</p>
			<p>Silahkan tunjukan kode berikut ini ke pihak merchant: </p>
			<p style="text-align: center;padding-top:10px; padding-bottom: 10px;">
				<span style="padding:10px; font-size: 14px; text-transform: uppercase; color:#01688c; font-weight: bold; border:4px dotted #01688c;">
					Kode : <?= $rand ?>
				</span>
			</p>
			<p>
				Kode diatas kan expired dalam 1 jam.
			</p>
			<p>
				Salam,<br/>
				<span style="font-weight: bold;color:#01688c">Team Dealsmash</span>
			</p>
		</div>
    </div>
</div>