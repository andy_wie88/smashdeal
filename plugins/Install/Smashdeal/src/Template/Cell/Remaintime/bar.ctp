<div>
	<span>
		<?= __d("smashdeal","Bargain Quota :") ?>
	</span>
	<span class="pull-right">
		<?php 

		    if($bargainquota<=$bargaintimes):
		        echo __d("dealsmash","Bargain Berakhir");
		    else:
		        echo $bargaintimes."/".$bargainquota;
		    endif;
		?>
	</span>
</div>
<div class="bar-wrapper">
	<div class="the-bar" style="width: <?= $percentage ?>%"></div>
</div>