<?php if(!isset($this->request->session()->read('DealAuth')["User"])): ?>
    <div class="selection-link user">
        <div class="user-wrapper">
            <span class="icon sm-user"></span>
            <?= $this->Html->link(__d("smashdeal", "Register"),['controller'=>'Users','action'=>'register','prefix'=>'Frontend','plugin'=>'Smashdeal']) ?>
            |
            <?= $this->Html->link(__d("smashdeal", "Login"),['controller'=>'Users','action'=>'login','prefix'=>'Frontend','plugin'=>'Smashdeal']) ?>
        </div>
    </div>
<?php else: ?>
    <div class="selection-link user">
        <div class="user-wrapper">
            <span class="icon sm-user"></span>
            <?= $this->Html->link($this->request->session()->read('DealAuth')["User"]["name"],['controller'=>'Users','action'=>'profile','prefix'=>'Frontend','plugin'=>'Smashdeal']) ?>
            |
            <?= $this->Html->link(__d("smashdeal", "Logout"),['controller'=>'Users','action'=>'logout','prefix'=>'Frontend','plugin'=>'Smashdeal']) ?>
        </div>
    </div>
<?php endif; ?>
