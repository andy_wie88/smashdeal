<?php if(count($slideshows->toArray())>0): ?>
<div class="carousel carousel-slider" style="height: 150px;">
  <?php foreach($slideshows as $slideshow): ?>
      <a class="carousel-item" href="#<?= $slideshow['id'] ?>!">
        <?=$this->cell('System.MediaRenderer',['mediaid'=>$slideshow['mediaid'],'plain'=>true]); ?>    
      </a>
  <?php endforeach; ?>
</div>
<?php else: ?>
    <div style="clear:both;margin-top:20px;"></div>
<?php endif; ?>
<script type="text/javascript">
  $(document).ready(function(){
     $('.carousel').carousel({fullWidth: true});
  });
</script>