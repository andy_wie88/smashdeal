<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * CTValue cell
 */
class CTValueCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        if(isset($this->request->session()->read('DealAuth')['User']))
        {
            $this->loadModel('Smashdeal.TbDealUsers');
            $user = $this->TbDealUsers->get($this->request->session()->read('DealAuth')['User']['id']);
            $this->set('user',$user);
        }
    }

    public function mobile()
    {
        if(isset($this->request->session()->read('DealAuth')['User']))
        {
            $this->loadModel('Smashdeal.TbDealUsers');
            $user = $this->TbDealUsers->get($this->request->session()->read('DealAuth')['User']['id']);
            $this->set('user',$user);
        }
    }
}
