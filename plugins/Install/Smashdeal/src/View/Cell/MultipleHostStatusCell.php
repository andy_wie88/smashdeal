<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * MultipleHostStatus cell
 */
class MultipleHostStatusCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($bargain)
    {
        if($this->isMultipleHost($bargain))
        {
            $this->loadModel('Smashdeal.TbDealUserProducts');
            $this->loadModel('Smashdeal.TbDealProducts');
            $bargains = $this->TbDealUserProducts->find('all')->where(['price'=>$bargain->price,'productid'=>$bargain->productid,'userid !='=>$bargain->userid])->order(['bargaintime'=>'ASC'])->toArray();
            $product = $this->TbDealProducts->get($bargain->productid);
            $price = $this->request->session()->read('Setting')["SINGLETOKENPRICE"];
            $percentage = $this->request->session()->read('Setting')["MULTIPLEHOSTPRC"];
            $shareprice = 0;
            foreach($bargains as $b)
            {
                $shareprice += ($product->bargaintoken * $price) * ($percentage/100);
            }
            $this->set('shareprice',$shareprice);
        }
    }

    private function isMultipleHost($bargain)
    {
        $this->loadModel('Smashdeal.TbDealUserProducts');
        $others = $this->TbDealUserProducts->find('all')
            ->where(['productid'=>$bargain->productid,'price'=>$bargain->price])
            ->order(['bargaintime'=>'ASC'])
            ->toArray();
        if(count($others)>1)
        {
            if($others[0]->id == $bargain->id)
            {
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
}
