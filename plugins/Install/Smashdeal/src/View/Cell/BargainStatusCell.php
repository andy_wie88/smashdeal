<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * BargainStatus cell
 */
class BargainStatusCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($bargain)
    {
        if($this->isSingleHost($bargain))
        {
            $this->set('label',__d("smashdeal","Single Host Bargain"));
        }
        else{
            $multipleHost = $this->isMultipleHost($bargain);
            $this->set('bargain',$bargain);
            $this->set('label',$multipleHost["label"]);
        }
    }

    public function bargainClass($bargain)
    {
        if($this->isSingleHost($bargain))
        {
            $rank = $this->singleHostRank($bargain);
            if($rank>=1 && $rank<=3)
            {
                $this->set('className','rank-'.$rank);
            }
            else{
                $this->set('className','singlehost');
            }
        }
        else{
            $multipleHost = $this->isMultipleHostTwo($bargain);
            $this->set('className',$multipleHost);
        }
    }

    private function singleHostRank($bargain)
    {
        $this->loadModel('Smashdeal.ViewDealUserBargainRanks');
        $bargains = $this->ViewDealUserBargainRanks->find('all')->where(['productid'=>$bargain->productid])->toArray();
        $rank = 1;
        foreach($bargains as $b)
        {
            if($b->price == $bargain->price)
            {
                return $rank;
            }
            $rank++;
        }
        return $rank;
    }

    private function isSingleHost($bargain)
    {
        $this->loadModel('Smashdeal.TbDealUserProducts');
        $others = $this->TbDealUserProducts->find('all')
            ->where(['userid !='=>$bargain->userid,'productid'=>$bargain->productid,'price'=>$bargain->price])
            ->toArray();
        if(count($others)>0)
        {
            return false;
        }
        return true;
    }

    private function isMultipleHostTwo($bargain)
    {
        $others = $this->TbDealUserProducts->find('all')
            ->where(['productid'=>$bargain->productid,'price'=>$bargain->price])
            ->order(['bargaintime'=>'ASC'])
            ->toArray();

        if(count($others)>0)
        {
            if($others[0]->id == $bargain->id)
            {
                return 'multiplehost';
            }
            else{
                return 'guestto';
            }
        }
    }

    private function isMultipleHost($bargain)
    {
        $others = $this->TbDealUserProducts->find('all')
            ->where(['productid'=>$bargain->productid,'price'=>$bargain->price])
            ->order(['bargaintime'=>'ASC'])
            ->toArray();

        if(count($others)>0)
        {
            if($others[0]->id == $bargain->id)
            {
                return ['label'=>__d("smashdeal","Multiple Host Bargain")];
            }
            else{
                $this->loadModel('Smashdeal.TbDealUsers');
                $user = $this->TbDealUsers->get($others[0]->userid);
                return ['label'=>__d("smashdeal","Guest To ''{0}''",$user->username)];
            }
        }
    }
}
