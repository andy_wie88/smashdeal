<?php
namespace Smashdeal\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * GeneralConfig Form.
 */
class GeneralConfigForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return $this
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField("SINGLETOKENPRICE.id","string")
            ->addField("SINGLETOKENPRICE.value","string")
            ->addField("MULTIPLEHOSTPRC.id","string")
            ->addField("MULTIPLEHOSTPRC.value","string");
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        $validator->notEmpty('SINGLETOKENPRICE.id',__d("system","Invalid Form"));
        $validator->notEmpty('SINGLETOKENPRICE.value',__d("system","Site Title can't be empty"));
        $validator->notEmpty('MULTIPLEHOSTPRC.id',__d("system","Invalid Form"));
        $validator->notEmpty('MULTIPLEHOSTPRC.value',__d("system","Site Tagline can't be empty"));
        return $validator;
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }
}
