<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealClaimVouchers Model
 *
 * @method \Smashdeal\Model\Entity\TbDealClaimVoucher get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealClaimVoucher newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealClaimVoucher[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealClaimVoucher|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealClaimVoucher patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealClaimVoucher[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealClaimVoucher findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealClaimVouchersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_claim_vouchers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('claimid', 'create')
            ->notEmpty('claimid');

        $validator
            ->integer('claimtype')
            ->allowEmpty('claimtype');

        $validator
            ->allowEmpty('vouchercode');

        $validator
            ->date('validity')
            ->allowEmpty('validity');

        $validator
            ->dateTime('claimat')
            ->allowEmpty('claimat');

        $validator
            ->allowEmpty('claimby');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
