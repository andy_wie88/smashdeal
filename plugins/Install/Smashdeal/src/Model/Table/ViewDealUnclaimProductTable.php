<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealUnclaimProduct Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealUnclaimProduct get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnclaimProduct newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnclaimProduct[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnclaimProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnclaimProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnclaimProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnclaimProduct findOrCreate($search, callable $callback = null, $options = [])
 */
class ViewDealUnclaimProductTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_unclaim_product');
        $this->displayField('name');

        $this->belongsTo('Product',['className'=>'Smashdeal.TbDealProducts','foreignKey'=>'productid']);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('vouchercode');

        $validator
            ->allowEmpty('productid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->integer('vouchertype')
            ->allowEmpty('vouchertype');

        $validator
            ->boolean('isvalid')
            ->allowEmpty('isvalid');

        $validator
            ->dateTime('invalidate')
            ->allowEmpty('invalidate');

        $validator
            ->allowEmpty('invalidateuser');

        $validator
            ->allowEmpty('productname');

        $validator
            ->allowEmpty('slug');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('merchantid');

        $validator
            ->allowEmpty('username');

        $validator
            ->allowEmpty('idno');

        $validator
            ->allowEmpty('address');

        $validator
            ->date('birthday')
            ->allowEmpty('birthday');

        $validator
            ->allowEmpty('handphoneno');

        $validator
            ->allowEmpty('phoneno');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    public function findMerchant(Query $query,array $options)
    {
        $user = $options["merchantid"];

        $query->where(['merchantid'=>$user]);

        return $query;

    }

}