<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealMultihostSummary Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealMultihostSummary get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealMultihostSummary newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealMultihostSummary[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealMultihostSummary|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealMultihostSummary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealMultihostSummary[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealMultihostSummary findOrCreate($search, callable $callback = null)
 */
class ViewDealMultihostSummaryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_multihost_summary');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('r');

        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('productid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->dateTime('bargaintime')
            ->allowEmpty('bargaintime');

        $validator
            ->allowEmpty('guesthost');

        return $validator;
    }
}
