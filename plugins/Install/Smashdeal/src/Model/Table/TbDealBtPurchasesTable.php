<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealBtPurchases Model
 *
 * @method \Smashdeal\Model\Entity\TbDealBtPurchase get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPurchase newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPurchase[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPurchase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPurchase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPurchase[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPurchase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealBtPurchasesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_bt_purchases');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('invoiceno', 'create')
            ->notEmpty('invoiceno');

        $validator
            ->integer('paymenttype')
            ->allowEmpty('paymenttype');

        $validator
            ->allowEmpty('tokenproductid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->decimal('uniquenumber')
            ->allowEmpty('uniquenumber');

        $validator
            ->decimal('grandtotal')
            ->allowEmpty('grandtotal');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findUser(Query $query, array $options)
    {
        $user = $options["userid"];

        $query->where(['userid'=>$user]);

        return $query;
    }
}
