<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealMerchants Model
 *
 * @method \Smashdeal\Model\Entity\TbDealMerchant get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealMerchant newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealMerchant[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealMerchant|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealMerchant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealMerchant[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealMerchant findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealMerchantsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_merchants');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->hasMany('Products',['className'=>'Smashdeal.TbDealProducts','foreignKey'=>'merchantid','dependent'=>true]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->allowEmpty('phonenoone');

        $validator
            ->allowEmpty('phonenotwo');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('createby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function findStatus($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = (strtolower($s)=="active")?TRUE:FALSE;
        $query->where(['isactive'=>$status]);
        return $query;
    }

    public function findName($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        if($status==null)
            $query->where([
                'or'=>[
                    ['lower(name) LIKE'=>'%'.$s.'%']
                ]]);
        else{
            $bools = (strtolower($status)=="active")?TRUE:FALSE;
            $query->where(
                [
                    'AND'=>
                        ['or'=>[
                            ['lower(name) LIKE'=>'%'.$s.'%']
                        ]],
                    'isactive'=>$bools
                ]);
        }
        return $query;
    }

    public function findAuth(Query $query,array $options)
    {
        $query->where(['isactive'=>TRUE]);
        return $query;
    }
}
