<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealProductMedias Model
 *
 * @method \Smashdeal\Model\Entity\TbDealProductMedia get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMedia newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMedia[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMedia|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMedia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMedia[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMedia findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealProductMediasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_product_medias');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');


        $this->belongsTo('Products',['Smashdeal.TbDealProducts','foreignKey'=>'productid']);
        $this->belongsTo('System.TbSysPosts',['foreignKey'=>'mediaid','dependent'=>true]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('productid');

        $validator
            ->allowEmpty('mediaid');

        $validator
            ->integer('index')
            ->allowEmpty('index');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
