<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealCashTokenJournal Entity
 *
 * @property string $userid
 * @property \Cake\I18n\Time $exttime
 * @property string $note
 * @property float $debit
 * @property float $credit
 */
class ViewDealCashTokenJournal extends Entity
{

}
