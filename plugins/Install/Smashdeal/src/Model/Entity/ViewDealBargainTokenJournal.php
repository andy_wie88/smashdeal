<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealBargainTokenJournal Entity
 *
 * @property string $userid
 * @property \Cake\I18n\Time $exttime
 * @property string $note
 * @property int $debit
 * @property int $credit
 */
class ViewDealBargainTokenJournal extends Entity
{

}
