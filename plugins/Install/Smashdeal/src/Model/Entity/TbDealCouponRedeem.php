<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbDealCouponRedeem Entity
 *
 * @property string $id
 * @property string $couponid
 * @property string $userid
 * @property \Cake\I18n\Time $redeemtime
 * @property string $computerip
 * @property string $useragent
 */
class TbDealCouponRedeem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
