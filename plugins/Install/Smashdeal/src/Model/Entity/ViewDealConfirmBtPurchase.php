<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealConfirmBtPurchase Entity
 *
 * @property string $id
 * @property string $invoiceno
 * @property int $paymenttype
 * @property string $tokenproductid
 * @property string $userid
 * @property float $price
 * @property float $uniquenumber
 * @property float $discount
 * @property float $grandtotal
 * @property int $tokenamount
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class ViewDealConfirmBtPurchase extends Entity
{

}
