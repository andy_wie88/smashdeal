<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealSinglehostSummary Entity
 *
 * @property string $userid
 * @property string $productid
 * @property float $price
 * @property int $count
 * @property \Cake\I18n\Time $bargaintime
 *
 * @property \Smashdeal\Model\Entity\TbDealUser $user
 */
class ViewDealSinglehostSummary extends Entity
{

}
