<?php
namespace Smashdeal\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Controller\Component\CategoryComponent;

/**
 * Smashdeal\Controller\Component\CategoryComponent Test Case
 */
class CategoryComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Controller\Component\CategoryComponent
     */
    public $Category;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Category = new CategoryComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Category);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
