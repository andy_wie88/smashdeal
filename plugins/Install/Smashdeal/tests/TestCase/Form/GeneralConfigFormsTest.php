<?php
namespace Smashdeal\Test\TestCase\Form;

use Cake\TestSuite\TestCase;
use Smashdeal\Form\GeneralConfigForms;

/**
 * Smashdeal\Form\GeneralConfigForms Test Case
 */
class GeneralConfigFormsTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Form\GeneralConfigForms
     */
    public $GeneralConfigForms;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->GeneralConfigForms = new GeneralConfigForms();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GeneralConfigForms);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
