<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewUnclaimMybargainTable;

/**
 * Smashdeal\Model\Table\ViewUnclaimMybargainTable Test Case
 */
class ViewUnclaimMybargainTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewUnclaimMybargainTable
     */
    public $ViewUnclaimMybargain;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_unclaim_mybargain'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewUnclaimMybargain') ? [] : ['className' => 'Smashdeal\Model\Table\ViewUnclaimMybargainTable'];
        $this->ViewUnclaimMybargain = TableRegistry::get('ViewUnclaimMybargain', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewUnclaimMybargain);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
