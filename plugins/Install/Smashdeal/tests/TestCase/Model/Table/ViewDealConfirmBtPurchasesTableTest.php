<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealConfirmBtPurchasesTable;

/**
 * Smashdeal\Model\Table\ViewDealConfirmBtPurchasesTable Test Case
 */
class ViewDealConfirmBtPurchasesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealConfirmBtPurchasesTable
     */
    public $ViewDealConfirmBtPurchases;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_confirm_bt_purchases'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealConfirmBtPurchases') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealConfirmBtPurchasesTable'];
        $this->ViewDealConfirmBtPurchases = TableRegistry::get('ViewDealConfirmBtPurchases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealConfirmBtPurchases);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
