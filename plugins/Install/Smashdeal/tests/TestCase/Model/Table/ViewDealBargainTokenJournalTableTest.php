<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealBargainTokenJournalTable;

/**
 * Smashdeal\Model\Table\ViewDealBargainTokenJournalTable Test Case
 */
class ViewDealBargainTokenJournalTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealBargainTokenJournalTable
     */
    public $ViewDealBargainTokenJournal;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_bargain_token_journal'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealBargainTokenJournal') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealBargainTokenJournalTable'];
        $this->ViewDealBargainTokenJournal = TableRegistry::get('ViewDealBargainTokenJournal', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealBargainTokenJournal);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
