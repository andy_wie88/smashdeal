<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\TbDealProductMetadatasTable;

/**
 * Smashdeal\Model\Table\TbDealProductMetadatasTable Test Case
 */
class TbDealProductMetadatasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\TbDealProductMetadatasTable
     */
    public $TbDealProductMetadatas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.tb_deal_product_metadatas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbDealProductMetadatas') ? [] : ['className' => 'Smashdeal\Model\Table\TbDealProductMetadatasTable'];
        $this->TbDealProductMetadatas = TableRegistry::get('TbDealProductMetadatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbDealProductMetadatas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
