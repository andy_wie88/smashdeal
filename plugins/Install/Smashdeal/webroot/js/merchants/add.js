$(document).ready(function(e){

    $("#media-selector").mediaSelector({
        'title':'Merchant Logo',
        'list_url':$("#media-list-link").attr("data-href"),
        'upload_url':$("#media-upload-link").attr("data-href"),
        'attribute_url':$("#media-attribute-link").attr("data-href"),
        'multiple':false,
        'filter':'image',
        'onselect' : function(e,selected)
        {
            $.get($("#get-image-link").attr("data-href")+"/"+selected,function(data)
            {
                $("#image-wrapper").empty().append($(data));
                $("#merchantlogo").val(selected);
            });
        }
    });

});