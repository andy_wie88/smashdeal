$(document).ready(function(e)
{

    $(".money").priceFormat({
        prefix: "Rp. ",
        centsSeparator: ",",
        thousandsSeparator: ".",
        centsLimit: 0
    });

    $(".money").blur(function (e) {
        var target = $(this).attr("data-target");
        $(target).val($(this).unmask().trim());
    });

});