$(document).ready(function(e)
{
	var date = $("#countdown").attr("data-time");
	$("#countdown").countdown(date, function(event) {
		$(this).text(
		  event.strftime('%D Hari %H:%M:%S')
		);
	});

	$(".guest").click(function(e)
	{
		e.preventDefault();
		$.get($(this).attr("href"),function(data)
		{
			$("#modal-content").empty().append($(data));
			$("#myModal").modal('show');
		});
	})

});