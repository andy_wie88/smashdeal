<?php

namespace Install\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class SetupForm extends Form{

	protected function _buildSchema(Schema $schema)
	{
		return $schema->addField("dbengine","string")
					->addField("host","string")
					->addField("port","integer")
					->addField("database","string")
					->addField("username","string")
					->addField("password","string")
					->addField("webtitle","string")
					->addField("tagline","string")
					->addField("language","string")
					->addField("adminprx","string");
	}

	protected function _buildValidator(Validator $validator)
	{
		$validator->notEmpty('dbengine',__d("install","Please choose database engine."));

		$validator->notEmpty('host',__d("install","Database host couldn't be empty"));

		$validator->notEmpty('port',__d("install","Database port couldn't be empty"));

		$validator->notEmpty('database',__d("install","Database name couldn't be empty"));

		$validator->notEmpty('username',__d("install","Username couldn't be empty"));

		$validator->notEmpty('password',__d("install","Password couldn't be empty"));

		$validator->notEmpty('website',__d("install","Website title couldn't be empty"));

		$validator->notEmpty('language',__d("install","Please choose your default language"));

		$validator->notEmpty('adminprx',__d("install","Admin page prefix couldn't be empty"));

		return $validator;
	}

	protected function _execute(array $data)
    {
        // Send an email.
        return true;
    }
}