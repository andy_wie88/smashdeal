<div style="background-color: #fff">
    <table bgcolor="#fff" cellpadding="15" width="100%">
        <tbody>
            <tr>
                <td>
                    <table height="79" style="max-width: 600px;margin:0px auto" width="588">
                        <tbody>
                            <tr>
                                <th style="padding:10px 40px;text-align:center">
                                    <img src="http://static.dealsmash.id/img/logo-sm.png" alt="Dealsmash logo" height="50"/>
                                </th>
                                <th style="padding: 10px 40px;text-align:center">
                                    <div>
                                        <a href="https://www.instagram.com/dealsmash.id/" target="_blank">
                                            <span style="display: inline-block; background:url('http://static.dealsmash.id/img/header-footer-sprite.png') no-repeat;overflow: hidden;text-indent: -9999px;text-align: left;width: 28px;height: 28px;background-position: -3px -223px;"></span>
                                        </a>
                                    </div>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                    <hr style="height:0px;border:none;border-top:1px solid #dddddd">
                    <div style="background-color:#f4f4f4;padding:30px;">
                        <?= $this->fetch('content') ?>
                    </div>
                    <hr style="height:0px;border:none;border-top:1px solid #dddddd">
                    <div style="background-color:#01688C;margin-top:5px; margin-bottom: 5px;padding:20px;color:#fff">
                        <p align="center" style="color:#fff;font-size:10.5pt">Temukan kami di: <a style="color:#f9b916" href="https://www.instagram.com/dealsmash.id/">Instagram</a></p>
                        <p align="center" style="color:#fff;font-size:10.5pt">Jika Anda memiliki Inquiry atau Pertanyaan mengenai Dealsmash, Anda dipersilahkan untuk mengirimkan ke alamat e-mail : <a style="color:#f9b916" href="maito:admin@dealsmash.id">admin@dealsmash.id</a></p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>