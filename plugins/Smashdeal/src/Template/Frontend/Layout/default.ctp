<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php if(!isset($title)): ?>
                <?= $config_front["SITETITLE"] ?> | <?= $config_front["SITETAGLN"] ?>
            <?php else: ?>
                <?= $config_front["SITETITLE"]; ?> | <?= $title ?>
            <?php endif; ?>
        </title>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans|Titillium+Web:400,700" rel="stylesheet">
        <?php
            echo $this->AssetCompress->css('Smashdeal.sm');
            echo $this->AssetCompress->script('Smashdeal.sm');
            echo $this->fetch('css');
        ?>
    </head>
    <body>
        <div class="page-wrap">
            <?= $this->Element('Smashdeal.header') ?>
            <div class="container">
                <?= $this->Flash->render('auth'); ?>
                <?= $this->Flash->render(); ?>
            </div>
            <div>
                <?= $this->fetch('content'); ?>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <h4 class="footer-link-header"><?= __d("smashdeal","Help") ?></h4>
                    <?= $this->cell('System.MenuRenderer',['locator'=>'SMFOOTERMENU','ulclass'=>'footer-link']) ?>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h4 class="footer-link-header"><?= __d("smashdeal","Follow Us At") ?></h4>
                    <ul class="social-link-list">
                        <li>
                            <?= $this->Html->link("<i class='sm sm-instagram'></i>",'https://www.instagram.com/dealsmash.id/',['escape'=>false,'class'=>'instagram-icon','target'=>'_blank']) ?>
                        </li>
                        <li>
                            <?= $this->Html->link("<i class='sm sm-facebook'></i>","https://www.facebook.com/dealsmash.id/",['escape'=>false,'class'=>'facebook-icon','target'=>"_blank"]) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    </body>
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/596ec86b6edc1c10b03469eb/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <?php
        echo $this->fetch('footer-script');
    ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-101653729-1', 'auto');
      ga('send', 'pageview');

    </script>
</html>