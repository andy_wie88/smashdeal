<div class="container sm-main-content">
	<div class="row">
		<div class="col-md-3">
			<div class="sm-box sidebar-wrapper">
				<div class="small-profile">
					<div class="row userpic">
						<div class="col-md-3 min-padding-right">
							<?= $this->Html->image('user.png',['style'=>'width:100%']) ?>
						</div>
						<div class="col-md-9 username">
							<?= $this->request->session()->read('DealAuth')['User']['username'] ?>
						</div>
					</div>
					<div class="row token-wrapper">
						<div class="col-md-3 no-padding-right">
							<?= $this->Html->image('token.svg',['style'=>'width:24px']) ?>
						</div>
						<div class="col-md-9 no-padding-left token-value">
							<?= $this->cell('Smashdeal.BTValue') ?>
						</div>
					</div>
					<div class="row token-wrapper">
						<div class="col-md-3 no-padding-right">
							<i class="fa fa-money"></i>
						</div>
						<div class="col-md-9 no-padding-left token-value">
							<?= $this->cell('Smashdeal.CTValue') ?>
						</div>
					</div>
				</div>
				<div class="separator-wrapper">
					<hr class="separator">
				</div>
				<ul class="sidebar profile">
					<li class="group">
						<?= __d("smashdeal","My Profile") ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","Edit Profile"),['controller'=>'Users','action'=>'profile','plugin'=>'Smashdeal','prefix'=>'Frontend']); ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","Change Password"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend']); ?>
					</li>

				</ul>
				<div class="separator-wrapper">
					<hr class="separator">
				</div>
				<ul class="sidebar profile">
					<li class="group">
						<?= __d("smashdeal","My Bargain") ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","On Going"),['controller'=>'Bargains','action'=>'ongoing','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","Status Klaim"),['controller'=>'Bargains','action'=>'claimstatus','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Bargains','action'=>'history','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
					</li>
				</ul>
				<div class="separator-wrapper">
					<hr class="separator">
				</div>
				<ul class="sidebar profile">
					<li class="group">
						<?= __d("smashdeal","My Purchase") ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","Status Klaim"),['controller'=>'Purchases','action'=>'claimstatus','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Purchases','action'=>'history','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
					</li>
				</ul>
				<div class="separator-wrapper">
					<hr class="separator">
				</div>
				<ul class="sidebar profile">
					<li class="group">
						<?= __d("smashdeal","BT Purchase") ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","Konfirmasi Pembayaran"),['controller'=>'Vouchers','action'=>'unconfirm','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
					</li>
					<li>
						<?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Vouchers','action'=>'history','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-md-9">
			<?= $this->fetch('profile') ?>
		</div>
	</div>
</div>