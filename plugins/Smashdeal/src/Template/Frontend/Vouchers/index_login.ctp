<?= $this->extend('../Users/template'); ?>
<?= $this->start('profile') ?>
<div class="sm-box">
    <h1 class="section-header">
        <?= __d("smashdeal","Buy Token Online") ?>
    </h1>
    <div class="row">
        <?php foreach($tokens  as $token): ?>
            <div class="col-md-3">
                <div class="token-box">
                    <?= $this->Html->image('coin.png') ?>
                    <h2 class="token-box-title">
                        <?= $token->tokenamount ?>
                        <?= ($token->tokenamount>1)?__d("smashdeal","Tokens"):__d("smashdeal","Token"); ?>
                    </h2>
                    <div class="body">
                        <div class="stroke-price text-center">
                            <?php if($token->discount!=0): ?>
                                <?= $this->Number->currency($token->price); ?>
                            <?php endif; ?>
                        </div>
                        <div class="price text-center">
                            <?php if($token->discount==0): ?>
                                <?= $this->Number->currency($token->price); ?>
                            <?php else: ?>
                                <?php
                                    $newprice = 0;
                                    $tmp = $token->price * ($token->discount/100);
                                    $newprice = $token->price - $tmp;
                                    echo $this->Number->currency($newprice);
                                ?>
                                <span class="discount">
                                    <?= $token->discount ?>% OFF
                                </span>
                            <?php endif; ?>
                        </div>
                        <div class="btn-wrapper">
                            <?=  $this->Html->link(__d("smashdeal","Buy"),['controller'=>'Vouchers','action'=>'buy','prefix'=>'Frontend','plugin'=>'Smashdeal',$token['id']],['class'=>'btn btn-smd btn-sm-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    
    <div class="sm-box">
        <h1 class="section-header">
            <?= __d("smashdeal","Beli Bargain Voucher") ?>
        </h1>
        <div class="row">
            <div class="col-md-3">
                <div class="token-box">
                    <?= $this->Html->image('20170624123019.jpeg'); ?>
                    <h2 class="token-box-title">
                        <?= __d("smashdeal","Travel Mall") ?>
                    </h2>
                    <div class="body">
                        <div>
                            <i class="fa fa-address-card"></i>
                            Jalan Asia, Komplek Asia Mega Mas Blok N-1 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="token-box">
                    <?= $this->Html->image('20170624134403.jpeg'); ?>
                    <h2 class="token-box-title">
                        <?= __d("smashdeal","Selular 1") ?>
                    </h2>
                    <div class="body">
                        <div>
                            <i class="fa fa-address-card"></i>
                            Sun Plaza LG Floor B-12a-15, B-11 & B-17 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->end(); ?>