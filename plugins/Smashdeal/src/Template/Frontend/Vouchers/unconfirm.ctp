<?= $this->extend('../Users/template'); ?>
<?= $this->start('profile') ?>
<div class="sm-box clearfix">
	<h1 class="section-header"><?= __d("smashdeal", "Konfirmasi Pembayaran") ?></h1>
	<div class="sm-box box-yellow cleafix">
		<h2 class="sub-header">
			<?= __d("smashdeal","Deal Smash's Bank Account") ?>
		</h2>
		<p><?= __d("smashdeal","Silahkan transfer ke rekening bank dibawah ini :") ?></p>
		<div class="row">
			<div class="col-md-3">
				<div class="sm-box">
					<?= $this->Html->image('icon-bca.png',['style'=>'width:100%']) ?>
					<p>8280 780 988 <br> a/n Barisan Usaha Media Indonesia, CV.</p>
				</div>
			</div>
		</div>
	</div>

	<h3 class="section-header">
		<?= __d("smashdeal","Pembelian BT yang belum dikonfirmasi : ") ?>
	</h3>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th><?= __d("smashdeal","Date") ?></th>
				<th><?= __d("smashdeal","No Invoice") ?></th>
				<th><?= __d("smashdeal","Detail Item") ?></th>
				<th><?= __d("smashdeal","Jumlah Pembayaran") ?></th>
				<th><?= __d("smashdeal","Action") ?></th>
			</tr>
		</thead>

		<?php foreach($unconfirms as $key=>$unconfirm): ?>
			<tbody>
				<tr>
					<td><?= $unconfirm['created']->i18nFormat('dd/MM/yyyy') ?></td>
					<td><?= $unconfirm['invoiceno'] ?></td>
					<td><?= $unconfirm['tokenamount']." Bargain Token" ?></td>
					<td><?= $this->Number->currency($unconfirm['grandtotal']) ?></td>
					<td class="text-center">
						<?= $this->Html->link(__d("smashdeal","konfirmasi"),['controller'=>'Vouchers','action'=>'paymentConfirmation','plugin'=>'Smashdeal','prefix'=>'Frontend',$unconfirm['invoiceno']]) ?>
					</td>
				</tr>
			</tbody>
		<?php endforeach; ?>
	</table>
	<?= $this->element('System.paginator') ?>
</div>
<?= $this->end(); ?>