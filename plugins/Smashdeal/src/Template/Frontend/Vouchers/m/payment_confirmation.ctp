<div class="container sm-main-content">
    <h1 class="section-header center-align">
        <?= $title ?>
    </h1>
    <section class="sub-section clearfix">
        <h2 class="sub-section-header"><?= __d("smashdeal","Detail Pembelian") ?></h2>
        <div class="detail">
            <table class="table cart-table">
                <thead>
                    <tr>
                        <th width="40%" class="text-center"><?= __d("smashdeal","ITEM") ?></th>
                        <th width="5%"></th>
                        <th width="10%" class="text-center"><?= __d("smashdeal","TOTAL") ?></th>
                    </tr>
                    <tbody>
                        <tr>
                            <td>
                                <div class="item">
                                    <strong><?= $tokenproduct['tokenamount'] ?> Bargain Token</strong>
                                </div>
                            </td>
                            <td></td>
                            <td>
                                <div class="item text-right">
                                    <?= $this->Number->currency($purchase->price); ?>
                                </div>
                            </td>
                        </tr>
                        <?php if($purchase->discount!=0): ?>
                            <tr>
                                <td class="text-right">
                                    <?= __d("smashdeal","Diskon ") ?>(<?= $purchase->discount ?>)
                                </td>
                                <td></td>
                                <td>
                                    <?php
                                        echo $this->Number->currency($purchase->discount);
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td class="text-right">
                                <?= __d("smashdeal","Kode Unik :") ?>
                            </td>
                            <td></td>
                            <td class="text-right">
                                <?= $this->Number->currency($purchase->uniquenumber); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">Grand Total :</td>
                            <td></td>
                            <td class="text-right">
                                <?= $this->Number->currency($purchase->grandtotal); ?>
                            </td>
                        </tr>
                    </tbody>
                </thead>
            </table>
        </div>
    </section>
    <div class="row">
        <div class="col s12">
            <section class="sub-section clearfix">
                <h2 class="sub-section-header"><?= __d("smashdeal","Cara Pembayaran via Transfer") ?></h2>
                <div class="detail" style="clear:both">
                    <ol>
                        <li>Lakukan transfer ke rekening di bawah :<br/>
                        <strong>BCA : 8280 780 988 A/N Barisan Usaha Media Indonesia, CV.</strong></li>
                        <li>Setelah transfer, isi kolom konfirmasi Pembayaran disamping ini atau dengan cara : <br/>
                        <strong>Login > Profile > BT Purchase History</strong>
                        </li>
                        <li>Konfirmasi pembayaran akan diproses dalam waktu 1 x 24 jam</li>
                    </ol>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <section class="sub-section clearfix">
                <h2 class="sub-section-header"><?= __d("smashdeal","Konfirmasi Pembayaran") ?></h2>
                <section class="detail" style="clear: both">
                    <?= $this->Form->create($confirmation,['class'=>'form-horizontal']) ?>
                    <div class="form-group">
                        <label for="bankaccountid" class="col-md-4  control-label">
                            <?= __d("smashdeal","Transfer Ke") ?>
                        </label>
                        <div class="col-md-8">
                            <?= $this->Form->input('bankaccountid',['class'=>'form-control','label'=>false,'id'=>'bankaccountid','options'=>$bankaccounts,'required'=>true,'class'=>'browser-default']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="transferdate" class="col-md-4 control-label">
                            <?= __d("smashdeal","Tanggal Transfer") ?>
                        </label>
                        <div class="col-md-8">
                            <input type="date" name="transferdate" class="form-control datepicker" value="<?php $date = new Cake\i18n\Date();echo $date->i18nFormat("dd/MM/yyyy"); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="accountname" class="col-md-4 control-label">
                            <?= __d("smashdeal","Nama Rekening") ?>
                        </label>
                        <div class="col-md-8">
                            <?= $this->Form->input('accountname',['class'=>'form-control','label'=>false,'id'=>'accountname','required'=>true]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nominal" class="col-md-4 control-label">
                            <?= __d("smashdeal","Nominal Transfer") ?>
                        </label>
                        <div class="col-md-8">
                            <?= $this->Form->input('nominal',['class'=>'form-control','label'=>false,'id'=>'nominal','required'=>true]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-push-3 col-md-6">
                            <button class="btn btn-smd btn-sm-primary" type="submit">
                                <?= __d("smashdeal","Konfirmasi Pembayaran") ?>
                            </button>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </section>
            </section>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 50, // Creates a dropdown of 15 years to control year,
        format:'dd/mm/yyyy'
    });
</script>