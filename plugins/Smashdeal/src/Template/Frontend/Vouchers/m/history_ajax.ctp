<?php if(count($unconfirms)>0): ?>
    <?php foreach($unconfirms as $unconfirm): ?>
        <div class="unconfirm-payment">
        	<div class="row">
        		<div class="col s6">
        			<span class="text-label">
        				<?= __d("smashdeal","No. Invoice : ") ?>
        			</span>
        			<br>
        			<?= $unconfirm["invoiceno"] ?>
        		</div>
        		<div class="col s6">
        			<span class="text-label">
        				<?= __d("smashdeal","Tanggal Transaksi : ") ?>
        			</span>
        			<br>
        			<?= $unconfirm["created"]->i18nFormat("dd/MM/yyyy") ?>
        		</div>
        	</div>
        	<div class="row">
                <div class="col s6">
                    <span class="text-label">
                        <?= __d("smashdeal","Jenis Pembayaran : ") ?>
                    </span>
                    <br>
                    <?= ($unconfirm["paymenttype"]==1)?"Transfer":"Cash Token" ?>
                </div>
        		<div class="col s6">
        			<span class="text-label">
        				<?= __d("smashdeal","Total Pembayaran : ") ?>
        			</span>
        			<br>
        			<?= $this->Number->currency($unconfirm["grandtotal"]); ?>
        		</div>
        	</div>
        </div>
    <?php endforeach; ?>
    <?php if(count($unconfirms)>0): ?>
        <div class="hide paginator">
            <?= $this->element('System.paginator') ?>
        </div>
    <?php endif; ?>
<?php endif; ?>