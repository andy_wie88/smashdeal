<?= $this->AssetCompress->script('Smashdeal.bt-buy.m',['block'=>'footer-script']) ?>
<?= $this->Form->create($purchase) ?>
<div class="container sm-main-content">
    <h1 class="section-header center-alignr">
        <?= $title ?>
    </h1>
    <section class="sub-section clearfix">
    	<h2 class="sub-section-header"><span class="number">1</span><?= __d("smashdeal","Detail Pembelian") ?></h2>
    	<div class="detail">
    		<table class="table cart-table">
				<thead>
					<tr>
						<th width="40%" class="text-center"><?= __d("smashdeal","ITEM") ?></th>
						<th width="5%"></th>
						<th width="10%" class="text-center"><?= __d("smashdeal","TOTAL") ?></th>
					</tr>
					<tbody>
						<tr>
							<td>
								<div class="item">
                                    <input type="hidden" name="tokenproductid" value="<?= $tokenproduct->id ?>" id="tokenproductid" />
									<strong><?= $tokenproduct['tokenamount'] ?> Bargain Token</strong>
								</div>
							</td>
							<td></td>
							<td>
								<div class="item text-right">
                                    <input type="hidden" name="price" value="<?= $tokenproduct->price ?>" id="price" />
                                	<?= $this->Number->currency($tokenproduct->price); ?>
								</div>
							</td>
						</tr>
                        <?php if($tokenproduct->discount!=0): ?>
							<tr>
                                <td class="text-right">
                                    <?= __d("smashdea","Diskon ") ?>(<?= $tokenproduct->discount ?>%)
                                </td>
                                <td></td>
                                <td class="text-right">
                                    <input type="hidden" name="discount" value="<?= $tokenproduct->discount ?>" id="discount" />
                                    <?php
                                        $tmp = $tokenproduct->price * ($tokenproduct->discount/100);
                                        echo $this->Number->currency($tmp);
                                    ?>
                                </td>
                            </tr>
                        <?php else: ?>
                            <input type="hidden" name="discount" value="<?= $tokenproduct->discount ?>" id="discount" />
                    	<?php endif; ?>
                        <tr>
                            <td class="text-right">
                                <?= __d("smashdeal","Kode Unik :") ?>
                            </td>
                            <td></td>
                            <td class="text-right">
                                <input type="hidden" name="uniquenumber" value="<?= $uniquenumber ?>" id="uniquenumber" />
                                <?= $this->Number->currency($uniquenumber); ?>
                            </td>
                        </tr>
                		<tr>
                			<td class="text-right">Grand Total :</td>
                			<td></td>
                			<td class="text-right">
                				<?php 
                					$newprice = 0; 
                					if(isset($tmp))
                					{
                						$newprice = $tokenproduct['price'] - $tmp;
                					}
                					else
                					{
                						$newprice = $tokenproduct['price'];
                					}
                				?>
                                <span id="withunique">
                                    <?= $this->Number->currency($newprice + $uniquenumber); ?>
                                </span>
                                <span id="withoutunique" style="display:none">
                                    <?= $this->Number->currency($newprice); ?>
                                </span>
                			</td>
                		</tr>
					</tbody>
				</thead>
			</table>
    	</div>
    </section>
    <section class="sub-section clearfix">
    	<h2 class="sub-section-header"><span class="number">2</span><?= __d("smashdeal","Metode Pembayaran") ?></h2>
    	<div class="detail" style="clear:both">
    		<div class="payment-choice clearfix">
    			<div class="radio-inline">
					<input type="radio" value="1" name="paymenttype" data-target="transfer" id="transfer-radio" checked="true" />
					<label for="transfer-radio"><?= __d("smashdeal","Transfer Bank") ?></label>
				</div>
				<div class="radio-inline">
					<input class="radio"  type="radio" value="2" name="paymenttype" id="cashtoken-radio" data-target="cashtoken"/>
					<label for="cashtoken-radio"><?= __d("smashdeal","Cash Token") ?> (<?= $this->Number->currency($user['topupamount']) ?>)</label>
				</div>
    		</div>
    		<div class="payment-detail" id="transfer" style="display: block">
    			<div class="header">
    				<?= __d("smashdeal","Transfer") ?>
    			</div>
    			<ul>
    				<li><?= __d("smashdeal","Proses Konfirmasi Manual (1x24 jam)"); ?></li>
    			</ul>
				<p><strong>BCA : 8280 780 988 A/N Barisan Usaha Media Indonesia, CV.</strong></p>
    			<p>
    				<strong><?= __d("smashdeal","Penting : ") ?></strong><br/>
    				<?= __d("smashdeal","* Apabila melakukan pembelian lebih dari satu kali mohon untuk tidak menggabungkan pembayaran tersebut") ?><br/>
    				<?= __d("smashdeal","* Transferan harus ditambahkan kode unik, transferan yang tidak ditambahkan kode unik tidak akan kami proses.") ?><br/>
    				<?= __d("smashdeal","* Proses Konfirmasi adalah 1x24 jam, untuk proses instant gunakan fitur Cash Token."); ?>
    			</p>
    		</div>
    		<div class="payment-detail" id="cashtoken" style="display:none">
    			<div class="header">
    				<?= __d("smashdeal","Cash Token") ?>
    			</div>
    			<ul>
    				<li><?= __d("smashdeal","Proses Konfirmasi Instant"); ?></li>
    				<li><?= __d("smashdeal","Bebas Biaya Proses") ?></li>
    			</ul>
    		</div>
    	</div>
    </section>
    <section class="sub-section clearfix center-align" style="border:none !important">
        <button class="btn btn-smd btn-sm-primary" type="submit">
            <?= __d("smashdeal","Lanjutkan") ?>
        </button>
    </section>
</div>
<?= $this->Form->end(); ?>