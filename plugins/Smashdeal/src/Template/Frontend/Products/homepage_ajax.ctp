<?php if(count($products)>0): ?>
    <?php  foreach($products as $product): ?>
        <div class="col-md-4 col-sm-6 col-xs-12 product-wrapper">
            <?= $this->Html->link($this->element('Smashdeal.Products/singleproductmd',['product'=>$product]),['controller'=>'Products','action'=>'view','plugin'=>'Smashdeal','prefix'=>'Frontend',$product->slug],['escape'=>false]); ?>
        </div>
    <?php endforeach; ?>
    <?php if(count($products)>0): ?>
        <div class="hide paginator">
            <?= $this->element('System.paginator') ?>
        </div>
    <?php endif; ?>
<?php endif; ?>