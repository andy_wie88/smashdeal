<div class="main-container">
    <div class="container">
        <h2 class="section-header">
            <?php if(isset($q)): ?>
                <?= __d("smashdeal","Search Product With Keyword ''{0}''",$q); ?>
            <?php elseif(isset($category)): ?>
                <?php foreach($category->langs as $lng){if($lng["languageid"]==$config_front["SITEDFLTLG_VALUE"]){$lang = $lng;break;}} ?>
                <?= __d("smashdeal","{0}",(isset($lang) && $lang["name"]!="")?$lang["name"]:$category["name"]); ?>
            <?php else: ?>
                <?= __d("smashdeal","All Products") ?>
            <?php endif; ?>
        </h2>
        <div class="row">
            <?php if(count($products)>0): ?>
                <?php  foreach($products as $product): ?>
                    <div class="col-md-4">
                        <?= $this->Html->link($this->element('Smashdeal.Products/singleproductmd',['product'=>$product]),['controller'=>'Products','action'=>'view','plugin'=>'Smashdeal','prefix'=>'Frontend',$product->slug],['escape'=>false]); ?>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-md-12 no-product">
                    <?php if(isset($q)): ?>
                        <?= __d("smashdeal","We currently has no product to display with keyword ''{0}''",$q); ?>
                    <?php elseif(isset($category)): ?>
                        <?php foreach($category->langs as $lng){if($lang["languageid"]==$config_front["SITEDFLTLG_VALUE"]){$lang = $lng;break;}} ?>
                        <?= __d("smashdeal","No Product Found at ''{0}'' Category",(isset($lang) && $lang["name"]!="")?$lang["name"]:$category["name"]); ?>
                    <?php else: ?>
                        <?= __d("smashdeal","We currently has no product to display"); ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <?php if(count($products)>0): ?>
            <?= $this->element('System.paginator') ?>
        <?php endif; ?>
    </div>
</div>