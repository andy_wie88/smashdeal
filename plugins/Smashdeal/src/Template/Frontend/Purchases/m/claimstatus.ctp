<div class="container sm-main-content">
	<h1 class="section-header"><?= __d("smashdeal", "Produk Belum diklaim") ?></h1>
	<div class="row scroll">
		<?php if(count($unclaims)>0): ?>
		    <?php foreach($unclaims as $unclaim): ?>
		        <div class="unconfirm-payment">
		        	<div class="row">
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Kode Voucher : ") ?>
		        			</span>
		        			<br>
		        			<?= $unclaim["vouchercode"] ?>
		        		</div>
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Tangal Exp.: ") ?>
		        			</span>
		        			<br>
		        			<?= $unclaim["validity"]->i18nFormat("dd/MM/yyyy") ?>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Produk : ") ?>
		        			</span>
		        			<br>
		        			<?= $unclaim["purchase"]['tb_deal_product']['productname'] ?>
		        		</div>
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Merchant : ") ?>
		        			</span>
		        			<br>
		        			<?= $unclaim["merchant"]['name'] ?>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col s12">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Sisa Pembayaran : ") ?>
		        			</span>
		        			<br>
		        			<?= $this->Number->currency($unclaim['purchase']["grandtotal"]); ?>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col s12 center-align">
		        			<?= $this->Html->link(__d("smashdeal","view"),['controller'=>'Purchases','action'=>'purchaseVoucher','plugin'=>'Smashdeal','prefix'=>'Frontend',$unclaim['id']]) ?>
		        		</div>
		        	</div>
		        </div>
		    <?php endforeach; ?>
	        <?php if(count($unclaims)>0): ?>
		        <div class="hide paginator">
		            <?= $this->element('System.paginator') ?>
		        </div>
		    <?php endif; ?>
		<?php else: ?>
		    <div class="col-md-12 no-product">
		        <?= __d("smashdeal","Anda tidak mempunyai pembelian yang belum dikonfirmasi."); ?>
		    </div>
		<?php endif; ?>
	</div>
	<?php if(count($unclaims)>0): ?>
        <div class="center-align" id="loader" style="margin-bottom: 20px;display: none">
            <div class="preloader-wrapper active">
                <div class="spinner-layer sm-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
	$(document).ready(function(e)
	{
	  $(window).scroll(function() {
	     if($(window).scrollTop()>150) {
	        if($("a.next").length>0){
	          console.log("here 2");
	          var href = $("a.next").attr("href");
	          $(".paginator").remove();
	          $("#loader").show();
	          $.get(href,function(data)
	          {
	            $(".scroll").append(data).fadeIn("slow");
	            $("#loader").hide();
	          });
	        }
	     }
	  });
	});
</script>