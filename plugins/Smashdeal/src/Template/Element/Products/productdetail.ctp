<?= $this->AssetCompress->script('Smashdeal.ProdDtl234',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('Smashdeal.ProdDtl234',['block'=>'css']) ?>
<div class="box box-default">
    <div class="box-header">
        <h1 class="box-title"><?= __d("smashdeal","Product Details") ?></h1>
    </div>
    <div class="box-body">
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label for="retailprice" class="control-label col-md-4"><?= __d("smashdeal","Retail Price") ?></label>
                    <div class="col-md-8">
                        <?= $this->Form->input('retailprice',['id'=>'retailprice','label'=>false,'type'=>'hidden']) ?>
                        <input type="text" data-target="#retailprice" class="form-control money" value="<?= (isset($bean->retailprice))?$bean->retailprice:0; ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label for="bargainstart" class="control-label col-md-4"><?= __d("smashdeal","Min. Price") ?></label>
                    <div class="col-md-8">
                        <?= $this->Form->input('bargainstart',['id'=>'bargainstart','label'=>false,'type'=>'hidden']) ?>
                        <input type="text" data-target="#bargainstart" class="form-control money" value="<?= (isset($bean->bargainstart))?$bean->bargainstart:0; ?>"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="bargainend" class="control-label col-md-4"><?= __d("smashdeal","Max. Price") ?></label>
                    <div class="col-md-8">
                        <?= $this->Form->input('bargainend',['id'=>'bargainend','label'=>false,'type'=>'hidden']) ?>
                        <input type="text" data-target="#bargainend" class="form-control money" value="<?= (isset($bean->bargainend))?$bean->bargainend:0; ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label for="dealend" class="control-label col-md-4"><?= __d("smashdeal","Increment Value") ?></label>
                    <div class="col-md-8">
                        <?= $this->Form->input('incrementvalue',['id'=>'incrementvalue','type'=>'hidden']) ?>
                        <input type="text" data-target="#incrementvalue" class="form-control money" value="<?= (isset($bean->incrementvalue))?$bean->incrementvalue:0; ?>"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="retailprice" class="control-label col-md-4"><?= __d("smashdeal","Bargain Token") ?></label>
                    <div class="col-md-8">
                        <?= $this->Form->input('bargaintoken',['class'=>'form-control','min'=>1,'max'=>1000000,'label'=>false]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label for="dealend" class="control-label col-md-4"><?= __d("smashdeal","Merchant") ?></label>
                    <div class="col-md-8">
                        <?= $this->Form->input('merchantid',['class'=>'form-control','options'=>$merchants,'label'=>false,'empty'=>__d("smashdeal","--Choose Merchant--"),'required'=>true]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>