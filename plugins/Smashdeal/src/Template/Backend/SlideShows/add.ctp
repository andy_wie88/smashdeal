<?= $this->AssetCompress->script('System.FeatureImage',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.FeatureImage',['block'=>'css']) ?>
<?= $this->Html->script('Smashdeal.slideshows/selectimage',['block'=>'footer-script']) ?>
<?= $this->Form->create($slideshow,['class'=>'form-horizontal']) ?>
<div class="toolbar">
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-save"></i>
    </button>
</div>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal", "Add Slide Show Image") ?></h2>
    </div>
    <div class="box-body">
        <div id="get-image-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'getMedia','prefix'=>'Ajax','plugin'=>'System']); ?>"></div>
        <input type="hidden" name="mediaid" id="feature-image" value="<?= $slideshow['mediaid'] ?>" />
        <div class="form-group">
            <label for="" class="col-md-2 control-label"><?= __d("smashdeal","Image") ?></label>
            <div class="col-md-2">
                <input type="text" readonly class="form-control" id="images_label" value="<?= ($slideshow['mediaid']!=null)?"Selected":"" ?>" required />
            </div>
            <div class="col-md-2 no-padding-left">
                <div>
                    <div id="media-list-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaList','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
                    <div id="media-upload-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'upload','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
                    <div id="media-attribute-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaAttribute','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
                    <a href="#" id="media-selector"><?= __d("system","select image"); ?></a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-push-2" id="image-wrapper">
                <?php
                    if($slideshow['mediaid']!=""){
                        echo $this->cell('System.MediaRenderer',['mediaId'=>$slideshow['mediaid']]);
                    }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-2 control-label"><?= __d("smashdeal","Link") ?></label>
            <div class="col-md-8">
                <?= $this->Form->input('link',['class'=>'form-control','placeholder'=>'Hyperlink...','label'=>false]) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-2 control-label"><?= __d("smashdeal","Description") ?></label>
            <div class="col-md-8">
                <?= $this->Form->input('description',['class'=>'form-control','placeholder'=>'Description...','label'=>false]) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-2 control-label"><?= __d("smashdeal","Index") ?></label>
            <div class="col-md-2">
                <?= $this->Form->input('index',['class'=>'form-control','placeholder'=>'index','label'=>false]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2">

            </div>
            <div class="col-md-4">
                <input type="checkbox" name="isactive"><label for="" class="control-label"><?= __d("smashdeal","Active") ?></label>
            </div>
        </div>
    </div>
</div>
<div class="toolbar">
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-save"></i>
    </button>
</div>
<?= $this->Form->end() ?>