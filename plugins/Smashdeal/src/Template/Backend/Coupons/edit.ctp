<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal", "Generate Coupon") ?></h2>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label for="Token Amount" class="col-md-3 control-label"><?= __d("smashdeal","Token Amount") ?></label>
            <div class="col-md-4">
                <?= $this->Form->input('tokenproductid',['label'=>false,'div'=>false,'class'=>'form-control','options'=>$tokenproducts]) ?>
            </div>
        </div>
        <div class="form-group"></div>
    </div>
</div>