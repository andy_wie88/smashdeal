<?= $this->AssetCompress->script('Smashdeal.Token234',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('Smashdeal.Token234',['block'=>'css']) ?>
<?= $this->Form->create($token,['class'=>'form-horizontal']) ?>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
    <div class="box box-default">
        <div class="box-header">
            <h2 class="box-title"><?= __d("smashdeal", "Add Token Product") ?></h2>
        </div>
        <div class="box-body">
            <div class="form-group"><label for="TokenAmount" class="col-md-3 control-label"><?= __d("smashdeal","Token Amount") ?></label>
                <div class="col-md-4">
                    <?= $this->Form->input('tokenamount',['div'=>false,'label'=>false,'class'=>'form-control','value'=>(isset($token->tokenamount))?$token->tokenamount:0,'min'=>0,'max'=>100000]) ?>
                </div>
            </div>
            <div class="form-group"><label for="Price" class="col-md-3 control-label"><?= __d("smashdeal","Price") ?></label>
                <div class="col-md-4">
                    <?= $this->Form->input('price',['div'=>false,'label'=>false,'id'=>'token-price','class'=>'hidden','value'=>(isset($token->price)?$token->price:0),'type'=>'text']) ?>
                    <input type="text" class="form-control money" data-target="#token-price" value="<?= (isset($token->price)?$token->price:0) ?>"/>
                </div>
            </div>
            <div class="form-group"><label for="Discount" class="col-md-3 control-label"><?= __d("smashdeal","Discount") ?></label>
                <div class="col-md-2">
                    <?= $this->Form->input('discount',['div'=>false,'label'=>false,'class'=>'form-control','value'=>(isset($token->discount))?$token->discount:0,'min'=>0,'max'=>100]) ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3"></div>
                <div class="col-md-4">
                    <label>
                        <?= $this->Form->input('isactive',['div'=>false,'label'=>false]) ?>
                    </label>
                    <label><?= __d('system',"Active") ?></label>
                </div>
            </div>
        </div>
    </div>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
<?= $this->Form->end(); ?>