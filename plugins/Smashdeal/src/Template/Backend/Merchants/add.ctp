<?= $this->AssetCompress->css('System.Use55dds',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Use55dds',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->script('System.FeatureImage',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.FeatureImage',['block'=>'css']) ?>
<?= $this->Html->script('Smashdeal.merchants/add',['block'=>'footer-script']) ?>
<?= $this->Form->create($merchant,['class'=>'form-horizontal']); ?>
    <div class="box box-default">
        <div class="box-header">
            <h2 class="box-title"><?= __d("smashdeal", "Add Merchants") ?></h2>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="" class="col-md-3 control-label"><?= __d("smashdeal", "Name") ?></label>
                <div class="col-md-6">
                    <?= $this->Form->input('name',['class'=>'form-control','label'=>false]) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-md-3 control-label"><?= __d("smashdeal", "Address") ?></label>
                <div class="col-md-6">
                    <?= $this->Form->input('address',['class'=>'form-control','label'=>false]) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-md-3 control-label"><?= __d("smashdeal", "Phone No.") ?></label>
                <div class="col-md-6">
                    <?= $this->Form->input('phonenoone',['class'=>'form-control','label'=>false,'required'=>true]) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-md-3 control-label"><?= __d("smashdeal", "Phone No. ") ?></label>
                <div class="col-md-6">
                    <?= $this->Form->input('phonenotwo',['class'=>'form-control','label'=>false]) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-md-3 control-label"><?= __d("smashdeal", "email") ?></label>
                <div class="col-md-6">
                    <?= $this->Form->input('email',['class'=>'form-control','label'=>false]) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-md-3 control-label"><?= __d("smashdeal", "username") ?></label>
                <div class="col-md-6">
                    <?= $this->Form->input('username',['class'=>'form-control','label'=>false]) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-md-3 control-label"><?= __d("smashdeal", "Logo") ?></label>
                <div class="col-md-6">
                    <?= $this->Form->input('merchantlogo',['id'=>'merchantlogo','class'=>'form-control','label'=>false,'type'=>'hidden']) ?>
                    <div id="image-wrapper" class="col-md-3">
                        <?= (isset($merchant->merchantlogo))?$this->cell('System.MediaRenderer',['mediaid'=>$merchant->merchantlogo]):"" ?>
                    </div>
                    <div style="clear:both">
                        <div id="get-image-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'getMedia','prefix'=>'Ajax','plugin'=>'System']); ?>"></div>
                        <div id="media-list-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaList','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
                        <div id="media-upload-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'upload','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
                        <div id="media-attribute-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaAttribute','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
                        <a href="#" id="media-selector"><?= __d("system","set merchant logo"); ?></a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <label>
                        <?= $this->Form->input('isactive',['div'=>false,'label'=>false]) ?>
                    </label>
                    <label><?= __d('system',"Active") ?></label>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary" type="submit"><?= __d("system","save"); ?></button>
<?= $this->Form->end(); ?>