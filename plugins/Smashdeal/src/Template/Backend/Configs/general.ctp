<?= $this->AssetCompress->script('Smashdeal.generalconfig',['block'=>'footer-script']) ?>
<?= $this->Form->create($generalConfig,['class'=>'form-horizontal','type'=>'file']); ?>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h2 class="box-title"><?= __d("system","General Settings") ?></h2>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="sitetitle" class="col-md-3 control-label">
                            <?= __d("system","Safety Margin") ?>
                        </label>
                        <div class="col-md-5">
                            <?= $this->Form->input('SAFETY_MARGIN.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('SAFETY_MARGIN.value',['id'=>'safetymargin','type'=>'number','class'=>'form-control','placeholder'=>'single token price','label'=>false,'div'=>false]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sitetitle" class="col-md-3 control-label">
                            <?= __d("system","Single Token Price") ?>
                        </label>
                        <div class="col-md-5">
                            <?= $this->Form->input('SINGLETOKENPRICE.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('SINGLETOKENPRICE.value',['id'=>'singleprice','type'=>'hidden','class'=>'form-control','placeholder'=>'single token price','label'=>false,'div'=>false]) ?>
                            <input type="text" value="<?= (isset($this->request->data["SINGLETOKENPRICE"]["value"])?$this->request->data["SINGLETOKENPRICE"]["value"]:0) ?>" class="money form-control" data-target="#singleprice"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sitetagline" class="col-md-3 control-label">
                            <?= __d("system","Multiple Host's Share Percentage"); ?>
                        </label>
                        <div class="col-md-2">
                            <?= $this->Form->input('MULTIPLEHOSTPRC.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('MULTIPLEHOSTPRC.value',['class'=>'form-control','id'=>'multiplehostprc','placeholder'=>'multiple host percentage','label'=>false,'div'=>false,'type'=>'number','min'=>0,'max'=>100]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
<?= $this->Form->end(); ?>