<div class="overflow-table-container">
	<table class="table table-sm table-bordered table-stripe">
	    <thead>
		    <tr>
		        <th width="3%" class="text-center"><?= __d("smashdeal","No. ") ?></th>
		        <th width="30%" class="text-center"><?= __d("smashdeal","Bargain Time") ?></th>
		        <th><?= __d("smashdeal","Username") ?></th>
		    </tr>
	    </thead>
	    <tbody>
			<?php foreach($guests as $key=>$guest): ?>
				<tr>
					<td><?= $key+1 ?></td>
					<td><?= $guest['bargaintime']->i18nFormat("dd/MM/yyyy HH:mm:ss"); ?></td>
					<td class="text-center"><?= $guest['user']['username']; ?></td>
				</tr>
			<?php endforeach; ?>
	    </tbody>
	</table>
</div>