<?php
	use Cake\I18n\I18n;

	I18n::locale('id_ID');
?>
<div style="max-width: 900px; margin:auto;width: 800px;">
    <div style="content: '';display:table; clear: both;width:100%">
    	<div style="background-color:#fff;padding:20px;">
    		<h1 style="text-align:center;padding:15px;color:#01688c;font-size: 1.2em;">
    			Selamat Anda Pemenang <br> Product <br><?= $product['productname'] ?>
    		</h1>
    		<div style="width: 100%;border-top:1px solid #ddd;padding-bottom: 10px;"></div>
    		<p style="text-align: center">Berikut ini ada kode voucher anda : </p>
    		<div style="padding:20px; font-size: 16px; text-transform: uppercase; color:#01688c; font-weight: bold; border:4px dashed #01688c;text-align: center">
				<?= $voucher['vouchercode']; ?>
			</div>
            <p style="text-align: center;">Segera Tukarkan Voucher ini ke merchant kami yang terterah dibawah ini : </p>
            <div style="max-width: 400px; margin:0px auto;width: 400px;padding-bottom: 10px">
                <table style="border-collapse:collapse;border:1px solid #ddd;width:400px">
                    <tr>
                        <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Nama Merchant</td>
                        <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $merchant['name'] ?></td>
                    </tr>
                    <tr>
                        <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Alamat</td>
                        <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $merchant['address'] ?></td>
                    </tr>
                    <tr>
                        <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Telephone</td>
                        <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $merchant['phonenoone'] ?></td>
                    </tr>
                    <tr>
                        <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Telephone</td>
                        <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $merchant['phonenotwo'] ?></td>
                    </tr>
                </table>
            </div>
            <div style="width: 100%;border-top:1px solid #ddd;padding-bottom: 10px;"></div>
            <div style="background-color:#fffcbf;padding:15px;">
                <h1 style="font-size: 1.2em;">
                    Prosedur Klaim Produk
                </h1>
                <p>
                    Berikut ini adalah prosedur dalam melakukan klaim produk : 
                    <ol>
                        <li>Sediakan dana pribadi sebesar nilai bargain yang anda menangkan.</li>
                        <li>Kunjungi pihak Merchant sesuai dengan alamat yang terterah diatas</li>
                        <li>Lampirkan <strong>Kode Voucher</strong> dan <strong>KTP Asli</strong></li>
                        <li>Pihak Merchant akan menginput <strong>Kode Voucher</strong> pada panel Merchant Dealsmash. Pada saat Merchant selesai menginput <strong>Kode Voucher</strong> Email Verifikasi akan dikirimkan ke email anda.</li>
                        <li>Tunjukkan kode verifikasi kepada pihak Merchant untuk melakukan invalidasi voucher.</li>
                        <li>Lakukan pembayaran dan proses terima produk anda.</li>
                    </ol>
                </p>
                <p>
                    <strong>Penting : </strong>
                    <ol>
                        <li>
                            Voucher ini hanya berlaku sampai dengan tanggal <?= $voucher['validity']->i18nFormat("dd/MM/yyyy") ?>. Voucher ini tidak dapat divalidasi lagi setelah melewati tanggal  tersebut.  
                        </li>
                        <li>
                            Harap Membawa <strong>KTP Asli</strong> sesuai dengan nama dan No. KTP yang terterah di user.
                        </li>
                    </ol>
                </p>
            </div>
            <p>
                Salam,<br/>
                <span style="font-weight: bold;color:#01688c">Team Dealsmash</span>
            </p>
    	</div>
	</div>
</div>