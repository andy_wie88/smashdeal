<?php
    if($product->bargainquota > $product->bargaintime):
?>
<?= $this->AssetCompress->script('Smashdeal.insertBargain') ?>
<?= $this->Form->create($bargain,['url'=>['action'=>'doBargain','controller'=>'Bargains','prefix'=>'Frontend','plugin'=>'Smashdeal'],'class'=>'form-horizontal']) ?>
    <div class="form-wrap">
        <div class="row">
            <div class="col s12 no-padding">
                <input type="hidden" name="bargainid" value="<?= $bargainid ?>"/>
                <input type="hidden" name="price" id="price" />
                <input type="text" data-target="#price" class="form-control money sm-form-control" value="0"/>
            </div>
            <div class="col s12 no-padding center-align">
                <button class="btn btn-smd btn-sm-primary"><?= __d("smashdeal","Bargain"); ?></button>
            </div>
        </div>
    </div>
<?= $this->Form->end(); ?>
<?php endif; ?>
