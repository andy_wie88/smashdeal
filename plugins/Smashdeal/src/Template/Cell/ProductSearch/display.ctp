<?= $this->Form->create($product,['url'=>['controller'=>'Products','action'=>'search'],'type'=>'get']) ?>
<div class="search-wrapper">
    <div class="search-form-wrapper">
        <input type="text" name="q" placeholder="<?= __d("smashdeal","Find what deal today?") ?>" value="<?= (isset($q))?$q:"" ?>" />
        <button type="submit" class="main-search-btn">
            <i class="fa fa-search"></i>
        </button>
    </div>
</div>
<?= $this->Form->end(); ?>