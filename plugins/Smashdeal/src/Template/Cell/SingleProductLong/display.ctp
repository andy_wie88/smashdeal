<div class="sm-vertical-bargain-box">
    <div class="row">
        <div class="col-md-5 img-wrapper">
            <div class="ribbon-two">
                Hemat <?= round((($product['retailprice']-$product['bargainstart'])/$product['retailprice']) * 100) ?>%
            </div>
            <?php
            if(isset($product->medias)){
                foreach($product->medias as $media)
                {
                    if($media["isdefault"])
                    {
                        $image = $media["mediaid"];
                        break;
                    }
                }
                if(!isset($image) && count($product->medias)>0)
                {
                    $image = $bean->medias[0]["mediaid"];
                }
            }
            ?>
            <?php if(isset($image)): ?>
                <?= $this->cell('System.MediaRenderer',['mediaId'=>$image]); ?>
            <?php else: ?>
                <?= $this->Html->image('no_image.png') ?>
            <?php endif; ?>
        </div>
        <?php $language = []; ?>
        <?php foreach($product->langs as $lng){
            if($lng["languageid"]==$config_front["SITEDFLTLG_VALUE"])
            {
                $language = $lng;
                break;
            }
        } ?>
        <div class="col-md-7 info-wrapper">
            <h2 class="bargain-title">
                <?= (isset($language["name"])&&$language["name"]!="")?$language["name"]:$product["productname"]; ?>
            </h2>
            <div class="line sm-second-line clearfix">
                <div class="col-md-3 col-sm-3 col-xs-3 no-padding">
                            <span class="merchant-name">
                                <i class="fa fa-map-marker"></i>
                                <?= $product->merchant["name"] ?>
                            </span>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 text-right sm-bargain-retail-price">
                    <span class="extra-label"><?= __d("smashdeal","Retail Price : "); ?></span>
                    <?= $this->Number->currency($product->retailprice); ?>
                </div>
            </div>
            <div class="line sm-third-line clearfix">
                <div class="col-md-3 col-sm-3 col-xs-3 no-padding sm-prd-token-amount">
                    <?= $this->Number->format($product->bargaintoken); ?><br/>
                    <?= ($product->bargaintoken>=2)?__d("smashdeal","Tokens"):__d("smashdeal","Token"); ?>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9">
                    <div class="sm-bargain-detail-price-range">
                        <?= $this->Number->currency($product->bargainstart) ?>-<?= $this->Number->currency($product->bargainend); ?>
                        <br/><?= __d("smashdeal","Multiply : ");?><?= $this->Number->currency($product->incrementvalue) ?></div>
                </div>
            </div>
            <div class="line sm-fifth-line clearfix">
                <?= $this->cell('Smashdeal.Remaintime::bar',['bargainquota'=>$product['bargainquota'],'bargaintimes'=>$product->bargaintimes]) ?>
            </div>
        </div>
    </div>
</div>