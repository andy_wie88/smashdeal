<div class="selection-link my-bargain">
    <div class="hand-shake-wrapper">
        <a href="<?= $this->Url->build(['controller'=>'Bargains','action'=>'ongoing','prefix'=>'Frontend','plugin'=>'Smashdeal']) ?>">
            <span class="icon sm-bargain"></span>
            <span class="bargain-label"><?= __d("smashdeal","My Bargain") ?></span>
        </a>
    </div>
</div>