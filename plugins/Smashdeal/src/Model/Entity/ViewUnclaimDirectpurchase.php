<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewUnclaimDirectpurchase Entity
 *
 * @property string $id
 * @property string $claimid
 * @property int $claimtype
 * @property string $vouchercode
 * @property \Cake\I18n\Time $validity
 * @property bool $isvalid
 * @property string $userid
 * @property string $merchantid
 * @property \Cake\I18n\Time $claimat
 * @property string $claimby
 * @property string $idno
 * @property string $name
 * @property string $address
 * @property string $phoneno
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class ViewUnclaimDirectpurchase extends Entity
{

}
