<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealClaimProduct Entity
 *
 * @property string $id
 * @property string $vouchercode
 * @property string $claimid
 * @property string $userid
 * @property string $merchantid
 * @property int $claimtype
 * @property bool $isvalid
 * @property \Cake\I18n\Time $claimat
 * @property string $claimby
 * @property string $username
 * @property string $idno
 * @property string $address
 * @property \Cake\I18n\Time $birthday
 * @property string $handphoneno
 * @property string $phoneno
 * @property string $email
 * @property string $name
 */
class ViewDealClaimProduct extends Entity
{

}
