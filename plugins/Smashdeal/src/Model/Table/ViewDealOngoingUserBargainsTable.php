<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealOngoingUserBargains Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealOngoingUserBargain get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealOngoingUserBargain newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealOngoingUserBargain[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealOngoingUserBargain|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealOngoingUserBargain patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealOngoingUserBargain[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealOngoingUserBargain findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ViewDealOngoingUserBargainsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_ongoing_user_bargains');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('id');

        $validator
            ->date('date')
            ->allowEmpty('date');

        $validator
            ->allowEmpty('productname');

        $validator
            ->allowEmpty('slug');

        $validator
            ->allowEmpty('description');

        $validator
            ->dateTime('dealend')
            ->allowEmpty('dealend');

        $validator
            ->decimal('retailprice')
            ->allowEmpty('retailprice');

        $validator
            ->decimal('bargainstart')
            ->allowEmpty('bargainstart');

        $validator
            ->decimal('bargainend')
            ->allowEmpty('bargainend');

        $validator
            ->decimal('incrementvalue')
            ->allowEmpty('incrementvalue');

        $validator
            ->allowEmpty('merchantid');

        $validator
            ->integer('bargaintoken')
            ->allowEmpty('bargaintoken');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->integer('publishstatus')
            ->allowEmpty('publishstatus');

        $validator
            ->integer('visibility')
            ->allowEmpty('visibility');

        $validator
            ->allowEmpty('postpassword');

        $validator
            ->allowEmpty('permalink');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findUser(Query $query, array $options)
    {
        $user = $options["userid"];

        $query->where(['userid'=>$user]);

        return $query;
    }
}
