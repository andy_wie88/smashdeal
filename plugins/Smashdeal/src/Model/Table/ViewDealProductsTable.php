<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\i18n\Date;

/**
 * ViewDealProducts Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealProduct get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProduct newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProduct[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProduct findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ViewDealProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_products');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Metas',['className'=>'Smashdeal.TbDealProductMetadatas','foreignKey'=>'productid']);
        $this->hasMany('Langs',['className'=>'Smashdeal.TbDealProductLangs','foreignKey'=>'productid']);
        $this->hasMany('Medias',['className'=>'Smashdeal.TbDealProductMedias','foreignKey'=>'productid']);
        $this->belongsToMany('Terms',[
            'className'=>'System.TbSysTerms',
            'joinTable'=>'tb_sys_term_relationships',
            'foreignKey'=>'objectid',
            'targetForeignKey'=>'termid',
        ]);
        $this->belongsTo('Merchants',['className'=>'Smashdeal.TbDealMerchants','foreignKey'=>'merchantid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        $validator
            ->date('date')
            ->allowEmpty('date');

        $validator
            ->allowEmpty('productname');

        $validator
            ->allowEmpty('slug');

        $validator
            ->allowEmpty('description');

        $validator
            ->dateTime('dealend')
            ->allowEmpty('dealend');

        $validator
            ->decimal('retailprice')
            ->allowEmpty('retailprice');

        $validator
            ->decimal('bargainstart')
            ->allowEmpty('bargainstart');

        $validator
            ->decimal('bargainend')
            ->allowEmpty('bargainend');

        $validator
            ->decimal('incrementvalue')
            ->allowEmpty('incrementvalue');

        $validator
            ->allowEmpty('merchantid');

        $validator
            ->integer('bargaintoken')
            ->allowEmpty('bargaintoken');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->integer('publishstatus')
            ->allowEmpty('publishstatus');

        $validator
            ->integer('visibility')
            ->allowEmpty('visibility');

        $validator
            ->allowEmpty('postpassword');

        $validator
            ->allowEmpty('permalink');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        $validator
            ->boolean('isclosed')
            ->allowEmpty('isclosed');

        $validator
            ->allowEmpty('closedby');

        $validator
            ->dateTime('closed')
            ->allowEmpty('closed');

        $validator
            ->allowEmpty('closedhistoryid');

        $validator
            ->allowEmpty('product_category');

        return $validator;
    }

    public function findDisplay($query, array $options)
    {
        $date = new Date();
        if(isset($options["category"]))
            $category = $options["category"];
        $query->contain(['Langs','Metas','Medias','Terms','Merchants']);
        if(isset($category))
        {
            $query->innerJoinWith('Terms', function($q) use ($category){
                return $q->where(['Terms.id'=>$category,'Terms.taxonomy'=>"product_category"]);
            });
        }
        $query->where(['and'=>['publishstatus'=>3,'date <='=>$date]]);
        return $query;
    }

    public function findCategories($query, array $options)
    {
        $date = new Date();
        if(isset($options["categories"]))
            $categories = $options["categories"];
        $query->contain(['Langs','Metas','Medias','Terms','Merchants']);
        
        if(isset($categories))
        {
            foreach($categories as $category){
                $query->orWhere(['ViewDealProducts.product_category LIKE '=>'%'.$category['id'].'%']);
            }
        }

        $query->where(['and'=>['publishstatus'=>3,'date <='=>$date]]);
        return $query;
    }

    public function findEnd($query, array $options)
    {
        $query->contain(['Langs','Metas','Medias','Terms']);
        $query->where(['bargaintimes >= bargainquota']);
        return $query;
    }
}
