<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealClaimProduct Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealClaimProduct get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealClaimProduct newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealClaimProduct[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealClaimProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealClaimProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealClaimProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealClaimProduct findOrCreate($search, callable $callback = null, $options = [])
 */
class ViewDealClaimProductTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_claim_product');
        $this->displayField('name');
        $this->primaryKey('id');


        $this->addBehavior('Timestamp');


        $this->belongsTo('Merchants',['className'=>'Smashdeal.TbDealMerchants','foreignKey'=>'merchantid']);
        $this->belongsTo('Purchases',['className'=>'Smashdeal.TbDealDirectPurchases','foreignKey'=>'claimid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('vouchercode');

        $validator
            ->allowEmpty('claimid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('merchantid');

        $validator
            ->integer('claimtype')
            ->allowEmpty('claimtype');

        $validator
            ->boolean('isvalid')
            ->allowEmpty('isvalid');

        $validator
            ->dateTime('claimat')
            ->allowEmpty('claimat');

        $validator
            ->allowEmpty('claimby');

        $validator
            ->allowEmpty('username');

        $validator
            ->allowEmpty('idno');

        $validator
            ->allowEmpty('address');

        $validator
            ->date('birthday')
            ->allowEmpty('birthday');

        $validator
            ->allowEmpty('handphoneno');

        $validator
            ->allowEmpty('phoneno');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    public function findUser(Query $query, array $options)
    {
        $user = $options["userid"];
        
        $query->contain(['Merchants','Purchases'=>function($q){return $q->contain(['TbDealProducts']);}]);
        $query->where(['ViewUnclaimDirectpurchase.userid'=>$user]);

        return $query;
    }
}
