<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealBargainTokenJournal Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealBargainTokenJournal get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealBargainTokenJournal newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealBargainTokenJournal[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealBargainTokenJournal|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealBargainTokenJournal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealBargainTokenJournal[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealBargainTokenJournal findOrCreate($search, callable $callback = null)
 */
class ViewDealBargainTokenJournalTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_bargain_token_journal');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('userid');

        $validator
            ->dateTime('exttime')
            ->allowEmpty('exttime');

        $validator
            ->allowEmpty('note');

        $validator
            ->integer('debit')
            ->allowEmpty('debit');

        $validator
            ->integer('credit')
            ->allowEmpty('credit');

        return $validator;
    }

    public function findUser($query,array $options)
    {
        $userid = strtolower($options['userid']);
        $query->where(['userid'=>$userid]);
        $query->order(['exttime'=>"DESC"]);
        return $query;
    }
}
