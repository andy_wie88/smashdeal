<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealBankAccounts Model
 *
 * @method \Smashdeal\Model\Entity\TbDealBankAccount get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBankAccount newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBankAccount[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBankAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBankAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBankAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBankAccount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealBankAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_bank_accounts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('bank');

        $validator
            ->allowEmpty('accountno');

        $validator
            ->allowEmpty('accountname');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
