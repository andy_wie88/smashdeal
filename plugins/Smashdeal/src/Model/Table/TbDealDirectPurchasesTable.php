<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealDirectPurchases Model
 *
 * @method \Smashdeal\Model\Entity\TbDealDirectPurchase get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealDirectPurchase newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealDirectPurchase[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealDirectPurchase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealDirectPurchase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealDirectPurchase[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealDirectPurchase findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealDirectPurchasesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_direct_purchases');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Smashdeal.TbDealProducts',['foreignKey'=>'productid']);
        $this->belongsTo('Smashdeal.TbDealUsers',['foreignKey'=>'userid']);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('productid', 'create')
            ->notEmpty('productid');

        $validator
            ->requirePresence('userid', 'create')
            ->notEmpty('userid');

        $validator
            ->integer('amount')
            ->allowEmpty('amount');

        $validator
            ->decimal('cashtoken')
            ->requirePresence('cashtoken', 'create')
            ->notEmpty('cashtoken');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->dateTime('modifiedby')
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
