<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;
use System\Controller\Component\TermsComponent;

/**
 * MainNavigation cell
 */
class MainNavigationCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($config)
    {
        $this->loadModel('System.TbSysTerms');
        $tempcategories = $this->TbSysTerms->find('all',['contain'=>'TermLangs'])->where(['taxonomy'=>'product_category']);
        $term = new TermsComponent();
        $categories = $term->collectCategoryNested($tempcategories->toArray());
        $this->set('categories',$categories);
        $this->set('config',$config);
    }
}
