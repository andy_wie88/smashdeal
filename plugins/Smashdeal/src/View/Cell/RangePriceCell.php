<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;
use Cake\Log\Log;

/**
 * RangePrice cell
 */
class RangePriceCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($startprice,$endprice,$incrementvalue,$price)
    {
        Log::error("-======================----===================-");
        Log::error("Price = ".$price);
        $selisi = $endprice - $startprice;
        Log::error("Selisi = ".$selisi);
        $times = $selisi/$incrementvalue;
        Log::error("Times = ".$times);
        $kelas = ceil($times*0.05);
        Log::error("Kelas = ".$kelas);
        $range = ceil($times/$kelas);
        Log::error("Range = ".$range);
        $before = $startprice;
        for($i=0;$i<$kelas;$i++)
        {
            Log::error("Loop ".($i+1));
            $last = $before + ($incrementvalue*$range);
            Log::error("Before Loop = ".$before);
            Log::error("After Loop = ".$last);
            if($price <= $last && $price >= $before)
            {
                Log::error('Before = '.$before);
                Log::error('After = '.$last);
                $this->set('before',$before);
                $this->set('after',$last);
                break;
            }
            $before = $last;
        }
    }
}
