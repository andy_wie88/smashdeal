<?php
namespace Smashdeal\Controller\Ajax;

use System\Controller\BackendAppController as AppController;

use Cake\i18n\Time;

/**
 * Vouchers Controller
 *
 * @property \Smashdeal\Model\Table\VouchersTable $Vouchers
 */
class VouchersController extends AppController
{

    public function approval($confirmationid, $status)
    {
 		$this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;

        $user = $this->Auth->user('id');
        $date = new Time();

        $this->loadModel('Smashdeal.TbDealBtPaymentConfirmations');
        $this->loadModel('Smashdeal.TbDealBtPurchases');
        $this->loadModel('Smashdeal.TbDealUsers');
        $confirmation = $this->TbDealBtPaymentConfirmations->get($confirmationid);
        $confirmation->status = $status;
        $confirmation->processby = $user;

        if(isset($this->request->data['note'])){
            $confirmation->appnote = $this->request->data['note'];
        }
        else
        {
            $confirmation->appnote = "-";
        }
        $confirmation->processed = $date;

        if($this->TbDealBtPaymentConfirmations->save($confirmation))
        {
        	$purchase = $this->TbDealBtPurchases->get($confirmation['purchaseid']);
        	$user = $this->TbDealUsers->get($purchase['userid']);
        	$user->tokenamount += $purchase['tokenamount'];
        	$this->TbDealUsers->save($user);
            if($status==2)
            {
                $type = "Approve";
            }
            else if($status == 4)
            {
                $type = "Reject";
            }
            echo json_encode(['status'=>true,'type'=>$type]);
        }
        else{
            echo json_encode(['status'=>false]);
        }
    }

    
}