<?php
namespace Smashdeal\Controller\Ajax;

use System\Controller\FrontAppController as AppController;

/**
 * Bargains Controller
 *
 * @property \Smashdeal\Model\Table\BargainsTable $Bargains
 */
class BargainsController extends AppController
{

    public function getGuestHostOf($bargainid)
    {
        $this->viewBuilder()->autoLayout(false);
        $this->loadModel('Smashdeal.TbDealUserProducts');
        $bargain = $this->TbDealUserProducts->get($bargainid);
        $guests = $this->TbDealUserProducts->find('all')->contain('Users')->where(['TbDealUserProducts.id !='=>$bargainid,'TbDealUserProducts.productid'=>$bargain['productid'],'price'=>$bargain['price']])->order(['TbDealUserProducts.bargaintime'=>'ASC']);

        $this->set('guests',$guests);
    }
  
}