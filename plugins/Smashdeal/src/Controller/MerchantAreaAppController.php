<?php
namespace Smashdeal\Controller;

use System\Controller\FrontAppController as AppController;

/**
 * MerchantAreaApp Controller
 *
 * @property \Smashdeal\Model\Table\MerchantAreaAppTable $MerchantAreaApp
 */
class MerchantAreaAppController extends AppController
{
    public $helpers = [
        'Paginator' => ['templates'=>'Smashdeal.paginator'],
        'Url'
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Auth');
        $this->Auth->__set('sessionKey', 'Merchant.User');
        $this->Auth->config([
            'loginAction'=>[
                'controller'=>'Users',
                'action'=>'login',
                'plugin'=>'Smashdeal',
                'prefix'=>'Merchant'
            ],
            'authenticate'=>[
                'Form' => [
                    'userModel'=>'Smashdeal.TbDealMerchants',
                    'finder'=>'Auth',
                    'fields'=>['username'=>'username','password'=>'password']
                ]
            ],
            'authorize'=>[
                'System.Idea'
            ],
            'authError'=>'Silahkan Login Untuk Melanjutkan'
        ]);
    }
    
}