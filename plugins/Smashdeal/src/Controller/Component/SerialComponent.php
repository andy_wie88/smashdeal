<?php
namespace Smashdeal\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\i18n\Date;

/**
 * Serial component
 */
class SerialComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function generate($name)
    {
    	$this->serial = TableRegistry::get('System.TbSysSerials');
    	$serialNumber = $this->serial->find('all')->where(['name'=>$name])->first();
    	if($serialNumber==null)
    		return "";
    	$date = new Date();
    	$number = $serialNumber['serial'];
    	
    	if($serialNumber['autoreset']==1)
    	{
    		$serialNumber->lastfetchpath = $date->day;
    		if($serialNumber['lastfetchpath'] != $date->day)
    		{
    			$number = 0;
    		}
    	}
    	else if($serialNumber['autoreset']==2)
    	{
    		$serialNumber->lastfetchpath = $date->month;
    		if($serialNumber['lastfetchpath'] != $date->month)
    		{
    			$number = 0;
    		}
    	}
		else if($serialNumber['autoreset']==3)
    	{
    		$serialNumber->lastfetchpath = $date->year;
    		if($serialNumber['lastfetchpath'] != $date->year)
    		{
    			$number = 0;
    		}
    	}

    	$number+=1;
    	$serialNumber->serial = $number;

    	$result = $serialNumber->pattern;
    	$result = str_replace("DD", str_pad($date->day,2,0,STR_PAD_LEFT), $result);
    	$result = str_replace("MM", str_pad($date->month,2,0,STR_PAD_LEFT), $result);
    	$result = str_replace("YY", str_pad($date->i18nFormat('YY'),2,0,STR_PAD_LEFT), $result);

    	$result = $result.str_pad($number,$serialNumber['number'],"0",STR_PAD_LEFT);
    	$serialNumber->lastgenerated = $result;

    	$this->serial->save($serialNumber);

    	return $result;
    }

}