<?php
namespace Smashdeal\Controller\Merchant;

use Smashdeal\Controller\MerchantAreaAppController as AppController;
use Cake\Mailer\Email;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \Smashdeal\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Smashdeal.TbDealMerchants');
        $this->Auth->allow(['login','logout','forgotPassword']);
    }

    public function login()
    {
        $user = $this->TbDealMerchants->newEntity();
        if($this->request->is(["post","put","patch"]))
        {
            $user = $this->Auth->identify();
            if($user)
            {
                $this->Auth->setUser($user);
                return $this->redirect(['controller'=>'Merchants','action'=>'productClaim']);
            }
            else{
                $this->Flash->error(__('Username or password is incorrect'), [
                    'key' => 'auth'
                ]);
            }
        }
        $this->set('user',$user);
    }

    public function changePassword()
    {
        $userid = $this->Auth->user('id');
        $user = $this->TbDealMerchants->get($userid);
        if($this->request->is(["post","patch","put"]))
        {
            if($user!=null) {
                $password = $this->request->data["password"];
                $new_password = $this->request->data["new_password"];
                $confirm_password = $this->request->data["confirm_password"];
                $valid = true;
                if(!(new DefaultPasswordHasher)->check($password,$user->password))
                {
                    $valid = false;
                    $this->set("change_error",__d("smashdeal","Don't match current password"));
                }
                else if($new_password!=$confirm_password)
                {
                    $valid = false;
                    $this->set("change_error",__d("smashdeal","Password and Confirm Password doesn't match"));
                }
                if($valid)
                {
                    $user->password = $new_password;
                    if($this->TbDealMerchants->save($user))
                    {
                        $this->Flash->success(__d("smashdeal","Password Changed"));
                    }
                    else{
                        $this->Flash->error(__d("smashdeal","Fail to Change Password"));
                    }
                }
            }
        }
        
        $this->set('user',$user);

        if($this->request->is("mobile"))
            $this->renderView();
        else
            $this->renderView();
    }

    public function forgotPassword()
    {
        $user = $this->TbDealMerchants->newEntity();
        $this->set('user',$user);
        if($this->request->is("post","put","patch"))
        {
            $step = $this->request->session()->read('step');
            if(is_null($step))
            {
                $user = $this->TbDealMerchants->find('all')->where(['email'=>$this->request->data['email']]);
                if(count($user->toArray())>0)
                {
                    $random = rand(1000,9999);
                    $this->request->session()->write('forgot_password',$random);
                    $email = new Email('waruna');
                    $email
                        ->template('Smashdeal.users/forget_password','Smashdeal.default')
                        ->emailFormat('html')
                        ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                        ->to($this->request->data['email'])
                        ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                        ->subject(__d("smashdeal","Dealsmash - Forget Password"))
                        ->viewVars(['rand'=>$random,'user'=>$user->toArray()[0]])
                        ->send();
                    $this->set('email',$this->request->data['email']);
                    $this->request->session()->write('step','two');
                    $this->request->session()->write('email',$this->request->data['email']);
                    $this->renderView('forgot_password_two');
                }       
                else
                {
                    $this->Flash->error(__d("smashdeal","Tidak Ada User ditemukan dengan email tersebut"));
                    $this->renderView('forgot_password_one');
                }
            }
            else if($step == "two")
            {
                $code = $this->request->session()->read('forgot_password');
                if($code != null){
                    if($code == $this->request->data['code'])
                    {
                        $this->request->session()->delete('forgot_password');
                        $this->request->session()->write('step','three');
                        $this->set('email',$this->request->data['email']);
                        $this->renderView('forgot_password_three');
                    }
                    else
                    {
                        $this->set('email',$this->request->data['email']);
                        $this->Flash->error(__d("smashdeal","Code Tidak Cocok. Silahkan Coba lagi."));
                        $this->renderView('forgot_password_two');
                    }
                }
                else
                {
                    $this->request->session->delete('step');
                    $this->request->session->delete('email');
                    $this->redirect(['action'=>'forgotPassword']);
                }
            }
            else if($step == "three")
            {
                if($this->request->data['password'] == $this->request->data['confirm_password'])
                {
                    $user = $this->TbDealMerchants->find('all')->where(['email'=>$this->request->data['email']])->first();
                    $user->password = $this->request->data['password'];
                    if($this->TbDealMerchants->save($user))
                    {
                        $this->request->session()->delete('step');
                        $this->request->session()->delete('email');
                        $this->Flash->success(__d("smashdeal","Password Berhasil direset"));
                        $this->redirect(['action'=>'login']);
                    }
                    else{
                        $this->Flash->error(__d("smashdeal","Gagal mereset password silahkan coba lagi"));
                    }
                }
            }
        }
        else
        {   
            $step = $this->request->session()->read('step');
            $email = $this->request->session()->read('email');
            if(is_null($step))
            {
                $this->renderView('forgot_password_one');
            }
            else if($step == "two")
            {
                $code = $this->request->session()->read('forgot_password');
                if($code==null){
                    $this->request->session->delete('step');
                    $this->request->session->delete('email');
                    return $this->redirect(['action'=>'forgotPassword']);
                }
                $this->set('email',$email);
                $this->renderView('forgot_password_two');
            }
            else if($step == "three")
            {
                $this->set('email',$email);
                $this->renderView('forgot_password_three');
            }
        }
    }

    public function logout()
    {
        $this->Cookie->delete('rememberme');
        return $this->redirect($this->Auth->logout());
    }
}