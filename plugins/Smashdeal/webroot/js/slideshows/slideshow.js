$(document).ready(function(e){

    $("input[type='checkbox']").iCheck({
        checkboxClass: 'icheckbox_polaris',
        radioClass: 'iradio_square',
        increaseArea: '20%' // optional
    });

    $("#checkall").on('ifChanged',function(event)
    {
        $("input.checkbox-child").iCheck('toggle');
    });


});