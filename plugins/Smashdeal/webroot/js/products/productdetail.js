$(document).ready(function(e)
{
   $(".money").priceFormat({
       prefix:"Rp. ",
       centsSeparator:",",
       thousandsSeparator:".",
       centsLimit:0
   });

    $(".money").blur(function(e)
    {
        var target = $(this).attr("data-target");
        $(target).val($(this).unmask().trim());
    });

    var lang = getCookie("lang").split("_")[0];
    var dateformat = dpconverter(getCookie("dateform"));
    $(".date").datepicker({language:lang,format:dateformat});

});