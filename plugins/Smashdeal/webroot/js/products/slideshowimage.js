$(document).ready(function(e)
{
    $("#image-wrapper img").imgCheckbox({
        radio:true,
        checkMarkPosition:'top-right',
        canDeselect:true,
        addToForm:false,
        onclick:function(){
            $("#image-wrapper .isdefault").val("false");
            $(".imgChked").first().parent().find(".isdefault").val("true");
        }
    });
    $(".delete-image").click(function(e){
        e.preventDefault();
        $(this).parent().remove();
    });
    var id = $(".isdefault[value='true']").parent().attr("id");
    $("#"+id+" img").select();
    $("#media-selector").mediaSelector({
        'title':'Feature Image',
        'list_url':$("#media-list-link").attr("data-href"),
        'upload_url':$("#media-upload-link").attr("data-href"),
        'attribute_url':$("#media-attribute-link").attr("data-href"),
        'multiple':true,
        'filter':'image',
        'onselect' : function(e,selecteds)
        {
            var index = parseInt($("#slideshow").attr("data-index"));
            var datas = {"ids":[]};
            for(idx in selecteds){
                datas["ids"].push(selecteds[idx]);
            }
            $.ajax({
                type:'POST',
                url:$("#get-image-link").attr("data-href"),
                data:datas,
                success:function(data)
                {
                    $("#image-wrapper").append($(data));
                    $(".image-wrapper").each(function(data){
                        var dataintial = $(this).attr("data-initial");
                        if(typeof dataintial == typeof undefined){
                            var mediaId = $(this).attr("id");
                            $(this).removeAttr("id");
                            $(this).append("<input type='hidden' name='medias[" + index + "][mediaid]' value='" + mediaId + "' /><input type='hidden' name='medias[" + index + "][index]' value='" + index + "'/><input type='hidden' name='medias[" + index + "][isdefault]' class='hidden isdefault'/><a href='#' class='pull-right text-danger delete-image' >delete</a>")
                            index++;
                        }
                    });
                    $("#slideshow").attr("data-index",index);
                    $(".delete-image").unbind('click');
                    $(".delete-image").click(function(e){
                        e.preventDefault();
                        $(this).parent().remove();
                    });
                    $("#image-wrapper img").imgCheckbox({
                        radio:true,
                        checkMarkPosition:'top-right',
                        canDeselect:true,
                        addToForm:false,
                        onclick:function(){
                            $("#image-wrapper .isdefault").val("false");
                            $(".imgChked").first().parent().find(".isdefault").val("true");
                        }
                    });
                }
            });
        }
    });

});