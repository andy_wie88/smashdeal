$(document).ready(function(e)
{
   $(".money").priceFormat({
       prefix:"Rp. ",
       centsSeparator:",",
       thousandsSeparator:".",
       centsLimit:0
   });

    $(".money").blur(function(e)
    {
        var target = $(this).attr("data-target");
        if(target!=undefined)
          $(target).val($(this).unmask().trim());
    });

    $("#quantity").blur(function(e)
    {
      var price = $(this).attr("data-price");
      var amount = $(this).val();
      var subtotal = price * amount;

      $("#subtotal").val(subtotal);
      $("#subtotal-bottom").val(subtotal);
      $("#grandtotal").val(subtotal);
      $(".money").priceFormat({
         prefix:"Rp. ",
         centsSeparator:",",
         thousandsSeparator:".",
         centsLimit:0
     });
    });

    $("#cashinput").blur(function(e)
    {
      var price = $("#quantity").attr("data-price");
      var amount = $("#quantity").val();
      var subtotal = price * amount;
      var cashtoken = $($(this).attr("data-target")).val();
      console.log(cashtoken);
      console.log(subtotal);
      console.log(subtotal-cashtoken);

      $("#grandtotal").val(subtotal-cashtoken);
      $(".money").priceFormat({
         prefix:"Rp. ",
         centsSeparator:",",
         thousandsSeparator:".",
         centsLimit:0
     });
    })

});