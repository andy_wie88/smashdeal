<?php
namespace Smashdeal\Test\TestCase\Form;

use Cake\TestSuite\TestCase;
use Smashdeal\Form\GeneralConfigForm;

/**
 * Smashdeal\Form\GeneralConfigForm Test Case
 */
class GeneralConfigFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Form\GeneralConfigForm
     */
    public $GeneralConfig;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->GeneralConfig = new GeneralConfigForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GeneralConfig);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
