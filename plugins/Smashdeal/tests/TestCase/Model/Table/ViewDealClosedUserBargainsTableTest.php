<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealClosedUserBargainsTable;

/**
 * Smashdeal\Model\Table\ViewDealClosedUserBargainsTable Test Case
 */
class ViewDealClosedUserBargainsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealClosedUserBargainsTable
     */
    public $ViewDealClosedUserBargains;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_closed_user_bargains'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealClosedUserBargains') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealClosedUserBargainsTable'];
        $this->ViewDealClosedUserBargains = TableRegistry::get('ViewDealClosedUserBargains', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealClosedUserBargains);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
