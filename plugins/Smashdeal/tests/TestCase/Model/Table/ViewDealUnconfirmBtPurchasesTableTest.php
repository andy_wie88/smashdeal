<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealUnconfirmBtPurchasesTable;

/**
 * Smashdeal\Model\Table\ViewDealUnconfirmBtPurchasesTable Test Case
 */
class ViewDealUnconfirmBtPurchasesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealUnconfirmBtPurchasesTable
     */
    public $ViewDealUnconfirmBtPurchases;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_unconfirm_bt_purchases'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealUnconfirmBtPurchases') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealUnconfirmBtPurchasesTable'];
        $this->ViewDealUnconfirmBtPurchases = TableRegistry::get('ViewDealUnconfirmBtPurchases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealUnconfirmBtPurchases);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
