<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewProductUserBargainingTable;

/**
 * Smashdeal\Model\Table\ViewProductUserBargainingTable Test Case
 */
class ViewProductUserBargainingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewProductUserBargainingTable
     */
    public $ViewProductUserBargaining;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_product_user_bargaining'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewProductUserBargaining') ? [] : ['className' => 'Smashdeal\Model\Table\ViewProductUserBargainingTable'];
        $this->ViewProductUserBargaining = TableRegistry::get('ViewProductUserBargaining', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewProductUserBargaining);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
