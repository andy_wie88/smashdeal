<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\TbDealBtPurchasesTable;

/**
 * Smashdeal\Model\Table\TbDealBtPurchasesTable Test Case
 */
class TbDealBtPurchasesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\TbDealBtPurchasesTable
     */
    public $TbDealBtPurchases;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.tb_deal_bt_purchases'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbDealBtPurchases') ? [] : ['className' => 'Smashdeal\Model\Table\TbDealBtPurchasesTable'];
        $this->TbDealBtPurchases = TableRegistry::get('TbDealBtPurchases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbDealBtPurchases);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
